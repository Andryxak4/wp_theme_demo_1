<?php get_header(); ?>
<?php
$fields = get_fields();
?>
<!--slider start-->
<section id="about">
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <!--slider start-->
                <div class="slider-inner">
                    <div class="slider-container clearfix" >


                        <div class="slides-block clearfix" id="main-slider">
                            <?php if ($fields['slider_block_slides']):
                                foreach ($fields['slider_block_slides'] as $slide): ?>
                                    <div class="slide-block">
                                        <img src="<?= $slide['image'] ?>" alt="" >
                                    </div>
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </div>

                        <?php if (!empty($fields['slider_block_center_image'])): ?>
                        <img src="<?= $fields['slider_block_center_image']; ?>" alt="" class="img-centered">
                        <?php endif; ?>
                        <div class="sr">
                            <?php if ($fields['slider_block_title']): ?> <h3 class="sr-header"><?= $fields['slider_block_title'] ?></h3><?php endif; ?>
                            <h4 class="sr-subheader"><?= $fields['slider_block_subtitle'] ?> </h4>
                            <p class="sr-info"><?= $fields['slider_block_description'] ?></p>

                            <div class="sr-grill-properties">
                                <?php if ($fields['slider_block_list_items']): ?>
                                    <?php foreach ($fields['slider_block_list_items'] as $item): ?>
                                        <div class="sr-grill-row clearfix">
                                            <div class="sr-grill-row-img-block">
                                                <img src="<?= $item['icon'] ?>">
                                            </div>

                                            <div class="sr-grill-row-description">
                                                <div class="sr-grill-row-description-header"><?= $item['title'] ?></div>
                                                <p class="sr-grill-row-description-info"><?= $item['description'] ?></p>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!--slider end-->
            </div>

        </div>
    </div>
</section>
<!--slider end-->
<!--grills types start-->
<section id="grill-types">
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <a href="<?= site_url() ?>/product" class="section-header"><?= $fields['grill_types_block_title'] ?></a>
                <p class="section-info">
                    <?= $fields['grill_types_block_description'] ?>
                </p>
                <?php //  grill_types_block_categories ?>
                <?php if ($fields['grill_types_block_categories']): ?>
                    <div class="grills-list container clearfix">
                        <?php
                        foreach ($fields['grill_types_block_categories'] as $cartegory):
                            $cat_id = $cartegory['Category'];
                            $term = get_term($cat_id, 'product_categories');
                            $thumbnail = get_field('category_image', 'product_categories_' . $term->term_id);
                            ?>
                            <div class="col-md-3 grill-block-container">
                                <a href="<?= get_term_link($cat_id) ?>" class=" text-center grill-block">
                                    <img src="<?= $thumbnail ?>" alt="" class="grill-img">
                                    <p class="grill-name">
                                        <?= $term->name ?>
                                    </p>
                                </a>
                            </div>
                        <?php endforeach; ?>
                        <div class="col-xs-12">
                          <a href="<?= site_url() ?>/product" class="grills-list-more">Смотреть все</a>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<!--grills types end-->
<!--video section start-->
<section id="main-grills-video">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <a href="<?= $fields['grills_video_block_button_url'] ?>" class="section-header"><?= $fields['grills_video_block_title'] ?></a>
                <p class="section-info"><?= $fields['grills_video_block_description'] ?></p>
                <div class="video-main clearfix">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/img/grill-blade.jpg" alt="" class="grill-blade">
                    <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0 video-block">
                        <div class="row">
                            <?php if ($fields['grills_video_block_slider']): ?>
                                <div class="col-md-10 col-md-offset-1 video-block-container" id="video-main">
                                    <?php foreach ($fields['grills_video_block_slider'] as $video): ?>
                                        <?php $video_url = $video['video_url']
                                                . (strpos($video['video_url'], "?") ? "&" : "?")
                                                . "enablejsapi=1"; ?>
                                        <iframe src="<?= $video_url; ?>" frameborder="0" allowscriptaccess="always" allowfullscreen="true"></iframe>
                                    <?php endforeach; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/img/grill-fork.jpg" alt="" class="grill-fork">
                </div>
                <a href="<?= $fields['grills_video_block_button_url'] ?>" class="video-all"><?= $fields['grills_video_block_button_label'] ?></a>
            </div>
        </div>
    </div>
</section>
<!--video section end-->
<?php if ($fields['block_products']): ?>
    <?php $count_slider = 0; ?>
    <?php foreach ($fields['block_products'] as $product): ?>
        <!--fireplace grills start-->
        <section id="fp-grills">
            <?php $count_init_slider = 0; ?>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <a href="<?= $product['all_models_url'] ?>" class="section-header"><?= $product['block_title'] ?></a>
                        <p class="section-info">
                            <?= $product['block_description'] ?>
                        </p>

                        <div class="grills-product">
                            <div class="grills-product-block">
                                <div class="grills-product-container row">            
                                    <div class="grills-info-blocks">
                                        <div class="grills-product-nav">
                                            <a href="<?= $product['all_models_url'] ?>">Посмотреть все модели</a>
                                            <ul class="nav nav-tabs" role="tablist">
                                                <?php $count_choise = 1; ?>
                                                <?php $tabIndex = 0; ?>
                                                <?php foreach($product['products'] as $grill): ?>
                                                    <?php $post_cat = get_the_terms($grill->ID, 'product_categories'); ?>
                                                    <li class="grills-product-nav-row clearfix <?php if($count_choise == 1){ echo 'active'; $count_choise++; }else{ echo 'half-row'; } ?>">
                                                        <a data-tabIndex="<?= $tabIndex ?>" data-target="#<?= $post_cat[0]->slug ?>_grill_<?= $grill->ID ?>" aria-controls="<?= $post_cat[0]->slug ?>_grill_<?= $grill->ID ?>" role="tab" data-toggle="tab" class="grills-product-nav-btn "><?= $grill->post_title ?></a>
                                                    </li>
                                                    <?php $tabIndex++; ?>
                                                <?php endforeach; ?>
                                            </ul>
                                        </div>
                                        <!--lappigrill-vs start-->
                                        <?php $count = 1; ?>
                                        <?php $image_count = 0; ?>
                                        <?php foreach($product['products'] as $grill): ?>
                                            <?php $wc_product = new WC_Product($grill->ID); ?>
                                            <?php $post_cat = get_the_terms($grill->ID, 'product_categories'); ?>
                                            <?php $grill_fields = get_fields($grill->ID); ?>
                                            <div role="tabpanel" id="<?= $post_cat[0]->slug ?>_grill_<?= $grill->ID ?>" class="grills-product-info tab-pane clearfix <?php if($count == 1){ echo 'active'; $count++; } ?>">
                                                <div class="grills-product-info-text">
                                                    <div class="grills-product-info-text-header clearfix">
                                                      <div class="grills-product-info-text-header-name">
                                                        <?= $grill->post_title ?>
                                                      </div>
                                                      <div class="grills-product-info-text-header-price">
                                                        <?= number_format($wc_product->get_price() ?: 0, 0, '.', ' '); ?> руб
                                                      </div>
                                                    </div>
                                                    <div class="grills-product-info-text-materials"><?= $grill_fields['fuel_type'] ?></div>
                                                    <div class="grills-product-info-text-text"><?= $grill->post_content ?></div>
                                                    <ul class="grill-product-params">
                                                        <li class="clearfix">
                                                            <div class="grill-product-params-param" title="<?= $grill_fields['hearth_size_hint'] ?>">
                                                                Размер очага:
                                                            </div>
                                                            <div class="grill-product-params-value"><?= lappi_hearth_size($grill_fields['hearth_size']); ?></div>
                                                        </li>

                                                        <li class="clearfix">
                                                            <div class="grill-product-params-param" title="<?= $grill_fields['hearth_thickness_hint'] ?>">
                                                                Толщина/материал очага:
                                                            </div>
                                                            <div class="grill-product-params-value"><?= $grill_fields['hearth_thickness'] ?></div>
                                                        </li>

                                                        <li class="clearfix">
                                                            <div class="grill-product-params-param" title="<?= $grill_fields['hearth_zone_control_hint'] ?>">
                                                                Регулировка по высоте:
                                                            </div>
                                                            <div class="grill-product-params-value"><?= arrayItem($grill_fields, 'hearth_zone_control', 'Нет'); ?></div>
                                                        </li>

                                                        <li class="clearfix">
                                                            <div class="grill-product-params-param" title="<?= $grill_fields['fuel_type_hint'] ?>">
                                                                Вид топлива:
                                                            </div>
                                                            <div class="grill-product-params-value"><?= $grill_fields['fuel_type'] ?></div>
                                                        </li>

                                                        <li class="clearfix">
                                                            <div class="grill-product-params-param" title="<?= $grill_fields['warranty_period_hint'] ?>">
                                                                Гарантийный срок:
                                                            </div>
                                                            <div class="grill-product-params-value"><?= $grill_fields['warranty_period'] ?></div>
                                                        </li>
                                                    </ul>
                                                    <a href="<?= get_permalink($grill->ID) ?>" class="grills-product-info-text-more">Узнать подробности о модели</a>
                                                </div>
                                                <div class="grills-product-info-img">
                                                  <div class="grills-product-info-img-slider">
                                                    <div class="slider-container product-main-slider main-vs-slider-<?= $count_slider ?>" data-sliderinit="<?= $count_init_slider ?>" id="">
                                                        <?php $product_pics = array_slice($grill_fields['images'],0,9); ?>
                                                        <div class="slide">
                                                          <img src="<?= get_the_post_thumbnail_url($grill->ID) ?>" data-zoom-image='<?= get_the_post_thumbnail_url($grill->ID) ?>' alt="<?=   get_the_post_thumbnail_url($grill->ID) ?>">
                                                        </div>
                                                        <?php $image_count++; ?>
                                                        <?php foreach($product_pics as $photo): ?>
                                                            <div class="slide">
                                                              <img src="<?= $photo['image'] ?>" data-zoom-image="<?= $photo['image'] ?>">
                                                            </div>
                                                            <?php $image_count++; ?>
                                                        <?php endforeach; ?>
                                                    </div>
                                                  </div>
                                                </div>
                                                <div class="grills-product-info-slider">
                                                    <div class="slider-container fp-vs-slider-<?= $count_slider ?>" data-sliderinit="<?= $count_init_slider ?>" id="">
                                                        <div class="slide">
                                                            <img src="<?= get_the_post_thumbnail_url($grill->ID) ?>" alt="<?= get_the_post_thumbnail_url($grill->ID) ?>">
                                                        </div>
                                                        <?php foreach($product_pics as $photo): ?>
                                                            <div class="slide" style="cursor: pointer;">
                                                                <img src="<?= $photo['image'] ?>">
                                                            </div>
                                                        <?php endforeach; ?>
                                                    </div>
                                                </div>
                                                
                                                <div class="grills-product-info-text mobile">
                                                    <h4 class="grills-product-info-text-header"><?= $grill->post_title ?></h4>
                                                    <div class="grills-product-info-text-materials"><?= $grill_fields['fuel_type'] ?></div>
                                                    <div class="grills-product-info-text-price"><?= number_format($wc_product->get_price() ?: 0, 0, '.', ' '); ?> руб</div>
                                                    <div class="grills-product-info-text-text"><?= $grill->post_content ?></div>
                                                    <ul class="grill-product-params">
                                                        <li class="clearfix">
                                                            <div class="grill-product-params-param" title="<?= $grill_fields['hearth_size_hint'] ?>">
                                                                Размер очага:
                                                            </div>
                                                            <div class="grill-product-params-value"><?= lappi_hearth_size($grill_fields['hearth_size']); ?></div>
                                                        </li>

                                                        <li class="clearfix">
                                                            <div class="grill-product-params-param" title="<?= $grill_fields['hearth_thickness_hint'] ?>">
                                                                Толщина/материал очага:
                                                            </div>
                                                            <div class="grill-product-params-value"><?= $grill_fields['hearth_thickness'] ?></div>
                                                        </li>

                                                        <li class="clearfix">
                                                            <div class="grill-product-params-param" title="<?= $grill_fields['hearth_zone_control_hint'] ?>">
                                                                Регулировка по высоте:
                                                            </div>
                                                            <div class="grill-product-params-value"><?= arrayItem($grill_fields, 'hearth_zone_control', 'Нет'); ?></div>
                                                        </li>

                                                        <li class="clearfix">
                                                            <div class="grill-product-params-param" title="<?= $grill_fields['fuel_type_hint'] ?>">
                                                                Вид топлива:
                                                            </div>
                                                            <div class="grill-product-params-value"><?= $grill_fields['fuel_type'] ?></div>
                                                        </li>

                                                        <li class="clearfix">
                                                            <div class="grill-product-params-param" title="<?= $grill_fields['warranty_period_hint'] ?>">
                                                                Гарантийный срок:
                                                            </div>
                                                            <div class="grill-product-params-value"><?= $grill_fields['warranty_period'] ?></div>
                                                        </li>
                                                    </ul>

                                                    <a href="<?= get_permalink($grill->ID) ?>" class="grills-product-info-text-more">Узнать подробности о модели</a>
                                                </div>
                                                
                                                <div class="btns-buy">
                                                  <a href="" class="buy-btn oneclick"
                                                       data-product-id="<?= $grill->ID ?>"
                                                       data-product-name="<?= esc_html($grill->post_title) ?>">
                                                    <span class="buy-btn-icon oneclick">
                                                      <img src="<?= get_stylesheet_directory_uri() ?>/img/buy-oneclick.png" alt="">
                                                      <img src="<?= get_stylesheet_directory_uri() ?>/img/buy-oneclick-hover.png" class="hover" alt="">
                                                    </span>
                                                    купить в 1 клик
                                                  </a>
                                                  <a href="" class="buy-btn btn-to-cart" data-product-id="<?= $grill->ID ?>"
                                                     data-product-name="<?= esc_html($grill->post_title) ?>" data-product-sku="<?= $wc_product->get_sku(); ?>"
                                                     data-product-price="<?= number_format($wc_product->get_price() ?: 0, 0, '.', ' '); ?>" data-product-image="<?= get_the_post_thumbnail_url($grill->ID); ?>">
                                                    <span class="buy-btn-icon oneclick">
                                                      <img src="<?= get_stylesheet_directory_uri() ?>/img/to-cart.png" alt="">
                                                      <img src="<?= get_stylesheet_directory_uri() ?>/img/to-cart-hover.png" class="hover" alt="">
                                                    </span>                                 
                                                    Добавить в корзину
                                                  </a>
                                                </div>
                                            </div>
                                            <?php $count_slider++; ?>
                                            <?php $count_init_slider++; ?>
                                        <?php endforeach; ?>
                                        <!--lappigrill-vs end-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!--fireplace grills start-->
    <?php endforeach; ?>
    <input type="hidden" id="hidden-slider-count" value="<?= $count_slider ?>">
<?php endif; ?>


<!--additional equipment start-->
<section id="add-equipment">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <a href="<?= $fields['block_additional equipment_button_url'] ?>" class="section-header"><?= $fields['block_additional equipment_title'] ?></a>
                <p class="section-info">
                    <?= $fields['block_additional equipment_description'] ?>
                </p>
                <?php ///  block_additional equipment_products   ?>

                <?php if ($fields['block_additional equipment_products']): ?>
                    <div class="add-equipment">
                        <!--add-eq slider start-->
                        <div class="add-equipment-container" id="addeq">
                            <?php foreach ($fields['block_additional equipment_products'] as $product): ?>
                                <?php $wc_product = new WC_Product($product->ID); ?>
                                <div class="add-equipment-slide">
                                    <div class="add-equipment-inner">
                                        <img src="<?php echo get_the_post_thumbnail_url($product->ID) ?>" alt="" class="add-equipment-img">
                                        <div class="add-equipment-header"><?= $product->post_title ?></div>
                                        <div class="add-equipment-info">
                                            <div class="add-equipment-info-text">
                                                <?= $product->post_excerpt?>
                                            </div>

                                            <a href="<?= get_permalink($product->ID) ?>" class="add-equipment-info-more">
                                                подробнее
                                            </a>
                                            <div class="add-equipment-info-price">
                                              <?= number_format($wc_product->get_price() ?: 0, 0, '.', ' '); ?> руб
                                            </div>
                                            <div class="btns-buy">
                                              <a href="" class="buy-btn oneclick"
                                                   data-product-id="<?= $product->ID ?>"
                                                   data-product-name="<?= esc_html($product->post_title) ?>">
                                                <span class="buy-btn-icon oneclick">
                                                  <img src="<?= get_stylesheet_directory_uri() ?>/img/buy-oneclick.png" alt="">
                                                  <img src="<?= get_stylesheet_directory_uri() ?>/img/buy-oneclick-hover.png" class="hover" alt="">
                                                </span>
                                                купить в 1 клик
                                              </a>
                                              <a href="" class="buy-btn btn-to-cart" data-product-id="<?= $product->ID ?>"
                                                     data-product-name="<?= esc_html($product->post_title) ?>" data-product-sku="<?= $wc_product->get_sku(); ?>"
                                                     data-product-price="<?= number_format($wc_product->get_price() ?: 0, 0, '.', ' '); ?>" data-product-image="<?= get_the_post_thumbnail_url($product->ID); ?>">
                                                <span class="buy-btn-icon oneclick">
                                                  <img src="<?= get_stylesheet_directory_uri() ?>/img/to-cart.png" alt="">
                                                  <img src="<?= get_stylesheet_directory_uri() ?>/img/to-cart-hover.png" class="hover" alt="">
                                                </span>                                 
                                                Добавить в корзину
                                              </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                            <!-- sliders end-->
                        </div>
                        <!--add-eq slider end-->
                        <a href="<?= $fields['block_additional equipment_button_url'] ?>" class="addeq-all"><?= $fields['block_additional equipment_button_label'] ?></a>

                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<!--additional equipment end-->

<!--dilers start-->
<section id="dilers">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <a href="<?= $fields['block_dilers_title_url'] ?>" class="section-header"><?= $fields['block_dilers_title'] ?></a>
                <div class="dilers-block clearfix">
                    <div class="col-md-6">
                        <?= $fields['block_dilers_description'] ?>
                    </div>

                    <div class="col-md-6 map">
                        <img src="<?= $fields['block_dilers_map'] ?>" alt="">
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!--dilers end-->

<?php get_footer(); ?>