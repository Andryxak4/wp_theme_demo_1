<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <?php wp_head(); ?>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

    </head>
    <?php
    $lappigrill_compare_ids = arrayItem($_COOKIE, 'lappigrill_compare_ids', '');
    $compare_ids = $lappigrill_compare_ids ? explode(',', $lappigrill_compare_ids) : array();
    $compare_page_url = get_field('comparison_page_url', 'options') ?: "#";
    $options = get_fields('options');
    ?>
    <body <?= $compare_ids ? 'class="comp-active"' : ''; ?>>

        <!--header start-->
        <header id="header">
            <nav class="navbar navbar-default">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 clearfix">
                            <!--logo start-->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                
                                <a class="navbar-brand" href="/"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/logo.png" alt="logo"></a>
                            </div>
                            <!--logo end-->

                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <div class="mobile-close"></div>
                                <?php
                                $walker = new LappigrillMenuWalker(array(
                                    'li' => array(),
                                    'a' => array(),
                                ), 'active');
                                $primary_menu_args = array(
                                    'container' => 'ul',
                                    'container_class' => 'nav navbar-nav',
                                    'items_wrap' => '<ul class="nav navbar-nav">%3$s</ul>',
                                    'walker' => $walker,
                                    'theme_location' => 'primary'
                                );
                                wp_nav_menu($primary_menu_args);
                                ?>
                            </div>  

                            

                            <div class="nav-contacts">
                                <div class="nav-contacts-phone clearfix">
                                    <?php $phone1_digits = preg_replace("/[^0-9]/", "", $options['phone_number']); ?>
                                    <?php if ($phone1_digits): ?>
                                    <img src="<?php echo get_stylesheet_directory_uri() ?>/img/nav-call.png" alt="" class="nav-c-icon">
                                    <a href="tel:+<?= $phone1_digits ?>" class="nav-c-info"><?= $options['phone_number'] ?></a><span class="nav-c-info-comment"><?= $options['phone_number_comment'] ?></span>
                                    <?php else: ?>
                                    <?= $options['phone_number'] ?> <?= $options['phone_number_comment'] ?>
                                    <?php endif; ?>
                                </div>
                                
                                <div class="nav-contacts-phone clearfix">
                                    <?php $phone2_digits = preg_replace("/[^0-9]/", "", $options['phone_number_retail']); ?>
                                    <?php if ($phone2_digits): ?>
                                    <img src="<?php echo get_stylesheet_directory_uri() ?>/img/nav-call.png" alt="" class="nav-c-icon">
                                    <a href="tel:+<?= $phone2_digits ?>" class="nav-c-info"><?= $options['phone_number_retail'] ?></a>
                                    <span class="nav-c-info-comment"><?= $options['phone_number_retail_comment'] ?></span>
                                    <?php else: ?>
                                    <?= $options['phone_number_retail'] ?> <?= $options['phone_number_retail_comment'] ?>
                                    <?php endif; ?>
                                </div>

<?php //DISABLED ?>
<?php if (0): ?>
                                <div class="nav-contacts-mail clearfix">
                                    <img src="<?php echo get_stylesheet_directory_uri() ?>/img/nav-mail.png" alt="" class="nav-c-icon">
                                    <a href="mailto:<?= $options['email'] ?>" class="nav-c-info"><?= $options['email'] ?></a>
                                </div>
<?php endif; ?>
                            </div>
                            
                            <div class="add-props-header">
                              <a href="<?= $compare_page_url; ?>" class="comparision clearfix">
                                <span class="comparision-wrapper">
                                  <span class="comparision-amount"><?= count($compare_ids); ?></span>
                                  <span class="comparision-icon"></span>
                                </span>
                              </a>

                              <a href="<?= wc_get_cart_url(); ?>" class="cart">
                                <span class="cart-wrapper">
                                  <span class="cart-amount"><?= WC()->cart->get_cart_contents_count() ?></span>
                                  <span class="cart-icon"></span>
                                </span>
                              </a>
                            </div>
                        </div>

                    </div> 
                </div>
            </nav>
        </header>
        <!--header end-->
        
        
        


        
        
        
      