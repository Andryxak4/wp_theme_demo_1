<?php
get_header();

$current_category_id = false;
if (!is_post_type_archive('product')):
    $current_term = get_queried_object();
    $current_category_id = get_queried_object_id();
endif;

$lappigrill_compare_ids = arrayItem($_COOKIE, 'lappigrill_compare_ids', '');
$compare_ids = $lappigrill_compare_ids ? explode(',', $lappigrill_compare_ids) : array();

?>

<!--categories start-->
<section id="categories">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ol class="breadcrumb">
          <li><a href="/">Главная</a></li>
          <li class="active"><?= isset($current_term) ? $current_term->name : 'Вся продукция'; ?></li>
        </ol>

        <h2  class="section-header">наша продукция</h2>
        <p class="section-info">
          <?= isset($current_term) ? $current_term->description : get_field('all_products_description', 'options'); ?>
        </p>


        <?php include __DIR__.DIRECTORY_SEPARATOR.'templates/product-categories-menu.php' ?>
      </div>

      <div class="col-md-12">
        <!--category products start-->
        <div class="category-products clearfix">
            <?php
            if(is_post_type_archive('product')):
                global $wpdb, $paged;
                $sql = "SELECT p.ID FROM {$wpdb->posts} p "
                    . " INNER JOIN {$wpdb->term_relationships} tr ON tr.object_id=p.ID "
                    . " INNER JOIN {$wpdb->term_taxonomy} tt ON tt.term_taxonomy_id=tr.term_taxonomy_id "
                    . " INNER JOIN {$wpdb->terms} t ON t.term_id=tr.term_taxonomy_id "
                    . " WHERE p.post_type='product' "
                    . " AND p.post_status='publish' "
                    . " AND tt.taxonomy='product_categories' "
                    . " ORDER BY t.term_order, p.menu_order, p.post_date DESC";
                $all_products = $wpdb->get_results($sql, ARRAY_A);
                $all_products = array_map(function($item) { return $item['ID']; }, $all_products);

                $args = array(
                    'post_type' => 'product',
                    'post__in' => $all_products,
                    'orderby' => 'post__in',
                    'paged' => $paged ?: 1,
                );
                $products_query = new WP_Query($args);
                while ($products_query->have_posts()):
                    $products_query->the_post();
                    $wc_product = new WC_Product(get_the_ID());
                    $fields = get_fields(false, false);
                    ?>
                  <!--prod-1-->
                  <div class="category-product product-<?php the_ID(); ?>">
                    <div class="category-product-img">
                        <a href="<?= get_permalink(get_the_ID()) ?>">
                            <img src="<?= get_the_post_thumbnail_url(); ?>" alt="">
                        </a>
                    </div>
                    <a href="<?php the_permalink(); ?>" class="category-product-link">
                    <div class="category-product-header">
                      <?php the_title(); ?>
                    </div>
                    </a>
                    <div class="category-product-params">
                      <div class="category-product-params-price"><?= number_format($wc_product->get_price() ?: 0, 0, '.', ' '); ?> руб</div>
                      <?php if (empty($fields['choose_product'])): ?>
                      <ul class="category-product-params-list">
                        <li class="clearfix">
                          <div class="param-name">Размер очага:</div>
                          <div class="param-value"><?= lappi_hearth_size(arrayItem($fields, 'hearth_size')); ?></div>
                        </li>

                        <li class="clearfix">
                          <div class="param-name">Толщина/материал очага:</div>
                          <div class="param-value"><?= arrayItem($fields, 'hearth_thickness'); ?></div>
                        </li>

                        <li class="clearfix">
                          <div class="param-name">Регулировка по высоте:</div>
                          <div class="param-value">
                              <?= arrayItem($fields, 'hearth_zone_control', 'Нет'); ?>
                          </div>
                        </li>

                        <li class="clearfix">
                          <div class="param-name">Вид топлива:</div>
                          <div class="param-value"><?= arrayItem($fields, 'fuel_type'); ?></div>
                        </li>

                        <li class="clearfix">
                          <div class="param-name">Гарантийный срок:</div>
                          <div class="param-value"><?= arrayItem($fields, 'warranty_period'); ?></div>
                        </li>
                      </ul>
                      <?php else: ?>
                      <div class="category-product-params-description"><?php the_content(); ?></div>
                      <?php endif; ?>
                    </div>

                    <div class="category-product-btns clearfix">
                      <a href="<?php the_permalink(); ?>" class="category-product-more">Подробнее</a>

                      <div class="category-comp-block <?= in_array(get_the_ID(), $compare_ids) ? "addedToComp" : "" ?>" data-post_id="<?php the_ID(); ?>">
                        <button class="category-product-btn">
                          <span class="compared"><span class="compared-text">В сравнении</span></span>
                        </button>
                        <div class="compare-remove">Удалить из сравнения</div>
                      </div>
                    </div>
                    
                    <div class="btns-buy">
                      <a href="" class="buy-btn oneclick"
                            data-product-id="<?php the_ID(); ?>"
                            data-product-name="<?= esc_html(get_the_title()); ?>">
                        <span class="buy-btn-icon oneclick">
                          <img src="<?= get_stylesheet_directory_uri() ?>/img/buy-oneclick.png" alt="">
                          <img src="<?= get_stylesheet_directory_uri() ?>/img/buy-oneclick-hover.png" class="hover" alt="">
                        </span>
                        Купить в 1 клик
                      </a>
                      <a href="" class="buy-btn btn-to-cart"
                            data-product-id="<?php the_ID(); ?>"
                            data-product-name="<?= esc_html(get_the_title()); ?>"
                            data-product-sku="<?= $wc_product->get_sku(); ?>"
                            data-product-price="<?= number_format($wc_product->get_price() ?: 0, 0, '.', ' '); ?>"
                            data-product-image="<?= get_the_post_thumbnail_url(get_the_ID()); ?>">
                        <span class="buy-btn-icon oneclick">
                          <img src="<?= get_stylesheet_directory_uri() ?>/img/to-cart.png" alt="">
                          <img src="<?= get_stylesheet_directory_uri() ?>/img/to-cart-hover.png" class="hover" alt="">
                        </span>                                 
                        В корзину
                      </a>
                    </div>
                  </div>
                  <!--prod-1 end-->

                <?php
                endwhile;
                wp_reset_query();
                wp_reset_postdata();
                $current_page = $paged ?: 1;
                $total_pages = $products_query->max_num_pages ?: 1;
            else:
                while (have_posts()):
                    the_post();
                    $wc_product = new WC_Product(get_the_ID());
                    $fields = get_fields(false, false);
                    ?>
                    <!--prod-1-->
                    <div class="category-product product-<?php the_ID(); ?>">
                        <div class="category-product-img">
                            <a href="<?= get_permalink(get_the_ID()) ?>">
                                <img src="<?= get_the_post_thumbnail_url(); ?>" alt="">
                            </a>
                        </div>
                        <a href="<?php the_permalink(); ?>" class="category-product-link">
                        <div class="category-product-header">
                            <?php the_title(); ?>
                        </div>
                        </a>
                        <div class="category-product-params">
                          <div class="category-product-params-price"><?= number_format($wc_product->get_price() ?: 0, 0, '.', ' '); ?> руб</div>
                          <?php if (empty($fields['choose_product'])): ?> 
                            <ul class="category-product-params-list">
                                <li class="clearfix">
                                    <div class="param-name">Размер очага:</div>
                                    <div class="param-value"><?= lappi_hearth_size(arrayItem($fields, 'hearth_size')); ?></div>
                                </li>

                                <li class="clearfix">
                                    <div class="param-name">Толщина/материал очага:</div>
                                    <div class="param-value"><?= arrayItem($fields, 'hearth_thickness'); ?></div>
                                </li>

                                <li class="clearfix">
                                    <div class="param-name">Регулировка по высоте:</div>
                                    <div class="param-value"><?= arrayItem($fields, 'hearth_zone_control', 'Нет'); ?></div>
                                </li>

                                <li class="clearfix">
                                    <div class="param-name">Вид топлива:</div>
                                    <div class="param-value"><?= arrayItem($fields, 'fuel_type'); ?></div>
                                </li>

                                <li class="clearfix">
                                    <div class="param-name">Гарантийный срок:</div>
                                    <div class="param-value"><?= arrayItem($fields, 'warranty_period'); ?></div>
                                </li>
                            </ul>
                         <?php else: ?>
                            <div class="category-product-params-description"><?php the_content(); ?></div>
                          <?php endif; ?>
                        </div>

                        <div class="category-product-btns clearfix">
                            <a href="<?php the_permalink(); ?>" class="category-product-more">Подробнее</a>

                            <div class="category-comp-block <?= in_array(get_the_ID(), $compare_ids) ? "addedToComp" : "" ?>" data-post_id="<?php the_ID(); ?>">
                                <button class="category-product-btn">
                                    <span class="compared"><span class="compared-text">В сравнении</span></span>
                                </button>
                                <div class="compare-remove">Удалить из сравнения</div>
                            </div>
                        </div>
                        
                        <div class="btns-buy">
                          <a href="" class="buy-btn oneclick"
                                data-product-id="<?php the_ID(); ?>"
                                data-product-name="<?= esc_html(get_the_title()); ?>">
                            <span class="buy-btn-icon oneclick">
                              <img src="<?= get_stylesheet_directory_uri() ?>/img/buy-oneclick.png" alt="">
                              <img src="<?= get_stylesheet_directory_uri() ?>/img/buy-oneclick-hover.png" class="hover" alt="">
                            </span>
                            Купить в 1 клик
                          </a>
                          <a href="" class="buy-btn btn-to-cart"
                                data-product-id="<?php the_ID(); ?>"
                                data-product-name="<?= esc_html(get_the_title()); ?>"
                                data-product-sku="<?= $wc_product->get_sku(); ?>"
                                data-product-price="<?= number_format($wc_product->get_price() ?: 0, 0, '.', ' '); ?>"
                                data-product-image="<?= get_the_post_thumbnail_url(get_the_ID()); ?>">
                            <span class="buy-btn-icon oneclick">
                              <img src="<?= get_stylesheet_directory_uri() ?>/img/to-cart.png" alt="">
                              <img src="<?= get_stylesheet_directory_uri() ?>/img/to-cart-hover.png" class="hover" alt="">
                            </span>                                 
                            В корзину
                          </a>
                        </div>
                        
                        
                    </div>
                    <!--prod-1 end-->

                <?php
                endwhile;
                global $paged, $wp_query;
                $current_page = $paged ?: 1;
                $total_pages = $wp_query->max_num_pages ?: 1;

            endif;
            ?>
        </div>
        
        <?php if ($total_pages > 1): ?>
        <!--blog pagination-->
        <nav class="page-nav">
          <ul class="pagination clearfix">
            <?php if ($current_page > 1): ?>
              <li class="page-item"><a class="page-link prev" href="<?= get_pagenum_link($current_page-1); ?>"></a></li>
            <?php endif; ?>
            
            <?php for ($i=1; $i<= $total_pages; $i++): ?>
              <?php if (absint($current_page - $i) <= 2): ?>
              <li class="page-item"><a class="page-link <?= $i==$current_page ? "active" : "" ?>" href="<?= get_pagenum_link($i); ?>"><?= $i ?></a></li>
              <?php endif; ?>
            <?php endfor; ?>
            
            <?php if ($current_page < $total_pages): ?>
              <li class="page-item"><a class="page-link next" href="<?= get_pagenum_link($current_page+1); ?>"></a></li>
            <?php endif; ?>
          </ul>
        </nav>
        <?php endif; ?>
        
        <!--category products end-->
      </div>
      
      
    </div>
  </div>
</section>
<!--categories end-->
<?php get_footer(); ?>