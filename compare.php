<?php get_header(); ?>


<!--dilers section start-->
<section class="compare" id="compare">
  <div class="container">
    <div class="row">
      <div class="compare-wrapper col-lg-12 col-md-12 col-sm-12 col-xs-12 clearfix">
        <?= dimox_breadcrumbs(); ?>

        <h2 class="section-header">
          <?php the_title() ?>
        </h2>
        
        <div class="compare-nav clearfix">
          <div class="compare-nav-header">Показывать:</div>
          <div class="compare-filters clearfix">
            <a href="#" class="filter-btn show-chars compare-all active">Все характеристики</a>
            <a href="#" class="filter-btn show-chars compare-diff">Только отличия</a>
            <a href="#" class="filter-btn compare-del"><span class="compare-del-text">Удалить все</span></a>
          </div>
          
          <a href="#" class="compare-back"><span class="compare-back-text">Назад к продукции</span></a>
        </div>
        
        <div class="compare-main">
          <!-- compare block 1 start -->
          <div class="compare-main-block">
            <div class="compare-main-block-header">ОСНОВНЫЕ ХАРАКТЕРИСТИКИ </div>
            <div class="compare-main-block-body">
              <!-- row names start -->
              <div class="compare-main-row clearfix row-name">
                <div class="compare-main-row-header">
                  <div class="compare-main-row-header-value">Наименование:</div>
                </div>
                <div class="compare-main-row-cell">
                  <div href="#" class="compare-main-row-cell-value">
                    <a href="#" class="row-name-text">Lappigrill-90</a>
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/img/cart-popup-remove.png" alt="" class="row-name-del">
                  </div>
                </div>
                
                <div class="compare-main-row-cell">
                  <div href="#" class="compare-main-row-cell-value">
                    <a href="#" class="row-name-text">Lappigrill-vs</a>
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/img/cart-popup-remove.png" alt="" class="row-name-del">
                  </div>
                </div>
                
                <div class="compare-main-row-cell">
                  <div href="#" class="compare-main-row-cell-value">
                    <a href="#" class="row-name-text">LAPPIGRILL-INOXQ-S</a>
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/img/cart-popup-remove.png" alt="" class="row-name-del">
                  </div>
                </div>
                
                <div class="compare-main-row-cell">
                  <div href="#" class="compare-main-row-cell-value">
                    <a href="#" class="row-name-text">LAPPIGRILL-INOXV-S</a>
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/img/cart-popup-remove.png" alt="" class="row-name-del">
                  </div>
                </div>
              </div>
              <!-- row names end -->
              
              <div class="compare-main-row clearfix row-images">
                <div class="compare-main-row-header">
                  <div class="compare-main-row-header-value">Изображение:</div>
                </div>
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/img/addeq1.png" alt="">
                  </div>
                </div>
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/img/bbq3.png" alt="">
                  </div>
                </div>
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/img/in4.png" alt="">
                  </div>
                </div>          
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/img/grillpr-3.png" alt="">
                  </div>
                </div>
              </div>
              
              <div class="compare-main-row clearfix">
                <div class="compare-main-row-header">
                  <div class="compare-main-row-header-value">Рекомендованная розничная цена:</div>
                </div>
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">
                    139 000 РУБ
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">
                    150 000 РУБ
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">
                    139 000 РУБ
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>          
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">
                    139 000 РУБ
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              
              <div class="compare-main-row clearfix">
                <div class="compare-main-row-header">
                  <div class="compare-main-row-header-value">Тип гриля:</div>
                </div>
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">
                    каминный
                  </div>
                </div>
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">
                    каминный
                  </div>
                </div>
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">
                    каминный
                  </div>
                </div>          
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">
                    каминный
                  </div>
                </div>
              </div>
              
              <div class="compare-main-row clearfix">
                <div class="compare-main-row-header">
                  <div class="compare-main-row-header-value">Размер очага:</div>
                </div>
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">
                    пикт. 60 см х 46 см
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">
                    пикт. 60 см х 46 см
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">
                    пикт. 60 см х 46 см
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>          
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">
                    пикт. 60 см х 46 см
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              
              <div class="compare-main-row clearfix">
                <div class="compare-main-row-header">
                  <div class="compare-main-row-header-value">Толщина/материал очага:</div>
                </div>
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">
                    "- 6 мм/углеродистая сталь"<br>  "- покрытие до t600 град. С"
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">
                    "- 6 мм/углеродистая сталь"<br>  "- покрытие до t600 град. С"
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">
                    "- 6 мм/углеродистая сталь"<br>  "- покрытие до t600 град. С"
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>         
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">
                    "- 6 мм/углеродистая сталь"<br>  "- покрытие до t600 град. С"
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              
              <div class="compare-main-row clearfix">
                <div class="compare-main-row-header">
                  <div class="compare-main-row-header-value">Регулировка по высоте: </div>
                </div>
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">
                    '- уровня горения
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">
                    '- уровня горения
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">
                    '- уровня горения
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>          
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">
                    '- уровня горения
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              
              <div class="compare-main-row clearfix">
                <div class="compare-main-row-header">
                  <div class="compare-main-row-header-value">Вид топлива:</div>
                </div>
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">
                    уголь/дрова
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">
                    уголь/дрова
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">
                    уголь/дрова
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>          
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">
                    уголь/дрова
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              
              <div class="compare-main-row clearfix">
                <div class="compare-main-row-header">
                  <div class="compare-main-row-header-value">Термоизолированный внешний корпус:</div>
                </div>
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">
                    'двойной, с вентиляционным зазором и изоляцией
                  </div>
                </div>
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">
                    'двойной, с вентиляционным зазором и изоляцией
                  </div>
                </div>
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">
                    'двойной, с вентиляционным зазором и изоляцией
                  </div>
                </div>          
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">
                    'двойной, с вентиляционным зазором и изоляцией
                  </div>
                </div>
              </div>
              
               <div class="compare-main-row clearfix">
                <div class="compare-main-row-header">
                  <div class="compare-main-row-header-value">Материал внешнего корпуса:</div>
                </div>
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">
                    'углеродистая сталь, порошковое покрытие
                  </div>
                </div>
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">
                    'углеродистая сталь, порошковое покрытие
                  </div>
                </div>
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">
                    'углеродистая сталь, порошковое покрытие
                  </div>
                </div>          
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">
                    'углеродистая сталь, порошковое покрытие
                  </div>
                </div>
              </div>
              
              
            </div>
          </div>
          <!-- compare block 1 end -->
          
          <!-- compare block 2 start -->
          <div class="compare-main-block">
            <div class="compare-main-block-header">
              БАЗОВАЯ КОМПЛЕКТАЦИЯ (основные параметры)
            </div>
            
            <div class="compare-main-block-body">
              <div class="compare-main-row clearfix">
                <div class="compare-main-row-header">
                  <div class="compare-main-row-header-value">
                    Дымовой купол с зонтом
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="compare-main-row-cell included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
              </div>
              
              <div class="compare-main-row clearfix">
                <div class="compare-main-row-header">
                  <div class="compare-main-row-header-value">
                    Дымовой купол с двойными стенками и зонтом
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">доп. оборудование</div>
                </div>
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">доп. оборудование</div>
                </div>
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">доп. оборудование</div>
                </div>
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">доп. оборудование</div>
                </div>
              </div>
              
              <div class="compare-main-row clearfix">
                <div class="compare-main-row-header">
                  <div class="compare-main-row-header-value">
                    Трубы для установки в беседке
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">доп. оборудование</div>
                </div>
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">доп. оборудование</div>
                </div>
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">доп. оборудование</div>
                </div>
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">доп. оборудование</div>
                </div>
              </div>
              
              <div class="compare-main-row clearfix">
                <div class="compare-main-row-header">
                  <div class="compare-main-row-header-value">
                    Дверца с термостойким стеклом
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="compare-main-row-cell included">
                  <div class="compare-main-row-cell-value">(с адаптером)</div>
                </div>
                <div class="compare-main-row-cell included">
                  <div class="compare-main-row-cell-value">(с адаптером)</div>
                </div>
                <div class="compare-main-row-cell included">
                  <div class="compare-main-row-cell-value">(с адаптером)</div>
                </div>
                <div class="compare-main-row-cell included">
                  <div class="compare-main-row-cell-value">(с адаптером)</div>
                </div>
              </div>
              
              <div class="compare-main-row clearfix">
                <div class="compare-main-row-header">
                  <div class="compare-main-row-header-value">
                    Поворотные подставки, регулируемые по высоте
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">2 - быстросъемные</div>
                </div>
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">2 - быстросъемные</div>
                </div>
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">2 - быстросъемные</div>
                </div>
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">2 - быстросъемные</div>
                </div>
              </div>
              
              <div class="compare-main-row clearfix">
                <div class="compare-main-row-header">
                  <div class="compare-main-row-header-value">
                    Шашлычный модуль - шампура с деревянными ручками
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="compare-main-row-cell included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
              </div>
              
              <div class="compare-main-row clearfix">
                <div class="compare-main-row-header">
                  <div class="compare-main-row-header-value">
                    Шашлычный модуль - базовый
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="compare-main-row-cell included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
              </div>
              
              <div class="compare-main-row clearfix">
                <div class="compare-main-row-header">
                  <div class="compare-main-row-header-value">
                    Основная решетка для барбекю
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">со съемными прутьями</div>
                </div>
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">со съемными прутьями</div>
                </div>
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">со съемными прутьями</div>
                </div>
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">со съемными прутьями</div>
                </div>
              </div>
              
              <div class="compare-main-row clearfix">
                <div class="compare-main-row-header">
                  <div class="compare-main-row-header-value">
                    Вспомогательная решетка для барбекю
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="compare-main-row-cell included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
              </div>
              
              <div class="compare-main-row clearfix">
                <div class="compare-main-row-header">
                  <div class="compare-main-row-header-value">
                    Откидная верхняя крышка гриля
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="compare-main-row-cell included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
              </div>
              
              <div class="compare-main-row clearfix">
                <div class="compare-main-row-header">
                  <div class="compare-main-row-header-value">
                    Коптильня Lappigrill (55х35х25)
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="compare-main-row-cell not-included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell not-included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
              </div>
              
              <div class="compare-main-row clearfix">
                <div class="compare-main-row-header">
                  <div class="compare-main-row-header-value">
                    Набор для «томления» рыбы на огне (с отражателем и лотком)
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="compare-main-row-cell not-included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell not-included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
              </div>
              
              <div class="compare-main-row clearfix">
                <div class="compare-main-row-header">
                  <div class="compare-main-row-header-value">
                    Набор для «томления» рыбы на огне (с лотком)
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="compare-main-row-cell included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell not-included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell not-included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
              </div>
              
              <div class="compare-main-row clearfix">
                <div class="compare-main-row-header">
                  <div class="compare-main-row-header-value">
                    Доски для «томления» рыбы на огне
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="compare-main-row-cell included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell not-included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell not-included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
              </div>
              
              <div class="compare-main-row clearfix">
                <div class="compare-main-row-header">
                  <div class="compare-main-row-header-value">
                    Чугунная сковорода 340*70
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="compare-main-row-cell included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell not-included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell not-included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
              </div>
              
              <div class="compare-main-row clearfix">
                <div class="compare-main-row-header">
                  <div class="compare-main-row-header-value">
                    Чугунный контактный гриль
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="compare-main-row-cell not-included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell not-included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
              </div>
              
              <div class="compare-main-row clearfix">
                <div class="compare-main-row-header">
                  <div class="compare-main-row-header-value">
                    Вертел
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="compare-main-row-cell not-included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell not-included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell not-included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell not-included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
              </div>
              
              <div class="compare-main-row clearfix">
                <div class="compare-main-row-header">
                  <div class="compare-main-row-header-value">
                    Деревянные пристолья 
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">4 навесные</div>
                </div>
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">4 навесные</div>
                </div>
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">4 навесные</div>
                </div>
                <div class="compare-main-row-cell">
                  <div class="compare-main-row-cell-value">4 навесные</div>
                </div>
              </div>
              
              <div class="compare-main-row clearfix">
                <div class="compare-main-row-header">
                  <div class="compare-main-row-header-value">
                    Проливная столешница
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="compare-main-row-cell not-included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell not-included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell not-included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell not-included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
              </div>
              
              <div class="compare-main-row clearfix">
                <div class="compare-main-row-header">
                  <div class="compare-main-row-header-value">
                    Пристолья из нержавеющей стали
                    <div class="hint">
                      <div class="hint-icon">?</div>
                      <div class="hint-text">
                          двойной, с вентиляционным зазором и изоляцией                                                        <div class="hint-close"></div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="compare-main-row-cell not-included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell not-included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell not-included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
                <div class="compare-main-row-cell not-included">
                  <div class="compare-main-row-cell-value"></div>
                </div>
              </div>
              
              
              
            </div>
          </div>
          <!-- compare block 2 end -->
        </div>
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>