function lappigrill_setCookie(cname, cvalue, hours) {
  var d = new Date();
  d.setTime(d.getTime() + (hours * 60 * 60 * 1000));
  var expires = "expires=" + d.toUTCString();
  var additional_cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  document.cookie = additional_cookie;
}

function lappigrill_getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

function toggleProductComparison(product_id) {
  var lappigrill_compare_ids = lappigrill_getCookie('lappigrill_compare_ids') || '';
  var compare_ids = lappigrill_compare_ids ? lappigrill_compare_ids.split(',') : [];
  var new_ids = [];
  var product_image_src = '', product_name = '';
  if ($('.category-product.product-'+product_id).length > 0) {
    // archive page
    product_image_src = $('.category-product.product-'+product_id+' .category-product-img img').attr('src') || '';
    product_name = $('.category-product.product-'+product_id+' .category-product-header').text() || '';
  } else if ($('.grill-main-block-img').length > 0) {
    // single product page
    product_image_src = $('.grill-main-block-img .grill-main-block-img-block-inner img').attr('src') || '';
    product_name = $('.section-header-product-name').text() || '';
  }
  $('.product-fade .product-fade-img img').attr('src', product_image_src);
  $('.product-fade .product-name').html(product_name);

  if (compare_ids.indexOf(product_id.toString()) != -1) {
    // added to comparision
    for (var i = 0; i < compare_ids.length; i++) {
      if (compare_ids[i] != product_id) {
        new_ids.push(compare_ids[i]);
      }
    }
//    if (new_ids.length == 0) {
//      $('body').removeClass('comp-active');
//    }
    $('#product .compare-btn .compare-btn-text').html('Сравнение');
    fadeBlock.hide();
    fadeBlockRemoved.fadeIn(500);
    clearTimeout(timer);
    timer = setTimeout(function() {
        fadeBlockRemoved.fadeOut(500);
      },
      3000);

  } else {
    //removed from comparision
//    if (compare_ids.length == 0) {
//      $('body').addClass('comp-active');
//    }
    $('#product .compare-btn .compare-btn-text').html('В сравнении');
    new_ids = compare_ids;
    new_ids.push(product_id);
    fadeBlock.hide();
    fadeBlockAdded.fadeIn(500);

    clearTimeout(timer);
    timer = setTimeout(function() {
        fadeBlockAdded.fadeOut(500);
      },
      3000);
  }

  $('\
#product .compare-btn .compare-btn-amount, \
#header .comparision .comparision-amount, \
.product-fade .product-fade-text-total .comparision-count, \
.product-fade .product-fade-comparision .product-fade-comparision-amount\
').html(new_ids.length);

  lappigrill_setCookie('lappigrill_compare_ids', new_ids.join(','), 24 * 7);
}

var timer;
var fadeBlock, fadeBlockAdded, fadeBlockRemoved;

$(document).ready(function() {

  fadeBlock = $('.product-fade'),
    fadeBlockAdded = $('.product-fade.added'),
    fadeBlockRemoved = $('.product-fade.removed');
  var body = $('body'),
    layer = $('.modal-layer'),
    nav = $('.navbar-collapse');

  //mobile menu actions
  $('.navbar-toggle').click(function() {
    body.addClass('not-visible');
    layer.addClass('open');
    nav.slideDown();
  });

  $('.navbar-collapse .mobile-close').click(function() {
    nav.slideUp();
    layer.removeClass('open');
    body.removeClass('not-visible');
  });
  
  var rightSlider = $('#about .slider-inner .slider-container .sr'),
      mainSlider = $('#main-slider');

  //slider init
  mainSlider.slick({
    dots: true,
    speed: 1000,
    infinite: true,
    slidesToShow: 1,
    adaptiveHeight: false,
    autoplay: true,
    autoplaySpeed: 30000,
    responsive: [{
      breakpoint: 768,
      settings: "unslick"
    }]
  });
  
   function mainSliderHeight() {
    if(window.innerWidth > 991) {
      mainSlider.find('.slide-block').height(rightSlider.outerHeight());
    }   
  }
  mainSliderHeight(); 

  //vertical img for main slider
  
  
  function mainSliderResize() {
    var sliderImgs = $('#main-slider .slide-block img'),
        sliderImgCoef = 1;

    sliderImgs.each(function(i, val) {
      if (val.width / val.height < sliderImgCoef) {
        val.classList.add('vertical-img');
      }
    });
  }
  mainSliderResize();
  
  //iframe resize main page
  var videoBlock = $('.video-block-container');

  if ($(window).width() <= '767') {
    coef = 32 / 27;
    $('#categories .nav-panel-block').click(function() {
      $(this).parent().slideUp();
    });

    $('#years-main-blog-videos').slick({
      dots: false,
      speed: 1000,
      infinite: true,
      slidesToShow: 1,
      adaptiveHeight: false,
      autoplay: true,
      autoplaySpeed: 30000
    });
  } else {
    coef = 1.7;
  }

  var videoBlockWidth = videoBlock.width();
  var videoBlockHeight = videoBlock.width() / coef;
  videoBlock.find("iframe").height(videoBlockHeight);

  $('#video-main').slick({
    dots: false,
    speed: 1000,
    infinite: true,
    slidesToShow: 1,
    adaptiveHeight: false,
    responsive: [{
      breakpoint: 768,
      settings: {
        arrows: false
      }
    }]
  }).on('beforeChange', function(event, slick, currentSlide, nextSlide){
    var data = {"event":"command","func":"pauseVideo","args":""};
    var message = JSON.stringify(data);
    $(slick.$slides[currentSlide])[0].contentWindow.postMessage(message, '*');
  });

  function resizeProductVideoIframes() {
    //iframe resize product video page main video
    var vdm = $('.years-main-video'),
      videoCoef = 2.06;
    var vdmw = vdm.width(),
      vdmh = vdm.width() / videoCoef;
    vdm.find("iframe").height(vdmh);

    //iframe resize product video page another videos
    var pvpv = $(".video-blocks-block"),
      pvpvw = pvpv.width(),
      pvpvh = pvpvw / coef;
    pvpv.find("img").height(pvpvh);
  }

  //years popup iframe
  $('.blog-video a').magnificPopup({
    type: 'iframe',
    mainClass: 'mfp-fade',
    removalDelay: 160,
    preloader: false
  });

  var sliderCounter = $('#hidden-slider-count').val() - 1;

  //product slider main page init
  var productNavs = $('#fp-grills .grills-info-blocks');

  $.each(productNavs, function(i, val) {
    var mainSlider = $(val).find('.tab-pane.active .grills-product-info-img-slider .product-main-slider'),
        verticalSlider = $(val).find('.tab-pane.active .grills-product-info-slider .slider-container');
    //first main slider init
    mainSlider.slick({
      dots: false,
      speed: 1000,
      infinite: true,
      slidesToShow: 1,
      adaptiveHeight: false,
      autoplay: false,
      arrows:false,
      focusOnSelect: true,
      asNavFor: verticalSlider,
      swipe: false
    });
    //first vertical slider init
    verticalSlider.slick({
      dots: false,
      speed: 1000,
      infinite: true,
      slidesToShow: 4,
      slidesToScroll: 1,
      adaptiveHeight: true,
      vertical: true,
      swipe: false,
      focusOnSelect: true,
      asNavFor: mainSlider,
      responsive: [{
        breakpoint: 992,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
          vertical: false
        }
      }]
    });

    $('a[data-toggle="tab"]:not(.active)').on('shown.bs.tab', function (e) {
      var target = $(this).data('target'),
          mainSliderHidden = $(target).find('.grills-product-info-img-slider .product-main-slider'),
          verticalSliderHidden = $(target).find('.grills-product-info-slider .slider-container');

      if (mainSliderHidden.hasClass('slick-initialized') && verticalSliderHidden.hasClass('slick-initialized')) {
//        console.log('inited');
        mainSliderHidden.slick('slickGoTo', 0);
      } else {
        mainSliderHidden.slick({
          dots: false,
          speed: 1000,
          infinite: true,
          slidesToShow: 1,
          adaptiveHeight: false,
          autoplay: false,
          focusOnSelect: true,
          arrows:false,
          asNavFor: verticalSliderHidden
       });

        verticalSliderHidden.slick({
         dots: false,
         speed: 1000,
         infinite: true,
         slidesToShow: 4,
         slidesToScroll: 1,
         adaptiveHeight: true,
         focusOnSelect: true,
         vertical: true,
         swipe: false,
         asNavFor: mainSliderHidden,
         responsive: [{
           breakpoint: 992,
           settings: {
             slidesToShow: 4,
             slidesToScroll: 1,
             vertical: false
           }
         }]
       });
      }
    });
  });

    //grill slider
  $('.grill-main-block-slider').slick({
    dots: false,
    speed: 1000,
    infinite: true,
    slidesToShow: 5,
    adaptiveHeight: false,
    autoplay: true,
    autoplaySpeed: 30000,
    responsive: [{
      breakpoint: 1200,
      settings: {
        slidesToShow: 4,
        variableWidth: false,
        adaptiveHeight: false
      }
    },{
      breakpoint: 991,
      settings: {
        slidesToShow: 3,
        variableWidth: false,
        adaptiveHeight: false,
        arrows: false
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 2,
        variableWidth: false,
        adaptiveHeight: false,
        arrows: false
      }
    }]
  });

  $('.equipment-tabs a[data-toggle="tab"]:not(.active)').on('shown.bs.tab', function (e) {
      var target = $(this).attr('href'),
          EquipmentSlider = $(target).find('.grill-main-block-slider');

      if (EquipmentSlider.hasClass('slick-initialized')) {
        EquipmentSlider.slick('destroy');
        EquipmentSlider.slick({
          dots: false,
          speed: 1000,
          infinite: true,
          slidesToShow: 5,
          adaptiveHeight: false,
          autoplay: true,
          autoplaySpeed: 30000,
          responsive: [{
            breakpoint: 1200,
            settings: {
              slidesToShow: 4,
              variableWidth: false,
              adaptiveHeight: false
            }
          },{
            breakpoint: 991,
            settings: {
              slidesToShow: 3,
              variableWidth: false,
              adaptiveHeight: false,
              arrows: false
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 2,
              variableWidth: false,
              adaptiveHeight: false,
              arrows: false
            }
          }]
        }); 
      }
  });

  var productMainSliderIframe  = $('.grills-product-info-img-slider iframe');
  productMainSliderIframe.height(productMainSliderIframe.width() * 0.8);

  //add equipment slider init
  $('#addeq').slick({
    dots: false,
    speed: 1000,
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    adaptiveHeight: true,
    autoplay: false,
    centerMode: true,
    variableWidth: false,
    responsive: [{
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
        adaptiveHeight: false
      }
    }, {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        adaptiveHeight: false
      }
    }]
  });

  var panelBlocks = $('.nav-panel .nav-panel-block'),
    subPanelBlocks = $('.sub-panel .sub-panel-block');

  panelBlocks.click(function() {
    panelBlocks.removeClass('active');
    $(this).addClass('active');
  });

  subPanelBlocks.click(function() {
    subPanelBlocks.removeClass('active');
    $(this).addClass('active');
  });

  //categories compare functions
  $('.category-product-btn').click(function() {
    var parent = $(this).parent();
    if (parent.hasClass('addedToComp')){
        return;
    }
    var post_id = parseInt(parent.data('post_id'));
    parent.addClass('addedToComp');
    toggleProductComparison(post_id);
  });

  $('.product-fade .comparision-close').click(function() {
    fadeBlock.fadeOut(500);
  });

  $('.compare-remove').click(function() {
    var parent = $(this).parent();
    var post_id = parseInt(parent.data('post_id'));
    parent.removeClass('addedToComp');
    toggleProductComparison(post_id);
  });

  //model type toggle
  $('.nav-regions-block-model').click(function() {
    $('.nav-regions-block-model').removeClass('active');
    $(this).addClass('active');
  });

  $('.nav-models-block-model').click(function() {
    $('.nav-models-block-model').removeClass('active');
    $(this).addClass('active');
  });

  //hint product grill
  $('.hint-icon').click(function(e) {
    $('.hint').removeClass('active')
    $(this).parent('.hint').addClass('active');
    hintsCheck();
    
    if ($(window).width() <= '767') {
      layer.addClass('open');
      body.addClass('not-visible');
    }
  });

  $('.hint-close').click(function() {
    $(this).closest('.hint').removeClass('active');

    if ($(window).width() <= '767') {
      layer.removeClass('open');
      body.removeClass('not-visible');
    }
  });




  //grill slider gallery init
  $carousel = $('.grill-main-block-slider, .grill-main-block-img-block-inner');
  $carousel.magnificPopup({
    type: 'image',
    delegate: 'a:not(.slick-cloned)',
    gallery: {
      enabled: true
    }
  });

  $basic_carousel = $('.grill-basic-block-slider');
  $basic_carousel.magnificPopup({
      type: 'image',
      delegate: 'a:not(.slick-cloned)',
      gallery: {
          enabled: true
      }
  });


  $additional_carousel = $('.grill-additional-block-slider');

  $additional_carousel.magnificPopup({
      type: 'image',
      delegate: 'a:not(.slick-cloned)',
      gallery: {
          enabled: true
      }
  });
  
  $additional_carousel.magnificPopup({
      type: 'image',
      delegate: 'a:not(.slick-cloned)',
      gallery: {
          enabled: true
      }
  });

  $('#product-gallery-slider').slick({
    dots: false,
    speed: 1000,
    infinite: false,
    slidesToShow: 1,
    adaptiveHeight: false,
    autoplay: true,
    autoplaySpeed: 30000,
    asNavFor: '.product-gallery-slider-thumbnails'
  });

  $('.product-gallery-slider-thumbnails').slick({
    rows: 3,
    slidesPerRow: 5,
    asNavFor: '#product-gallery-slider',
    dots: false,
    infinite: true,
    arrows: false,
    responsive: [{
        breakpoint: 1200,
        settings: {
          rows: 4,
          slidesPerRow: 4
        }
      },
      {
        breakpoint: 992,
        settings: {
          rows: 5,
          slidesPerRow: 3
        }
      }
    ]
  });

  //slider thumbnail scroll
  var $photoSlider = $("#product-gallery-slider"),
    $thumbnails = $(".product-gallery-slider-thumbnails"),
    $thumbnail = $thumbnails.find(".product-gallery-slider-thumbnails-thumbnail"),
    $photo = $photoSlider.find('.slide'),
    killit = false;

  $photoSlider.on(' afterChange', function(event, slick, currentSlide) {
    var parentBlock = $(slick.$slides.get(currentSlide)).data('index');
    var currentBlock = $thumbnail[parentBlock - 1];

    $thumbnail.removeClass('active');
    currentBlock.classList.add('active');
  });

  $thumbnail.on("click", function(e) {
    if (!killit) {
      e.stopPropagation();
      var idx = $(this).data("thumb");
      $photoSlider.slick("goTo", idx - 1);
      $thumbnail.removeClass('active');
      $(this).addClass('active');
    }
  });

  $thumbnails
    .on("beforeChange", function() {
      killit = true;
    }).on("afterChange", function() {
      killit = false;
    });




  $('#nav-mobile-header').click(function() {
    $(this).next().slideToggle();
  });


  //accordeon product mobile slider init
  $('#product-photo-panel').click(function() {
    if (!$('#product-photo').html().trim()) {
      $('#product-photo').html(product_template_parts_html['mobile']['product-photo']);
    }
    setTimeout(function() {
      $('#product-mobile-slider').slick({
        dots: false,
        rows: 4,
        slidesPerRow: 2,
        speed: 1000,
        infinite: true,
        slidesToShow: 1,
        adaptiveHeight: false,
        autoplay: true,
        autoplaySpeed: 30000
      });

      var imgMobileGalleryBlock = $('#product-photo .grill-main-block-slider .slide');
      var imgMobileGalleryBlockWidth = imgMobileGalleryBlock.innerWidth();
      imgMobileGalleryBlock.height(imgMobileGalleryBlockWidth);
    }, 0);


  });

  $('#product-video-panel').click(function() {
    if (!$('#product-video').html().trim()) {
      $('#product-video').html(product_template_parts_html['mobile']['product-video']);
    }
    setTimeout(function() {
      var pvpv = $(".video-blocks-mobile"),
        pvpvw = pvpv.width(),
        pvpvh = pvpvw / coef;
      pvpv.find("img").height(pvpvh);

      $('.video-blocks-mobile a').magnificPopup({
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false

      });
    }, 0);
  });

  $('#product-scheme-panel').click(function() {
    if (!$('#product-scheme').html().trim()) {
      $('#product-scheme').html(product_template_parts_html['mobile']['product-scheme']);
    }
  });

  $('#main-blog-slider').slick({
    dots: false,
    speed: 1000,
    infinite: true,
    slidesToShow: 3,
    adaptiveHeight: false,
    autoplay: true,
    autoplaySpeed: 30000,
    responsive: [{
        breakpoint: 992,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  });
  
  //product main page mobile margin for nav
  var grillProductHeaders = $('.grills-product-nav');

  if($(window).width() < 992) {
    $.each(grillProductHeaders, function(i, el) {
      var currentNavHeight = $(el).height() + 20;


      $(el).parents('.grills-info-blocks').find('.grills-product-info-text.mobile').css('margin-top', currentNavHeight);
    });
  }

  //product zoom
  $(".grills-product-info-img-slider .slider-container .slide").zoom({
    magnify: 1.5,
    onZoomIn: function() {
      $(this).parent().find('img:not(.zoomImg)').css('opacity', 0);
    },
    onZoomOut: function() {
      $(this).parent().find('img:not(.zoomImg)').css('opacity', 1);
    }
  });

  //mobile filter video menu events
  $('.mobile-filter-menu').click(function() {
    $(this).toggleClass('isOpen');
  });

//  $('body').click(function(e) {
//    var elem = $(e.target).parents('.mobile-filter-menu.isOpen');
//
//    if (elem.length <= 0) {
//      $('.mobile-filter-menu').removeClass('isOpen');
//    } else {
//      $('.mobile-filter-menu').addClass('isOpen');
//    }
//  });

  var filterVideo = $('.mobile-filter-menu li');
  filterVideo.click(function() {
    filterVideo.removeClass('current');
    $(this).addClass('current');
  })

  //compare page set slider width by elem amount
  var compCols = $('.cd-products-columns');
  var compBlockWidth = compCols.find('.product').width();
  var compBlockAmount = compCols.find('.product').length;
  compCols.width(compBlockWidth * compBlockAmount);

  var dataHeaders = $('.ch-header'),
    dataSubheaders = $('.ch-subheader');

  $.each(dataHeaders, function(index, item) {
    var currentDataHeaders = $(item).data('header');
    var listsData = $('.cd-features-list[data-header=' + currentDataHeaders + ']');

    if (listsData.length > 0) {
      var listDataPos = listsData.position().top;
      $(item).css('top', listDataPos);
    }

  });

  $.each(dataSubheaders, function(index, item) {
    var currentDataHeaders = $(item).data('subheader');
    var listsData = $('.cd-features-list li[data-subheader=' + currentDataHeaders + ']');

    if (listsData.length > 0) {
      var listDataPos = listsData.position().top;
      $(item).css('top', listDataPos);
    }

  });

  //product image gallery popup
  $('#product-photo').magnificPopup({
    delegate: 'a',
    type: 'image',
    gallery: {
      enabled: true
    },
    image: {
      titleSrc: 'title'
    }
  });

  resizeProductVideoIframes();

  //popups for cart
  var popupLayout = $('.popup-layout'),
      popupOneClick = $('.one-click-popup'),
      popupCart = $('.cart-popup'),
      oneClickBtn = $('.btns-buy .buy-btn.oneclick'),
      cartPopupBtn = $('.btns-buy .btn-to-cart'),
      popup = $('.popup'),
      removeCell = $('.cart-popup .cell-remove'),
      dropdown = $('.checkout-dropdown'),
      checkoutDrop = $('.checkout-dropdown');
  
  //cart checkout dropdowns open  
  checkoutDrop.click(function(e) {
    var target = $(e.target);
    checkoutDrop.removeClass('isOpen');  
    $(this).toggleClass('isOpen');

    if ($(e.target).is('.checkout-dropdown-row:not(.active)')) {
      $(e.target).parents('.checkout-dropdown').find('.checkout-dropdown-row').removeClass('active');
      $(e.target).addClass('active');
      
      var input_name = $(e.target).parents('.checkout-dropdown-wrapper').data('input');
      var input_value = $(e.target).data('value');
      $(this).parents('form').find('[name='+input_name+']').val(input_value);
      if (input_name=='shipping_method') {
        ajaxCart_changeShippingMethod($(e.target).data('code'));
      }
      
      $(this).removeClass('isOpen');
    }
  });
  
  var cartDropdowns = $('.checkout-dropdown'),
      cartDropdownsStatuses = 140 + cartDropdowns.length;

  $.each(cartDropdowns, function(i, elem) {
    $(elem).css('z-index', cartDropdownsStatuses - i);
  });
  
  //click on body
  $('body').click(function(e) {
    var elem = $(e.target).parents('.popup, .checkout-dropdown, .btns-buy, .mobile-filter-menu.isOpen, .hint');
//    console.log(elem);
    if (elem.length <= 0) {
      popupLayout.fadeOut(100);
      popup.fadeOut(500);
      body.removeClass('not-visible');
      dropdown.removeClass('isOpen');
      $('.mobile-filter-menu').removeClass('isOpen');
      $('.hint').removeClass('active');
    }

    //cart popup product remove
    if ($(e.target).hasClass('product-remove') || $(e.target).hasClass('product-remove-mobile')) {
      var row_product = $(e.target).parents('.row-product');
      var product_id = row_product.data('product-id');
      ajaxCart_removeProduct(product_id);
      row_product.remove();
      recountHeaderCartAmount();
      cartPopupCalc();
    }
  });
  
  popup.find('.popup-close, .popup-btn.continue').click(function(){
    popupLayout.fadeOut(100);
    popup.fadeOut(500);
    body.removeClass('not-visible');
  });

  //different popup open event
  oneClickBtn.click(function(e) {
    e.preventDefault();
    var product_id = $(this).data('product-id');
    var product_name = $(this).data('product-name');
    $('.one-click-order-form').show();
    $('.one-click-order-thankyou').hide();
    $('.one-click-popup .popup-header').html(product_name);
    $('.one-click-order-form')[0].reset();
    $('.one-click-order-form .invalid').removeClass('invalid');
    $('.one-click-order-form [name=product_id]').val(product_id);
    popupResize();
    body.addClass('not-visible');
    popupLayout.fadeIn(100);
    popupOneClick.fadeIn(500);
  });

  cartPopupBtn.on('click', function(e) {
    e.preventDefault();
    body.addClass('not-visible');
    popupLayout.fadeIn(100);
    popupCart.fadeIn(500);
    popupCart_addProductRow(this);
    popupResize();
    cartPopupCalc();
  });

  //cart and popup inc/dec amount
  $(document).on('click', '.woocommerce-cart-form .product-quantity .inc', function(event) {
    var input = $(this).parent().find('input'),
        currentVal = parseFloat(input.val()),
        newVal = currentVal + 1;

    input.val(newVal);
    ajaxCart_addProduct($(this).parents('.cart_item').data('product-id'));
    recountHeaderCartAmount();
    cartCalc();
  });
  
  $(document).on('click', '.woocommerce-cart-form .product-quantity .decr', function(event) {
    var input = $(this).parent().find('input'),
        currentVal = parseFloat(input.val()),
        newVal = currentVal - 1;

    if (currentVal > 1) {
      input.val(newVal);
      ajaxCart_changeQuantity($(this).parents('.cart_item').data('product-id'), newVal);
      recountHeaderCartAmount();
      cartCalc();
    }
  });
  
  $(document).on('input', '.woocommerce-cart-form .product-quantity input', function(event) {
    var currentValue = event.target.value;

    if (currentValue.length < 1 || currentValue == 0) {
      $(event.target).val(1);
      ajaxCart_changeQuantity($(this).parents('.cart_item').data('product-id'), 1);
    }

    cartCalc();
    recountHeaderCartAmount();
  });

  $(document).on('click', '.woocommerce-cart-form .product-remove-wrapper a.remove', function(event) {
    event.preventDefault();
    $(this).parents('td').children('a').click();
  });
  
  // этот триггер срабатывает после ответа AJAX при удалении товара из корзины на странице /cart
  $(document.body).on('updated_wc_div', function() {
    recountHeaderCartAmount();
    cartCalc();
  });

  $(document).on('input', '.cart-popup .cell-quantity input', function(event) {
    var currentValue = event.target.value;

    if (currentValue.length < 1 || currentValue == 0) {
      $(event.target).val(1);
      ajaxCart_changeQuantity($(this).parents('.row-product').data('product-id'), 1);
    }

    cartPopupCalc();
    recountHeaderCartAmount();
  });

  $(document).on('keypress', '.cart-popup .cell-quantity input, .cart_item .quantity input', function(e) {
    if (e.charCode && (e.charCode < 48 || e.charCode > 57)){
      return false;
    }
  });

  $(document).on('change', '.cart-popup .cell-quantity input, .cart_item .quantity input', function() {
    ajaxCart_changeQuantity($(this).parents('.row-product, .cart_item').data('product-id'), this.value);
  });

  $(document).on('click', '.cart-popup .quantity-inc', function() {
    var input = $(this).parent().find('input'),
        currentVal = parseFloat(input.val()),
        newVal = currentVal + 1;

    input.val(newVal);
    ajaxCart_addProduct($(this).parents('.row-product').data('product-id'));
    recountHeaderCartAmount();
    cartPopupCalc();
  });
  
  $(document).on('click', '.cart-popup .quantity-decr', function() {
    var input = $(this).parent().find('input'),
        currentVal = parseFloat(input.val()),
        newVal = currentVal - 1;

    if (currentVal > 1) {
      input.val(newVal);
      ajaxCart_changeQuantity($(this).parents('.row-product').data('product-id'), newVal);
      recountHeaderCartAmount();
      cartPopupCalc();
    }
  });
  
  //cart img popup animate
  $(document).on('mouseenter', '.cart-popup .product-img', function() {
    $(this).find('.product-img-hover').fadeIn(600);
  });
  $(document).on('mouseleave', '.cart-popup .product-img', function() {
    $(this).find('.product-img-hover').fadeOut(600);
  });

  //cart popup calc total
  function cartPopupCalc() {
    var totalCart = $('.cart-popup-nav .total .amount'),
        productsInCart = $('.cart-popup .row-product'),
        cartSum = 0,
        startValue = parseFloat($(totalCart).html().replace(/\s/g, ''));

    $.each(productsInCart, function(i, elem) {
      var price = parseFloat($($(elem).find('.cell-price .amount')).html().replace(/\s/g, '')),
          amount = parseFloat($(elem).find('.cell-quantity input').val());
      
        cartSum += price * amount;        
    });

    animateNumber(totalCart, startValue, cartSum, 1000);
  }

  //cart calc total
  function cartCalc() {
    var totalCart = $('.woocommerce-cart-form .cart-additional .total-cart-sum-amount'),
        productsInCart = $('.woocommerce-cart-form tr.cart_item'),
        cartSum = 0;
    if (totalCart.length > 0) {
      var startValue = parseFloat($(totalCart).html().replace(/\s/g, ''));
    }
        
    $.each(productsInCart, function(i, elem) {
      var price = parseFloat($($(elem).find('.product-price-amount')).html().replace(/\s/g, '')),
          amount = parseFloat($(elem).find('.product-quantity input').val());
      
      cartSum += price * amount;
    });
    
    animateNumber(totalCart, startValue, cartSum, 1000);
  }
  
  body.find('.woocommerce').length  > 0 ? cartCalc() : cartPopupCalc();
  
  //animate number value
  function animateNumber(elem, animateFrom, animateTo, time) {
    var currentVal = animateFrom,
        iterationTime = 50,
        iterationAmount = time / iterationTime;

    if (animateFrom < animateTo ) {
      valueTo = (animateTo - animateFrom) / iterationAmount;

      var t = setInterval(function() {
        if ( currentVal < animateTo) {
          currentVal += valueTo;
          if (currentVal >= animateTo) {
            currentVal = animateTo;
            clearInterval(t);
          }
          $(elem).text(numberSplit(Math.round(currentVal)));
        }
        }, iterationTime);
      } else {
        valueTo = (animateFrom - animateTo) / iterationAmount;

        var t = setInterval(function() {
          if ( currentVal > animateTo) {
            currentVal -= valueTo;
            if (currentVal <= animateTo) {
              currentVal = animateTo;
              clearInterval(t);
            }
            $(elem).text(numberSplit(Math.round(currentVal)));
          }
        }, iterationTime);
      }
  }
  
  //split number for every 3 elems
  function numberSplit(number) {
    var arr = (number + '').split( /(?=(?:\d{3})+(?!\d))/ ).join(' ');
    return arr;
  }
  
  //match height for all products content in a row
  function matchPostsHeight() {
    var ww = window.innerWidth || document.body.scrollWidth || $(window).width();
    var items_per_row = ww > 991 ? 3 : ww > 767 ? 2 : 1;
    var row_block_heights = {};
    var current_row = 0;
    
    if (items_per_row > 1) {
        $('.category-product').each(function(i, el) {
            if ((i % items_per_row) === 0) {
                current_row++;
                row_block_heights[current_row] = {};
            }
          
            var headers = $(el).find('.category-product-link'),
                content = $(el).find('.category-product-params');
            
            if (headers.height() > (row_block_heights[current_row].headers || 0))
                row_block_heights[current_row].headers = headers.outerHeight();
            if (content.height() > (row_block_heights[current_row].content || 0))
                row_block_heights[current_row].content = content.height();
        });
        
        current_row = 0;
        $('.category-product').each(function(i, el) {
            if ((i % items_per_row) === 0) {
                current_row++;
            }
            $(el).find('.category-product-link').height(row_block_heights[current_row].headers);
            $(el).find('.category-product-params').height(row_block_heights[current_row].content);
        });
    
    }
  }
  matchPostsHeight();
  
  //product popup hint
  function hintsCheck() {
    var descrBlock = $('.grill-main-block-description-list'),
        descrBlockHeight = descrBlock.height(),
        hintsList = $('.grill-main-block-description-list .hint');
    
    if (descrBlock.length > 0) {
      var descrBlockBottom = descrBlock.offset().top + descrBlockHeight;
      
      $.each(hintsList, function(i, el) {
        var hintBlock = $(el).find('.hint-text'),
            hintBottom = hintBlock.offset().top + hintBlock.outerHeight();

        if (hintBottom > descrBlockBottom) {
          $(el).find('.hint-text').addClass('hint-bottom');
        }
      });
    }
  }
  
  //cart functions 
  //cart img hover popup
  $('td.product-name .product-img').hover(
    function() {
      $(this).find('.product-thumbnail-popup').fadeIn(600);
    },
    function() {
      $(this).find('.product-thumbnail-popup').fadeOut(600);
    }
  ); 
  

  function popupCart_addProductRow(cart_button) {
    var product_id = $(cart_button).data('product-id'),
        product_name = $(cart_button).data('product-name'),
        product_price = $(cart_button).data('product-price'),
        product_sku = $(cart_button).data('product-sku'),
        product_image = $(cart_button).data('product-image');
    ajaxCart_addProduct(product_id);
    if ($('.cart-popup form .cart-row.row-product[data-product-id='+product_id+']').length > 0) {
        var qty = parseInt($('.cart-popup form .cart-row.row-product[data-product-id='+product_id+'] .cell-quantity input').val()) || 0;
        $('.cart-popup form .cart-row.row-product[data-product-id='+product_id+'] .cell-quantity input').val(qty+1);
    } else {
        var html = "<div class=\"cart-row row-product clearfix\" data-product-id=\""+product_id+"\">\n\
<div class=\"cell cell-name\">\n\
  <div class=\"product-header-mobile\">Наименование товара</div>\n\
  <div class=\"product-img\">\n\
   <img src=\"/wp-content/themes/grill/img/cart-popup-img.png\" alt=\"\">\n\
   \n\
   <div class=\"product-img-hover\">\n\
     <img src=\""+product_image+"\" alt=\"\">\n\
   </div>\n\
  </div>\n\
  "+product_name+"\n\
</div>\n\
<div class=\"cell cell-artic\">"+(product_sku || "&nbsp;")+"</div>\n\
<div class=\"cell cell-quantity\">\n\
  <div class=\"product-header-mobile\">Количество</div>\n\
  <div class=\"cell-quantity-wrapper\">\n\
    <span class=\"quantity-decr\"></span>\n\
    <input type=\"text\" value=\"1\">\n\
    <span class=\"quantity-inc\"></span>\n\
  </div>\n\
</div>\n\
<div class=\"cell cell-artic mobile\">\n\
  <div class=\"product-header-mobile\">Артикул</div>\n\
  "+(product_sku || "&nbsp;")+"\n\
</div>\n\
<div class=\"cell cell-price\">\n\
  <div class=\"product-header-mobile\">Цена</div>\n\
  <div class=\"amount\">"+product_price+"</div> руб\n\
</div>\n\
<div class=\"cell cell-remove\">\n\
  <img class=\"product-remove\" src=\"/wp-content/themes/grill/img/cart-popup-remove.png\" alt=\"\">\n\
  <div class=\"product-remove-mobile\">Убрать</div>\n\
</div>\n\
</div>";
        $(html).insertBefore('.cart-popup form .cart-popup-nav');
    }
    recountHeaderCartAmount();
    
  }

  function recountHeaderCartAmount() {
    var cart_amount_element = $('.add-props-header .cart-amount');
    var cart_count = 0, quantity_inputs;
    if ($('.woocommerce-cart-form').length>0) {
      quantity_inputs = $('.cart .cart_item .quantity input');
    } else {
      quantity_inputs = $('.cart-popup .row-product .cell-quantity input');
    }
    quantity_inputs.each(function(i, el) {
      cart_count += parseInt(el.value) || 0;
    });
    cart_amount_element.html(cart_count);
  }

  function ajaxCart_addProduct(product_id, callback) {
    $.ajax({
      url: '/?lappi_cart_ajax_add_product=1',
      data: {
        'add-to-cart': product_id
      },
      method: 'post',
      success: (typeof callback == 'function') && callback
    });
  }

  function ajaxCart_changeQuantity(product_id, quantity, callback) {
    $.ajax({
      url: '/wp-admin/admin-ajax.php',
      data: {
        action: 'cart_ajax_change_quantity',
        product_id: product_id,
        quantity: quantity
      },
      method: 'post',
      success: (typeof callback == 'function') && callback
    });
  }

  function ajaxCart_removeProduct(product_id, callback) {
    $.ajax({
      url: '/wp-admin/admin-ajax.php',
      data: {
        action: 'cart_ajax_remove_product',
        product_id: product_id
      },
      method: 'post',
      success: (typeof callback == 'function') && callback
    });
  }

  function ajaxCart_changeShippingMethod(method) {
    $.ajax({
      url: '/wp-admin/admin-ajax.php',
      data: {
        action: "set_shipping_method",
        shipping_method: method
      },
      method: 'post',
      success: (typeof callback == 'function') && callback
    });
  }


  function validatePhone(phone) {
    var phone_without_spaces = phone.replace(/\s/g, '');
    var re = /^\+?\d*(\(\d+\))?(\d+-?)*\d$/;
    return (phone_without_spaces != '') && re.test(phone_without_spaces);
  }

  function validateEmail(email) {
    var re = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z]{2,}$/;
    return (email != '') && re.test(email);
  }

  $('.one-click-order-form-submit').click(function(e) {
    e.preventDefault();
    
    if ($(this).hasClass('not-allowed')) {
      return false;  
    } 
    
    $('.one-click-order-form .invalid').removeClass('invalid');
    if (!validateOneClickForm()) {
      return false;
    } 
    
    $(this).addClass('not-allowed');
    $('.popup-preloader').show();
    var form_data = $('.one-click-order-form').serialize();

    $.ajax({
      url: '/wp-admin/admin-ajax.php',
      data: {
        action: 'one_click_order',
        form_data: form_data
      },
      method: 'post',
      dataType: 'json',
      success: function(response) {
        $('.popup-preloader').hide();
        if (response.success){
          $('.one-click-order-form').hide();
          $('.one-click-order-thankyou .order-number').html('#'+response.order_id);
          $('.one-click-order-thankyou').show();
          $('.one-click-order-form-submit').removeClass('not-allowed');
          popupResize();
        }
      },
      error: function() {
        
      }
    });
    return false;
  });

  function validateOneClickForm() {
    var form = $('.one-click-order-form');
    var product_id = form.find('[name=product_id]');
    var name = form.find('[name=name]');
    var phone = form.find('[name=phone]');
    var comment = form.find('[name=comment]');
    var accept_terms = form.find('[name=accept_terms]');

    var valid = true;
    if (product_id.val()=='') {
//      product_id.addClass('invalid');
      valid = false;
    }
    if (name.val()=='') {
      name.addClass('invalid');
      valid = false;
    }
    if (!validatePhone(phone.val())) {
      phone.addClass('invalid');
      valid = false;
    }
    if (accept_terms.prop('checked')==false) {
      accept_terms.parents('.popup-checkbox').find('label').addClass('invalid');
      valid = false;
    }
    return valid;
  }
  
  $('.cart-checkout-form .checkout-submit').click(function(e) {
    e.preventDefault();
    
    if ($(this).hasClass('not-allowed')) {
      return false;  
    } 
    
    $('.cart-checkout-form .invalid').removeClass('invalid');
    if (!validateCheckoutForm()) {
      return false;
    }
    
    $(this).addClass('not-allowed');
    $('.popup-preloader').show();
    var form_data = $('.cart-checkout-form').serialize();
    $.ajax({
      url: '/wp-admin/admin-ajax.php',
      data: {
        action: 'process_checkout',
        form_data: form_data
      },
      method: 'post',
      dataType: 'json',
      success: function(response) {
        $('.popup-preloader').hide();
        if (response.success && response.thank_you_page) {
          location.href = response.thank_you_page;
        }
      },
      error: function() {
        
      }
    });
    return false;
    
  });
  
  function validateCheckoutForm() {
    var form = $('.cart-checkout-form');
    var name = form.find('[name=name]');
    var phone = form.find('[name=phone]');
    var email = form.find('[name=email]');
    var city = form.find('[name=city]');
    var comment = form.find('[name=comment]');
    var accept_terms = form.find('[name=accept_terms]');
    var payment_method = form.find('[name=payment_method]');
    var shipping_method = form.find('[name=shipping_method]');

    var valid = true;
    if (name.val()=='') {
      name.addClass('invalid');
      valid = false;
    }
    if (!validatePhone(phone.val())) {
      phone.addClass('invalid');
      valid = false;
    }
    if (!validateEmail(email.val())) {
      email.addClass('invalid');
      valid = false;
    }
    if (city.val()=='') {
      city.addClass('invalid');
      valid = false;
    }
    if (payment_method.val()=='') {
      $('.cart-checkout-form .checkout-dropdown.dropdown-payment').addClass('invalid');
      valid = false;
    }
    if (shipping_method.val()=='') {
      $('.cart-checkout-form .checkout-dropdown.dropdown-delivery').addClass('invalid');
      valid = false;
    }
    if (accept_terms.prop('checked')==false) {
      accept_terms.parents('.cart-agreement-checkbox').find('label').addClass('invalid');
      valid = false;
    }
    return valid;
  }
  
  //resize popup by max height
  function popupResize() {
    var popups = $('.popup');
    
    $.each(popups, function(i, el) {
      $(el).find('.popup-wrapper').css({
        height: 'auto'
      });
      $(el).find('.popup-wrapper').height($(el).height());
    });
  }
  
  popupResize();
  
  $( window ).resize(function() {
    popupResize();
    mainSliderHeight(); 
    mainSliderResize();
  });

  //sub panel click
  $('.sub-panel-block').click(function() {
    var tab;

    if ($(window).width() > '767') {
      tab = $(this).data('tab');
      $('.product-page-content-tab').hide();
    }

    location.hash = $(this).data('hash');
    if ($('.product-page-content-tab.' + tab).length > 0) {
      $('.product-page-content-tab.' + tab).show();
      return;
    }

    if (typeof product_template_parts_html !== 'undefined') {
      var html = product_template_parts_html[tab] || '';
      $(html).insertAfter('.grill-main-block');
    }
    if (tab == 'product-gallery') {
      $('#product-gallery-slider').slick({
        dots: false,
        speed: 1000,
        infinite: false,
        slidesToShow: 1,
        adaptiveHeight: false,
        autoplay: true,
        autoplaySpeed: 30000,
        asNavFor: '.product-gallery-slider-thumbnails'
      });

      $('.product-gallery-slider-thumbnails').slick({
        slidesToScroll: 1,
        slidesToShow: 10,
        asNavFor: '#product-gallery-slider',
        dots: false,
        focusOnSelect: true
      });

      //product image gallery popup
      $('#product-tab-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        gallery: {
          enabled: true
        },
      image: {
        titleSrc: 'title'
      }
      });

        //product photo gallery resize
      var imgGalleryBlock = $('#product-tab-gallery .grill-main-block-slider .slide');
      var imgGalleryBlockWidth = imgGalleryBlock.width();
      imgGalleryBlock.height(imgGalleryBlockWidth);
    }
    if (tab == 'video') {
      resizeProductVideoIframes();
      $('.video-main.years-main-video a').magnificPopup({
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false

      });

      $('.video-blocks .video-blocks-block a').magnificPopup({
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false

      });
    }


  });
  
  if (location.hash) {
    if (location.hash=='#eq-add' || location.hash=='#eq-base') {
      if (location.hash=='#eq-add') {
        $('.equipment-tabs .nav-tabs li a[href=\\'+location.hash+']').click();
      }
    } else {
      var tab = $('.sub-panel-block[data-hash='+location.hash.replace('#', '')+']');
      if (tab.length) {
          tab.click();
      }
    }
  }
  
  $('.equipment-tabs .nav-tabs li a').click(function() {
      location.hash = $(this).attr('href');
  });
  

  $('#product .compare-btn').click(function() {
    toggleProductComparison(product_id);
    $(this).toggleClass('in-compare');
  })
  
  //compare page scripts
  $('.filter-btn.show-chars').click(function() {
    $('.filter-btn.show-chars').removeClass('active');
    $(this).addClass('active');
    
    if ($(this).hasClass('compare-all')) {
      $('.compare-main-row').removeClass('equal');
    }
    if ($(this).hasClass('compare-diff')) {
      compareDiff();
    }
  });
  
  function matchCompareCells() {
    var ww = window.innerWidth || document.body.scrollWidth || $(window).width();
    var row_block_heights = {};
    var current_row = 0;
    
    $('.compare-main-row').each(function(i, el) {
      row_block_heights[current_row] = {};

      var cells = $(el).find('.compare-main-row-header-value, .compare-main-row-cell-value');
      
      cells.each(function(i, el) {
        $(el).css({'height': 'auto'});
        if ($(el).outerHeight() > (row_block_heights[current_row].cells || 0)) {
          row_block_heights[current_row].cells = $(el).outerHeight();
        };
      });         
      current_row++;
    });
        
    current_row = 0;
    $('.compare-main-row').each(function(i, el) {
      $(el).find('.compare-main-row-header-value, .compare-main-row-cell-value').height(row_block_heights[current_row].cells);
      current_row++;
    });
  }
  matchCompareCells();
  
  //z-indexes for each hint
  var compareHints = $('.compare-main-block .hint'),
      compareHintsIndexes = 900 + compareHints.length;

  $.each(compareHints, function(i, elem) {
    $(elem).css('z-index', compareHintsIndexes - i);
  });
  
  //delete column(one item)
  $('.compare-main-row .row-name-del').click(function() {
    var row = $(this).parents('.compare-main-row').find('.compare-main-row-cell'),
        currentName = $(this).parent().find('.row-name-text').html(),
        rows = $('.compare-main-row'),
        compareIndex;
    //finding index of elem which needto delete
    $.each(row, function(i, el) {
      if($(el).find('.row-name-text').html() === currentName) {
        compareIndex = i;
      }
    });
    //removing all elems by index
    $.each(rows, function(i, el) {
      $(el).find('.compare-main-row-cell')[compareIndex].remove();
    });
//    compareTableWidthFix();
    matchCompareCells();
  }); 
  
  //table width by cells and cells with adaptive by table free space without headers
//  function compareTableWidthFix() {
//    var row = $('.compare-main-row.row-name'),
//        rows = $('.compare-main-row'),
//        rowHeader = row.find('.compare-main-row-header'),
//        rowCells = row.find('.compare-main-row-cell'),
//        rowWidth = 0,
//        cellWidth = ((100  - Math.round(parseFloat(rowHeader.css('width')) / row.outerWidth() * 100)) / rowCells.length).toFixed(3) + '%';
//
//    $.each(rows, function(i, el) {
//      $(el).find('.compare-main-row-cell').css({
//        'width': cellWidth
//      });
//    });
//    $.each(rowCells, function(i,el) {
//      rowWidth += $(el).outerWidth(); 
//    });
//    
//    rowWidth += rowHeader.outerWidth();
//    $('.compare-main').width(rowWidth);
//  }  
  
  //del all items
  $('.compare-filters .compare-del').click(function() {
    $('.compare-main-row-cell').remove();
    matchCompareCells();
  });
  
  //show differents
  
  function compareDiff() {
    var rows = $('.compare-main-row:not(.row-name)');
    
    $.each(rows, function(i, el) {
      var cells = $(el).find('.compare-main-row-cell'),
          firstCell = $(cells[0]).text();

      $.each(cells, function(i, el) {
        if ($(el).text() !== firstCell) {
          $(el).parents('.compare-main-row').addClass('equal');
          return false;
        }
      });
    })
  } 

  
  //compare page scripts end
  


  jQuery('.nav-regions-block-model').click(function() {
    clearTextField();
    setHeaderTitle(jQuery(this).text());

    catClass = getActiveDataAttr();

    findItemByClass();
    filterAll();

  });

  //add event listener for mobile

  jQuery('.mobile-item').click(function() {
    clearTextField();
    setHeaderTitle(jQuery(this).text());
    catClass = getActiveMobileAttr();
    findItemByClass();
    filterAll();

  });

  var items = jQuery(".dilersp-region-row");

  for (var i = 0; i < items.length; i++) {
    items[i]['position'] = i;
  }


  //jQuery('.nav-regions-block-model.active').removeClass( "active" );
  resetClasses();


  var getParametr = findGetParameter('reg');

  if (getParametr == null) {
    getParametr = "default";
  }

  setSelected();
  setHeaderTitle();

  var catClass = getActiveDataAttr();
  var catItems;
  findItemByClass();

  jQuery(".input-search").on('change keydown paste input', function(e) {
    var code = e.keyCode || e.which;
    if(!e.altKey && !e.shiftKey && !e.ctrlKey && code != 9 ) {
        var subst = $(this).val();
        var allItems = jQuery('.dilersp-region');
//        console.log(allItems);
        for (var i = 0; i < allItems.length; i++) {
            var children = allItems[i]['children'][0].innerText;
            var tempText = children.toLowerCase();
            if (tempText.indexOf(subst.toLowerCase()) !== -1) {
                jQuery(".dilersp-region." + allItems[i].classList[1]).show();
                jQuery(".dilersp-region." + allItems[i].classList[1]).children().show();
                jQuery(".dilersp-region." + allItems[i].classList[1]).children('textarea.hide-regions-taxonomy').hide();
            } else {
                var children = allItems[i]['children'][1].innerText;
                var tempText = children.toLowerCase();
                if (tempText.indexOf(subst.toLowerCase()) !== -1) {
                    jQuery(".dilersp-region." + allItems[i].classList[1]).show();
                    jQuery(".dilersp-region." + allItems[i].classList[1]).children().show();
                    jQuery(".dilersp-region." + allItems[i].classList[1]).children('textarea.hide-regions-taxonomy').hide();
                } else {
                    jQuery(".dilersp-region." + allItems[i].classList[1]).hide();
                }
            }
        }
        if (subst === '') {
            jQuery(".dilersp-region").show();
        }
    }
  });

  function setSelected() {
    jQuery('.id-' + getParametr).addClass("current");
  }

  function clearTextField() {
    jQuery(".input-search").val("");
  }

  function resetClasses() {
    jQuery('.nav-regions-block-model.active').removeClass("active");
    jQuery('.mobile-item.current').removeClass("current");
  }

  function setHeaderTitle(text) {
    if (!text) {
        text = jQuery('.nav-regions-block-model.active').text();
        data_cs = jQuery('.nav-regions-block-model.active').data('cs');
    }

    //big screen
    if(data_cs != '') {
        jQuery('.dilersp-region-header .' + data_cs).html(text);
    }

  }

  function getActiveDataAttr() {
    return getActiveMobileAttr();
  }

  function getActiveMobileAttr() {
    return jQuery('.mobile-item.current').attr('data-cs');
  }

  function findGetParameter(parameterName) {
    var result = null,
      tmp = [];
    location.search
      .substr(1)
      .split("&")
      .forEach(function(item) {
        tmp = item.split("=");
        if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
      });
    return result;
  }


  function findItemByClass() {
    if (catClass !== "") {
      catItems = jQuery(".dilersp-region-row." + catClass);
      filterAll();
    } else {
      catItems = items;
    }
  }

  function filterAll() {
    for (var i = 0; i < items.length; i++) {

      var appear = false;

      for (var j = 0; j < catItems.length; j++) {
        if (items[i]['innerText'] == catItems[j]['innerText']) {
          appear = true;
          break;
        }
      }
      if (appear) {
        jQuery(".dilersp-region-row:eq(" + i + ")").parent().show();
        jQuery(".dilersp-region-row:eq(" + i + ")").show();
      } else {
        jQuery(".dilersp-region-row:eq(" + i + ")").parent().hide();
        jQuery(".dilersp-region-row:eq(" + i + ")").hide();
      }

    }
  }

disablePagForMobile();

function disablePagForMobile()
{
  if ($(window).width() <= '767') {
    jQuery('.pag_small').hide();
      jQuery('.pag_small_bottom').hide();
  }
}

  $(window).resize(function() {
    disablePagForMobile();
  });

});
