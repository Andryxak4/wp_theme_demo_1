<?php
/*
 * Template name: 7 Лет
 */

get_header();
$fields = get_fields();
?>

<!--product section start-->
<section id="years">
    <div class="container">
        <div class="row">


            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                    <li class="active">7 лет</li>
                </ol>

                <h2  class="section-header">
                    7 лет
                </h2>

                <?php
                $args = array(
                    'post_parent' => $post->ID,
                    'post_type' => 'page',
                    'order' => 'ASC',
                    'numberposts' => -1,
                    'post_status' => 'publish'
                );
                $children = get_children($args);
                ?>

                <div class="nav-panel clearfix">
                        <a class="nav-panel-block active" href="<?= get_permalink($post->ID) ?>"><?= $post->post_title ?></a>
                    <?php
                    if ($post && $children):
                        foreach ($children as $child):
                            echo '<a class="nav-panel-block"'
                                .' href="' . get_permalink($child->ID) . '">' . $child->post_title . '</a>';
                            ?>

                        <?php endforeach; ?>
                    <?php endif; ?>

                </div>


            </div>



            <?php if ($fields['blocks_list']): ?>

                <div class="years-main clearfix">

                    <?php foreach ($fields['blocks_list'] as $block): ?>
                        <?php
                        $img_position = (isset($block['img_position']) && !empty($block['img_position']) && $block['img_position'] == 'right') ? 'right' : 'left';
                        ?>


                        <div class="years-main-row <?php
                        if ($img_position == 'right'): echo ' text-left ';
                        endif;
                        ?> clearfix" >
                             <?php if ($img_position == 'left' && $block['image']): ?>
                                <div class="years-main-img col-md-6">
                                    <img src="<?php echo $block['image'] ?>" alt="">
                                </div>
                            <?php endif; ?>

                            <?php if ($block['text_blocks']): ?>

                                <div class="years-main-text col-md-6">
                                    <?php foreach ($block['text_blocks'] as $text): ?>
                                        <?php if ($text['text'] && !empty($text['text'])): ?>   
                                            <div class="years-main-text-row">
                                                <?= $text['text']; ?>
                                            </div>
                                        <?PHP endif; ?>
                                    <?php endforeach; ?>


                                </div>

                            <?php endif; ?>

                            <?php if ($img_position == 'right' && $block['image']): ?>
                                <div class="years-main-img col-md-6">
                                    <img src="<?php echo $block['image'] ?>" alt="">
                                </div>
                            <?php endif; ?>


                        </div>



                        <?php
                    endforeach;
                    ?>
                </div>

            <?php endif; ?>
            <div class="years-main-blog clearfix col-md-12">

                <?php if($fields['hidden_blog']): ?>
                    <h2  class="section-header">
                        блог
                    </h2>
                    <?php

                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                    $args = array(
                        'post_type'         => 'post',
                        'post_status'       => 'publish',
                        'paged'             =>  $paged,
                    );

                    $wp_query = new WP_Query($args);
                    $posts_count = $wp_query->max_num_pages;
                    ?>
                    <div class="years-main-blog-slider col-md-12">
                        <div class="years-main-blog-slider-container clearfix" id="main-blog-slider">
                        <?php while ($wp_query->have_posts()): $wp_query->the_post(); ?>
                            <div class="slide">
                                <div class="slide-img">
                                    <?php
                                    $post_thumbnail = get_the_post_thumbnail_url($wp_query->post->ID);

                                    if (!empty($post_thumbnail)):
                                        ?>
                                        <img src="<?= $post_thumbnail ?>" alt="<?php the_title() ?>">
                                    <?php endif; ?>
                                </div>
                                <div class="slide-header">
                                    <?php the_title(); ?>
                                </div>
                                <div class="slide-text">
                                    <?php the_excerpt(); ?>
                                </div>
                                <div class="slide-info-row clearfix">
                                    <a href="<?php the_permalink() ?>" class="slide-info-row-more">
                                        Читать далее
                                    </a>

                                    <div class="slide-info-row-date">
                                        <?php the_date('d/m/Y') ?>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile; ?>
                        </div>

                        <a href="<?= site_url() ?>/7-let/blog/" class="years-main-blog-slider-more">
                            все новости
                        </a>
                    </div>
                <?php endif; ?>

                <?php if($fields['hidden_video']): ?>
                    <div class="years-main-blog-videos col-md-12" >
                        <h2 class="section-header">
                            видео
                        </h2>
                        <?php $videos = get_fields('31'); ?>
                        <div class="video-blocks col-md-10 col-md-offset-1 clearfix" id="years-main-blog-videos">
                            <?php foreach ($videos['list_of_videos'] as $count => $video): ?>
                            <?php   if($count < 2): ?>
                                        <div class="video-blocks-block">
                                            <iframe width="100%" height="250px" src="https://www.youtube.com/embed/<?= $video['video'] ?>" frameborder="0" allowfullscreen></iframe>
                                            <div class="video-header">
                                                <?= $video['title'] ?>
                                            </div>
                                            <div class="video-info">
                                                <?= $video['description'] ?>
                                            </div>
                                        </div>
                            <?php   endif; ?>
                            <?php endforeach; ?>
                        </div>

                        <div class="video-blocks-more">
                            <a href="<?= site_url() ?>/7-let/video/" class="video-blocks-more-btn">смотреть все</a>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<!--product section end-->


<?php get_footer() ?>