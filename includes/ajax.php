<?php

add_action('wp_ajax_get_latlng', 'get_latlng_func');
add_action('wp_ajax_nopriv_get_latlng', 'get_latlng_func');

function get_latlng_func() {

    if (!isset($_POST['address']))
        die();

    $address = trim($_POST['address']);
    $opt_key = 'latlng_' . strtolower(md5($_POST['address']));
    $latlng = get_option($opt_key, false);

    if (!$latlng) {
        $res = json_decode(
                file_get_contents(
                        "http://maps.google.com/maps/api/geocode/json?address=" . urlencode($address) . "&sensor=false"
                )
        );
        if ($res->status == 'OK') {
            $latlng = json_encode($res->results[0]->geometry->location);
            update_option($opt_key, $latlng);
        }
    }

    echo $latlng;

    die();
}

add_action('wp_ajax_faq_search', 'faq_search');
add_action('wp_ajax_nopriv_faq_search', 'faq_search');

function faq_search() {
    $search_results = array();
    if (isset($_POST['term']) && !empty($_POST['term'])):


        $args = array(
            'post_type' => 'faq',
            'posts_per_page' => '-1',
            's' => $_POST['term']
        );
        $wp_query = new WP_Query($args);

        if ($wp_query->have_posts()):
            $m = 0;
            while ($wp_query->have_posts()): $wp_query->the_post();
                $title = str_ireplace(trim($_POST['term']), '<span class="found">' . $_POST['term'] . '</span>', $wp_query->post->post_title);
                $content = str_ireplace(trim($_POST['term']), '<span class="found">' . $_POST['term'] . '</span>', $wp_query->post->post_content);
                $search_results[] = array(
                    'title' => $title,
                    'content' => $content
                );


            endwhile;
        endif;

    endif;
    echo json_encode($search_results);

    die();
}

add_action('wp_ajax_cart_ajax_remove_product', 'lappi_cart_ajax_remove_product');
add_action('wp_ajax_nopriv_cart_ajax_remove_product', 'lappi_cart_ajax_remove_product');
function lappi_cart_ajax_remove_product() {
    if (!empty($_POST['product_id'])):
        $product_id = sanitize_text_field( $_POST['product_id'] );
        $cart_item_key = false;
        foreach (WC()->cart->get_cart() as $key => $cart_item):
            if ($product_id == $cart_item['product_id']):
                $cart_item_key = $key;
                break;
            endif;
        endforeach;

        if ( $cart_item_key && ($cart_item = WC()->cart->get_cart_item($cart_item_key)) ) {
            WC()->cart->remove_cart_item( $cart_item_key );
            $product = wc_get_product( $cart_item['product_id'] );
            $item_removed_title = apply_filters( 'woocommerce_cart_item_removed_title', $product
                    ? sprintf( _x( '&ldquo;%s&rdquo;', 'Item name in quotes', 'woocommerce' ), $product->get_name() )
                    : __( 'Item', 'woocommerce' ), $cart_item );
            // Don't show undo link if removed item is out of stock.
            if ( $product->is_in_stock() && $product->has_enough_stock( $cart_item['quantity'] ) ) {
                $removed_notice  = sprintf( __( '%s removed.', 'woocommerce' ), $item_removed_title );
                $removed_notice .= ' <a href="' . esc_url( WC()->cart->get_undo_url( $cart_item_key ) ) . '" class="restore-item">' . __( 'Undo?', 'woocommerce' ) . '</a>';
            } else {
                $removed_notice = sprintf( __( '%s removed.', 'woocommerce' ), $item_removed_title );
            }
            wc_add_notice( $removed_notice );
            
            exit(json_encode(array(
                'success' => true,
            )));
        }
    endif;
    exit(json_encode(array(
        'success' => false,
        'errors' => 'Empty product ID'
    )));
}

add_action('wp_ajax_cart_ajax_change_quantity', 'lappi_cart_ajax_remove_product');
add_action('wp_ajax_nopriv_cart_ajax_change_quantity', 'lappi_cart_ajax_remove_product');
function lappi_cart_ajax_change_quantity() {
    if ((!empty($_POST['product_id'])) && (!empty($_POST['quantity']))):
        $product_id = sanitize_text_field( $_POST['product_id'] );
        $cart_item_key = false;
        foreach (WC()->cart->get_cart() as $key => $cart_item):
            if ($product_id == $cart_item['product_id']):
                $cart_item_key = $key;
                break;
            endif;
        endforeach;
        $quantity = $_POST['quantity'];
        if ($cart_item_key):
            WC()->cart->set_quantity( $cart_item_key, $quantity, false );
            WC()->cart->calculate_totals();
        endif;
        
        exit(json_encode(array(
            'success' => true,
        )));
    endif;
    exit(json_encode(array(
        'success' => false,
        'errors' => 'Empty product ID or quantity'
    )));
}

add_action('wp_ajax_one_click_order', 'lappi_one_click_order');
add_action('wp_ajax_nopriv_one_click_order', 'lappi_one_click_order');
function lappi_one_click_order() {
    
    // TODO: captcha code checking
    
    $form_data = array();
    parse_str($_POST['form_data'], $form_data);
    $product_id = arrayItem($form_data, 'product_id');
    $name = arrayItem($form_data, 'name');
    $phone = arrayItem($form_data, 'phone');
    $comment = arrayItem($form_data, 'comment');
    
    try {
    
        // save current cart state & empty cart
        $cart = wc()->cart;
        wc()->cart = new WC_Cart;

        $_REQUEST['add-to-cart'] = $product_id;
        WC_Form_Handler::add_to_cart_action();

        $checkout = new WC_Checkout();
        $order_id = $checkout->create_order(array(
            'payment_method' => '',
            'order_comments' => $comment,
            'billing_phone' => $phone,
            'billing_first_name' => 'Заказ в 1 клик - ',
            'billing_last_name' => $name,
        ));
        
        $order = new WC_Order($order_id);
        add_filter('woocommerce_payment_complete_order_status', function($status) {
            return 'processing';
        });
        $order->payment_complete();

        // restore saved cart state
        wc()->cart = $cart;
        wc()->cart->set_session();

        exit(json_encode(array(
            'success' => true,
            'order_id' => $order_id,
        )));
    } catch (Exception $e) {
        add_log('one_click_order', get_class($e)." with code ".$e->getCode()
            . "\nin file [" . $e->getFile()."] on line " . $e->getLine()
            . "\nMessage: [" . $e->getMessage() . "]"
            . "\nTrace:"
            . "\n" . $e->getTraceAsString()
            . "\n   POST:"
            . "\n".var_export($_POST, true));
        exit(json_encode(array(
            'success' => false
        )));
    }
}

add_action('wp_ajax_process_checkout', 'lappi_process_checkout');
add_action('wp_ajax_nopriv_process_checkout', 'lappi_process_checkout');
function lappi_process_checkout() {
    $form_data = array();
    parse_str($_POST['form_data'], $form_data);
    
    $name = arrayItem($form_data, 'name');
    $phone = arrayItem($form_data, 'phone');
    $email = arrayItem($form_data, 'email');
    $city = arrayItem($form_data, 'city');
    $comment = arrayItem($form_data, 'comment');
    $payment_method = arrayItem($form_data, 'payment_method');
    $shipping_method = arrayItem($form_data, 'shipping_method');
    
    try {

        $checkout = new WC_Checkout();
        $order_id = $checkout->create_order(array(
            'payment_method' => $payment_method,
            'order_comments' => $comment,
            'billing_phone' => $phone,
            'billing_first_name' => $name,
            'billing_city' => $city,
            'billing_email' => $email,
            'billing_address_1' => $shipping_method,
        ));

        wc_empty_cart();
        
        $order = new WC_Order($order_id);
        add_filter('woocommerce_payment_complete_order_status', function($status) {
            return 'processing';
        });
        $order->payment_complete();
        $thank_you_page = $order->get_checkout_order_received_url();

        exit(json_encode(array(
            'success' => true,
            'thank_you_page' => $thank_you_page,
        )));

    } catch (Exception $e) {
        add_log('process_checkout', get_class($e)." with code ".$e->getCode()
            . "\nin file [" . $e->getFile()."] on line " . $e->getLine()
            . "\nMessage: [" . $e->getMessage() . "]"
            . "\nTrace:"
            . "\n" . $e->getTraceAsString()
            . "\n   POST:"
            . "\n".var_export($_POST, true));
        exit(json_encode(array(
            'success' => false
        )));
    }
}

add_action('wp_ajax_set_shipping_method', 'lappi_set_shipping_method');
add_action('wp_ajax_nopriv_set_shipping_method', 'lappi_set_shipping_method');
function lappi_set_shipping_method() {
    WC()->session->set('chosen_shipping_methods', array(
        0 => arrayItem($_POST, 'shipping_method')
    ));
    wc_maybe_define_constant( 'WOOCOMMERCE_CART', true );
    WC()->cart->calculate_totals();
    exit();
}