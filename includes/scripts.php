<?php

function theme_enqueue_scripts() {
    if (!is_admin()) {
        wp_deregister_script('jquery');
    }
    $wp_version = 120;

    wp_enqueue_script('jquery', get_stylesheet_directory_uri() . '/js/jquery-3.2.1.min.js', array(), $wp_version, true);
    wp_enqueue_script('magnific-popup', get_stylesheet_directory_uri() . '/js/jquery.magnific-popup.min.js', array(), $wp_version, true);
    wp_enqueue_script('slick', get_stylesheet_directory_uri() . '/js/slick.min.js', array(), $wp_version, true);
    wp_enqueue_script('bootstrap', get_stylesheet_directory_uri() . '/js/bootstrap.min.js', array(), $wp_version, true);
    wp_enqueue_script('theme-script', get_stylesheet_directory_uri() . '/js/script.js', array('jquery'), $wp_version, true);
    wp_enqueue_script('compare-script', get_stylesheet_directory_uri() . '/js/compare.js', array('jquery'), $wp_version, true);
    wp_enqueue_script('zoom-script', get_stylesheet_directory_uri() . '/js/jquery.zoom.min.js', array('jquery'), $wp_version, true);
}

add_action('wp_enqueue_scripts', 'theme_enqueue_scripts');

function theme_add_styles() {
    wp_enqueue_style('slick', get_stylesheet_directory_uri() . '/css/slick.css');
    wp_enqueue_style('bootstrap', get_stylesheet_directory_uri() . '/css/bootstrap.css');
    wp_enqueue_style('magnific-popup', get_stylesheet_directory_uri() . '/css/magnific-popup.css');
    wp_enqueue_style('theme-style', get_stylesheet_directory_uri() . '/css/style.css');
    wp_enqueue_style('compare-style', get_stylesheet_directory_uri() . '/css/compare.css');
}

add_action('wp_print_styles', 'theme_add_styles');


//remove_action('wp_head', 'print_emoji_detection_script', 7);
//remove_action('wp_print_styles', 'print_emoji_styles');
