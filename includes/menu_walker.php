<?php

class LappigrillMenuWalker extends Walker_Nav_Menu {

    private $structure = array();
    private $active_item_class = '';
    private $submenu_closed = false;

    public function __construct($structure = array('a' => array()), $active_item_class = '') {
        $this->structure = $structure;
        $this->active_item_class = $active_item_class;
    }

    public function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
        foreach ($this->structure as $tag => $opts):
            if ($tag == 'a'):
                $output .= $this->renderLink($item, $depth, $args, $id);
            else:
                $classes = isset($opts['class']) ? array($opts['class']) : array();
                if ($this->has_children):
                    $classes[] = 'dropdown';
                endif;
                if ($item->current && $this->active_item_class):
                    $classes[] = $this->active_item_class;
                endif;
                $class = $classes ? " class='" . implode(' ', $classes) . "'" : "";
                $output .= "\n<" . $tag . $class . ">\n";
            endif;
        endforeach;
    }

    public function end_el(&$output, $item, $depth = 0, $args = array()) {
        $tags = array_keys($this->structure);
        for ($i = count($tags) - 1; $i >= 0; $i--):
            if ($tags[$i]=='a' && $this->submenu_closed):
                $tags[$i] = 'span';
                $this->submenu_closed = false;
            endif;
            $output .= "</" . $tags[$i] . ">\n";
        endfor;
    }

    private function renderLink($item, $depth = 0, $args = array(), $id = 0) {
        $opts = $this->structure['a'];
        $classes = isset($opts['class']) ? array($opts['class']) : array();
        if ($item->classes[0] != "menu-item"):
            $classes[] = $item->classes[0];
        endif;
        if ($this->has_children):
            $classes[] = "dropdown-toggle";
        endif;
        
        $class = $classes ? " class='" . implode(' ', $classes) . "'" : "";
        $target = $item->target ? " target='{$item->target}'" : "";

        if (isset($opts['template'])):
            $title = str_replace(array('%title%', '%desc%'), array($item->title, $item->description), $opts['template']);
        else:
            $title = $item->title;
        endif;
        
        if ($this->has_children):
            return "<a href='{$item->url}'$class data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'$target>$title 
<span class='caret'></span>
<div class='mobile-caret'>
    <div class='mobile-caret-line horizontal'></div>
    <div class='mobile-caret-line vertical'></div>
</div>";
        else:
            return "<a$class href='" . $item->url . "'$target>$title";
        endif;
    }
    
    public function start_lvl( &$output, $depth = 0, $args = array() ) {
        $output .= "\n<ul class='dropdown-menu'>\n";
    }
    
    public function end_lvl( &$output, $depth = 0, $args = array() ) {
        $output .= "\n</ul>\n";
        $this->submenu_closed = true;
    }

}
