<?php

add_action('after_setup_theme', 'wpp_theme_setup');

function wpp_theme_setup() {
    add_theme_support('post-thumbnails');
}

add_action('after_setup_theme', 'add_settings_pages');

function add_settings_pages() {

    if (function_exists('acf_add_options_sub_page')) {
        acf_set_options_page_title('Настройки Темы');
        acf_set_options_page_menu('Настройки Темы');

        acf_add_options_sub_page(array(
            'title' => 'Главные',
            'menu' => 'Главные Настройки',
            'slug' => 'general-settings',
            'capability' => 'manage_options',
            'parent_slug' => 'acf-options',
        ));


        acf_add_options_sub_page(array(
            'title' => 'Страница 404',
            'menu' => 'Настройки Страницы 404',
            'slug' => '404-page-settings',
            'capability' => 'manage_options',
            'parent_slug' => 'acf-options',
        ));
    }
}

add_action('init', function() {

    add_filter('manage_product_posts_columns', function ($existing_columns) {
        if ( empty( $existing_columns ) && ! is_array( $existing_columns ) ) {
            $existing_columns = array();
        }
        unset($existing_columns['product_cat']);
        $last_2_cols = array_splice($existing_columns, -2, 2);
        $existing_columns['product_categories'] = 'Категории';
        

        return array_merge($existing_columns, $last_2_cols);
    });
    
    add_action('manage_product_posts_custom_column', function($column) {
        global $post, $the_product;

        if ( empty( $the_product ) || $the_product->get_id() != $post->ID ) {
             $the_product = wc_get_product( $post );
        }

        // Only continue if we have a product.
        if ( empty( $the_product ) ) {
             return;
        }

        switch ( $column ):
            case 'product_categories' :
                if ( ! $terms = get_the_terms( $post->ID, $column ) ):
                     echo '<span class="na">&ndash;</span>';
                else:
                    $termlist = array();
                    foreach ( $terms as $term ):
                        $termlist[] = '<a href="' . admin_url( 'edit.php?' . $column . '=' . $term->slug . '&post_type=product' ) . ' ">' . $term->name . '</a>';
                    endforeach;

                    echo implode( ', ', $termlist );
                endif;
                break;
        endswitch;
    }, 2);
}, 99);