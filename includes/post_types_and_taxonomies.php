<?php

function create_post_types() {
    $multiArgs = array(
        'product' => array(
            'single' => 'Продукт',
            'plural' => 'Продукты',
            'public' => true,
            'hierarchical' => false,
            'has_archive' => true,
            'capability_type' => 'post',
            'taxonomies' => array(),
            'supports' => array('title', 'author', 'thumbnail', 'editor', 'excerpt'),
        ),
        'dealers' => array(
            'single' => 'Дилер',
            'plural' => 'Дилеры',
            'public' => true,
            'hierarchical' => false,
            'has_archive' => true,
            'capability_type' => 'post',
            'taxonomies' => array(),
            'supports' => array('title', 'author', 'thumbnail'),
        ),
    );
    $i = 4;
    foreach ($multiArgs as $key => $value) {
        $labels = array(
            'name' => $value['plural'],
            'singular_name' => $value['single'],
            'add_new' => 'Добавить ' . $value['single'],
            'add_new_item' => 'Добавить Новую ' . $value['single'],
            'edit_item' => 'Редактировать ' . $value['single'],
            'new_item' => 'Новая ' . $value['single'],
            'all_items' => 'Все ' . $value['plural'],
            'view_item' => 'Перейти ' . $value['single'],
            'search_items' => 'Искать ' . $value['plural'],
            'not_found' => $value['plural'] . ' Не Найдено',
            'not_found_in_trash' => $value['plural'] . ' Не Найдено В Корзине',
            'menu_name' => $value['plural']
        );

        if ($key == 'countries'):
            $rewrite = array('slug' => 'strany', 'with_front' => true);
        else:
            $rewrite = true;
        endif;

        $show_in_menu = true;
        $args = array(
            'labels' => $labels,
            'public' => $value['public'],
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'query_var' => true,
            'rewrite' => $rewrite,
            'capability_type' => $value['capability_type'],
            'has_archive' => $value['has_archive'],
            'hierarchical' => $value['hierarchical'],
            'menu_position' => $i,
            //    'taxonomies' => $value['taxonomies'],
            'supports' => $value['supports']
        );
        register_post_type($key, $args);
        $i++;
    }
}
add_action('init', 'create_post_types');

function custom_taxonomies() {
    $labels = array(
        'name' => 'Категории Товаров',
        'singular_name' => 'Категория',
        'search_items' => 'Поиск Категорий',
        'all_items' => 'Все Категории',
        'parent_item' => 'Родительская Категория',
        'parent_item_colon' => 'Родительская Категория:',
        'edit_item' => 'Редактировать Категорию',
        'update_item' => 'Обновить Категорию',
        'add_new_item' => 'Добавит Новую Категорию',
        'new_item_name' => 'Новая Категория',
        'menu_name' => 'Категории',
    );
    $args = array(
        'label' => '',
        'labels' => $labels,
        'show_in_nav_menus' => true,
        'hierarchical' => true,
        'update_count_callback' => '',
        'rewrite' => array('slug' => 'product-categories'),
        'query_var' => true
    );
    register_taxonomy('product_categories', array('product'), $args);

    $labels = array(
        'name' => 'Регионы',
        'singular_name' => 'Регион',
        'search_items' => 'Поиск Категорий',
        'menu_name' => 'Регионы'
    );
    $args = array(
        'label' => '',
        'labels' => $labels,
        'show_in_nav_menus' => true,
        'hierarchical' => true,
        'update_count_callback' => '',
        'rewrite' => array('slug' => 'dealers-region'),
        'query_var' => true
    );
    register_taxonomy('dealers_region', array('dealers'), $args);
}
add_action('init', 'custom_taxonomies');
