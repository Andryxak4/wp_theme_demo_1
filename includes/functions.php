<?php

function dimox_breadcrumbs() {

    /* === ОПЦИИ === */
    $text['home'] = 'Главная'; // текст ссылки "Главная"
    $text['category'] = '%s'; // текст для страницы рубрики
    $text['search'] = 'Результаты поиска по запросу "%s"'; // текст для страницы с результатами поиска
    $text['tag'] = 'Записи с тегом "%s"'; // текст для страницы тега
    $text['author'] = 'Статьи автора %s'; // текст для страницы автора
    $text['404'] = 'Ошибка 404'; // текст для страницы 404
    $text['page'] = 'Страница %s'; // текст 'Страница N'
    $text['cpage'] = 'Страница комментариев %s'; // текст 'Страница комментариев N'

    $wrap_before = '<ol class="breadcrumb">'; // открывающий тег обертки
    $wrap_after = '</ol><!-- .breadcrumb -->'; // закрывающий тег обертки
    $sep = ''; // разделитель между "крошками"
    $sep_before = ''; // тег перед разделителем
    $sep_after = ''; // тег после разделителя
    $show_home_link = 1; // 1 - показывать ссылку "Главная", 0 - не показывать
    $show_on_home = 0; // 1 - показывать "хлебные крошки" на главной странице, 0 - не показывать
    $show_current = 1; // 1 - показывать название текущей страницы, 0 - не показывать
    $before = '<li class="active">'; // тег перед текущей "крошкой"
    $after = '</li>'; // тег после текущей "крошки"
    /* === КОНЕЦ ОПЦИЙ === */

    global $post;
    $home_link = home_url('/');
    $link_before = '<li>';
    $link_after = '</li>';
    $link_attr = '';
    $link_in_before = '';
    $link_in_after = '';
    $link = $link_before . '<a href="%1$s"' . $link_attr . '>' . $link_in_before . '%2$s' . $link_in_after . '</a>' . $link_after;
    $frontpage_id = get_option('page_on_front');
    $parent_id = $post->post_parent;
    $sep = ' ' . $sep_before . $sep . $sep_after . ' ';

    if (is_home() || is_front_page()) {

        if ($show_on_home)
            echo $wrap_before . '<a href="' . $home_link . '">' . $text['home'] . '</a>' . $wrap_after;
    } else {

        echo $wrap_before;
        if ($show_home_link)
            echo sprintf($link, $home_link, $text['home']);

        if (is_category()) {
            $cat = get_category(get_query_var('cat'), false);
            if ($cat->parent != 0) {
                $cats = get_category_parents($cat->parent, TRUE, $sep);
                $cats = preg_replace("#^(.+)$sep$#", "$1", $cats);
                $cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr . '>' . $link_in_before . '$2' . $link_in_after . '</a>' . $link_after, $cats);
                if ($show_home_link)
                    echo $sep;
                echo $cats;
            }

            if (get_query_var('paged')) {
                $cat = $cat->cat_ID;
                echo $sep . sprintf($link, get_category_link($cat), get_cat_name($cat)) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
            } else {
                if ($show_current)
                    echo $sep . $before . sprintf($text['category'], single_cat_title('', false)) . $after;
            }


        } elseif (is_search()) {
            if (have_posts()) {
                if ($show_home_link && $show_current)
                    echo $sep;
                if ($show_current)
                    echo $before . sprintf($text['search'], get_search_query()) . $after;
            } else {
                if ($show_home_link)
                    echo $sep;
                echo $before . sprintf($text['search'], get_search_query()) . $after;
            }
        } elseif (is_day()) {
            if ($show_home_link)
                echo $sep;
            echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $sep;
            echo sprintf($link, get_month_link(get_the_time('Y'), get_the_time('m')), get_the_time('F'));
            if ($show_current)
                echo $sep . $before . get_the_time('d') . $after;
        } elseif (is_month()) {
            if ($show_home_link)
                echo $sep;
            echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y'));
            if ($show_current)
                echo $sep . $before . get_the_time('F') . $after;
        } elseif (is_year()) {
            if ($show_home_link && $show_current)
                echo $sep;
            if ($show_current)
                echo $before . get_the_time('Y') . $after;
        } elseif (is_single() && !is_attachment()) {

            $pt = get_post_type();
            if ($show_home_link) {
                echo $sep;
            }
            if (get_post_type() != 'post') {
                $post_type = get_post_type_object(get_post_type());
                $label = apply_filters('dimox_breadcrumbs_custom_post_type_name', $post_type->labels->singular_name);
                $slug = apply_filters('dimox_breadcrumbs_custom_post_type_link', $post_type->rewrite);
                if ($post_type->name != 'product') {
                    printf($link, $home_link . '' . $slug['slug'] . '/', $label);
                } else {
                    $product_categories = wp_get_object_terms(get_the_ID(), 'product_categories');
                    $product_category = arrayItem($product_categories, 0);
                    if ($product_category) {
                        printf($link, $home_link . 'product-categories/' . $product_category->slug . '/', $product_category->name);
                    } else {
                        printf($link, $home_link . '' . $slug['slug'] . '/', $label);
                    }
                }
                if ($show_current)
                    echo $sep . $before . get_the_title() . $after;
            } else {
                $cat = get_the_category();
                $cat = $cat[0];
                $cats = get_category_parents($cat, TRUE, $sep);
                if (!$show_current || get_query_var('cpage'))
                    $cats = preg_replace("#^(.+)$sep$#", "$1", $cats);
                $cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr . '>' . $link_in_before . '$2' . $link_in_after . '</a>' . $link_after, $cats);
                echo $cats;
                if (get_query_var('cpage')) {
                    echo $sep . sprintf($link, get_permalink(), get_the_title()) . $sep . $before . sprintf($text['cpage'], get_query_var('cpage')) . $after;
                } else {
                    if ($show_current)
                        echo $before . get_the_title() . $after;
                }
            }


            // custom post type
        } elseif (!is_single() && !is_page() && get_post_type() != 'post' && !is_404()) {
            $post_type = get_post_type_object(get_post_type());
            if (get_query_var('paged')) {
                echo $sep . sprintf($link, get_post_type_archive_link($post_type->name), $post_type->label) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
            } else {
                if ($show_current)
                    echo $sep . $before . $post_type->label . $after;
            }
        } elseif (is_attachment()) {
            if ($show_home_link)
                echo $sep;
            $parent = get_post($parent_id);
            $cat = get_the_category($parent->ID);
            $cat = $cat[0];
            if ($cat) {
                $cats = get_category_parents($cat, TRUE, $sep);
                $cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr . '>' . $link_in_before . '$2' . $link_in_after . '</a>' . $link_after, $cats);
                echo $cats;
            }
            printf($link, get_permalink($parent), $parent->post_title);
            if ($show_current)
                echo $sep . $before . get_the_title() . $after;
        } elseif (is_page() && !$parent_id) {
            if ($show_current)
                echo $sep . $before . get_the_title() . $after;
        } elseif (is_page() && $parent_id) {
            if ($show_home_link)
                echo $sep;
            if ($parent_id != $frontpage_id) {
                $breadcrumbs = array();
                while ($parent_id) {
                    $page = get_page($parent_id);
                    if ($parent_id != $frontpage_id) {
                        $breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
                    }
                    $parent_id = $page->post_parent;
                }
                $breadcrumbs = array_reverse($breadcrumbs);
                for ($i = 0; $i < count($breadcrumbs); $i++) {
                    echo $breadcrumbs[$i];
                    if ($i != count($breadcrumbs) - 1)
                        echo $sep;
                }
            }
            if ($show_current)
                echo $sep . $before . get_the_title() . $after;
        } elseif (is_tag()) {
            if (get_query_var('paged')) {
                $tag_id = get_queried_object_id();
                $tag = get_tag($tag_id);
                echo $sep . sprintf($link, get_tag_link($tag_id), $tag->name) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
            } else {
                if ($show_current)
                    echo $sep . $before . sprintf($text['tag'], single_tag_title('', false)) . $after;
            }
        } elseif (is_author()) {
            global $author;
            $author = get_userdata($author);
            if (get_query_var('paged')) {
                if ($show_home_link)
                    echo $sep;
                echo sprintf($link, get_author_posts_url($author->ID), $author->display_name) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
            } else {
                if ($show_home_link && $show_current)
                    echo $sep;
                if ($show_current)
                    echo $before . sprintf($text['author'], $author->display_name) . $after;
            }
        } elseif (is_404()) {
            if ($show_home_link && $show_current)
                echo $sep;
            if ($show_current)
                echo $before . $text['404'] . $after;
        } elseif (has_post_format() && !is_singular()) {
            if ($show_home_link)
                echo $sep;
            echo get_post_format_string(get_post_format());
        }

        echo $wrap_after;
    }
}

/**
 * Table-view var_dump's version
 * @param mixed $var variable to dump
 * @param array $args <i>[optional]</i> options array
 */
function pretty_var_dump($var, $args = array()) {
    $defautls = array(
        'before_table_text' => '',
        'min_width' => 250,
        'skip_int_indices' => false,
        'dump_values' => true,
        'depth' => 0,
        'max-depth' => 10,
        'echo' => true,
        'show_classes_names' => true,
        'cut_keys' => false,
    );
    if (!is_array($args)) {
        $args = array();
    }
    $args = array_merge($defautls, $args);
    $args['depth'] = !empty($args['depth']) ? $args['depth'] + 1 : 1;
    $echo = $args['echo'];
    $args['echo'] = true;
    $classname_str = "";
    if (is_object($var)):
        if ($args['show_classes_names']):
            $classname_str = "<u><i>object</i>(<b>" . get_class($var) . "</b>)</u>\n";
        endif;
        $var = (array) $var;
        $color = '#00CC00';
    else: $color = '#333';
    endif;
    if ($echo === false) {
        ob_start();
    }
    if (!defined("PRETTY_VAR_DUMP_STYLES")) {
        define("PRETTY_VAR_DUMP_STYLES", 1);
        echo 
"<script>
function rightCellToggleCollapsed(){
    el = event.path[1];
    var next_td = el.nextElementSibling;
    el.classList.toggle('collapsed_parent');
    next_td.classList.toggle('collapsed');
}
</script>
<style>
    .pretty_var_dump__table { min-width:{$args['min_width']}px; width: auto; padding: 0; margin: 0; }
    .pretty_var_dump__table td { padding:1px; line-height: 1; }
    .pretty_var_dump__table td:first-child:hover { background-color: #DDD; }
    .pretty_var_dump__table pre { white-space: pre-line; padding:0; margin: 0; border: 0; line-height: 1; overflow: initial;}
    .pretty_var_dump__table .collapsed { display: none; }
    .pretty_var_dump__table .collapsed_parent { background-color: #CCC; }
</style>\n";
    }
    if ($args['depth']==1) { echo $args['before_table_text'];}
    $spaces = 6 * ($args['depth'] - 1);
    if (is_array($var)):
        echo str_pad("", $spaces, " ") . "$classname_str<table class=\"pretty_var_dump__table\" style='border:2px solid $color; min-width:{$args['min_width']}px;'>\n" .
        str_pad("", $spaces + 2, " ") . "<tbody>\n";
        foreach ($var as $k => $v):
            if (!is_int($k) || !$args['skip_int_indices']):
                if ($args['cut_keys'])
                    $k = (strlen($k) > $args['cut_keys'] + 2) ? "<span title='$k'>" . substr($k, 0, $args['cut_keys']) . "..</span>" : $k;
                echo str_pad("", $spaces + 2, " ") . "<tr class='depth-{$args['depth']}'>\n" .
                str_pad("", $spaces + 4, " ") . "<td style='border-right: 2px #4444FF dotted; text-align: right'>\n" .
                str_pad("", $spaces + 6, " ") . "[" . (is_int($k) ? "(int)" : "") . $k . "]"
                        . "<span onclick='rightCellToggleCollapsed()'>=&gt;</span>\n" .
                str_pad("", $spaces + 4, " ") . "</td>\n" .
                str_pad("", $spaces + 4, " ") . "<td>\n";
                if ($args['depth'] < $args['max-depth']):
                    pretty_var_dump($v, $args);
                else: echo "<pre><b>Max depth reached!</b></pre>";
                endif;
                echo str_pad("", $spaces + 4, " ") . "</td>\n" .
                str_pad("", $spaces + 2, " ") . "</tr><!--depth-{$args['depth']}-->\n";
            endif;
        endforeach;
        echo str_pad("", $spaces + 2, " ") . "</tbody>\n" .
        str_pad("", $spaces, " ") . "</table>\n";
    else:
        if ($args['dump_values']):
            echo str_pad("", $spaces, " ") . "<pre>";
            var_dump(htmlentities($var));
            echo str_pad("", $spaces, " ") . "</pre>\n";
        else:
            echo str_pad("", $spaces, " ") . htmlentities($var) . "\n";
        endif;
    endif;
    if ($echo === false) {
        $result = ob_get_contents();
        ob_end_clean();
        return $result;
    }
}

function arrayItem($array, $item, $default = null) {
    if (is_array($item)) {
        $element = $array;
        foreach ($item as $key) {
            if (is_object($element) && isset($element->$key)) {
                $element = $element->$key;
            } elseif (isset($element[$key])) {
                $element = $element[$key];
            } else {
                return $default;
            }
        }
        return $element;
    }
    if (is_object($array))
        return isset($array->$item) ? $array->$item : $default;
    return isset($array[$item]) ? $array[$item] : $default;
}

function checkHTTPBaseAuth($login, $password) {
    if (!isset($_SERVER['PHP_AUTH_USER'])) {
        header('WWW-Authenticate: Basic realm="My Realm"');
        header('HTTP/1.0 401 Unauthorized');
        exit(json_encode(array('error' => 'Auth cancelled')));
    } else {
        if (($_SERVER['PHP_AUTH_USER'] !== $login) || ($_SERVER['PHP_AUTH_PW'] !== $password)) {
            exit(json_encode(array('error' => 'Wrong login or password')));
        }
    }
}

function validateRequired($array, $keys) {
    if (!is_array($array) && is_array($keys)):
        return false;
    endif;
    foreach ($keys as $key):
        if (!isset($array[$key]) || (empty($array[$key]) && !(is_numeric($array[$key]) && $array[$key] == 0) )):
            return false;
        endif;
    endforeach;
    return true;
}

function counting_morph_ending($count, $gender = 'f', $ending1="", $ending2="", $ending5="") {
    $last_digit = $count % 10;
    $rest_digits = ($count - $last_digit) / 10;
    if ($rest_digits != 1 && $last_digit == 1):
        if ($gender == 'f'):
            return $ending1 ?: 'а';
        else:
            return $ending1 ?: '';
        endif;
    elseif ($rest_digits != 1 && $last_digit > 1 && $last_digit < 5):
        if ($gender == 'f'):
            return $ending2 ?: 'ы';
        else:
            return $ending2 ?: 'а';
        endif;
    else:
        if ($gender == 'f'):
            return $ending5 ?: '';
        else:
            return $ending5 ?: 'ов';
        endif;
    endif;
}

function get_month_rus($month) {
    $months = array(
        '',
        'января',
        'февраля',
        'марта',
        'апреля',
        'мая',
        'июня',
        'июля',
        'августа',
        'сентября',
        'октября',
        'ноября',
        'декабря'
    );
    return $months[$month];
}

function get_request_scheme(){
    return (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != "off") ? "https" : "http";
}

function add_log($log_name, $str) {
    $logs_dir = $_SERVER['DOCUMENT_ROOT'].'/logs';
    if (!is_dir($logs_dir)):
        mkdir($logs_dir, 0777);
    endif;
    
    file_put_contents($logs_dir . "/{$log_name}_" . date('Y-m-d'), date("  == Y-m-d H:i:s P T ==\n").$str."\n\n");
}

function lappi_hearth_size($str) {
    $pict_html = '<img src="' . get_template_directory_uri() . '/img/grill-size.png">';
    $str_with_pict = str_replace('[]', $pict_html, $str);
    return $str_with_pict;
}

function hard_remove_action($hook, $class, $method, $priority=10) {
    global $wp_filter;
    if (!arrayItem($wp_filter, array($hook, 'callbacks', $priority))):
        return;
    endif;
    foreach ($wp_filter[$hook]->callbacks[$priority] as $i => $callback):
        if (is_array($callback['function']) && ($callback['function'][0] instanceof $class)
                && $callback['function'][1] == $method):
            unset($wp_filter[$hook]->callbacks[$priority][$i]);
            break;
        endif;
    endforeach;
}