<?php get_header(); ?>


<!--slider start-->
<section id="about">
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <!--slider start-->
                <div class="slider-inner">
                    <div class="slider-container clearfix" >
                        <div class="slides-block clearfix" id="main-slider">
                            <div class="slide-block">
                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/sl1.jpg" alt="" >
                            </div>

                            <div class="slide-block">
                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/sl1.jpg" alt="" >
                            </div>

                            <div class="slide-block">
                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/gt1.png" alt="" >
                            </div>

                        </div>

                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/sc1.png" alt="" class="img-centered">
                        <div class="sr">
                            <h3 class="sr-header">Lappi Grill</h3>
                            <h4 class="sr-subheader">мы <span class="colored">производим</span> грили-барбекю </h4>
                            <p class="sr-info">
                                Мы производим отечественные угольные и дровяные грили по
                                собственным технологиям, адаптированным под российский рынок
                            </p>

                            <div class="sr-grill-properties">
                                <div class="sr-grill-row clearfix">
                                    <div class="sr-grill-row-img-block">
                                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/si11.png">
                                    </div>

                                    <div class="sr-grill-row-description">
                                        <div class="sr-grill-row-description-header">ВЫСОКОПРОЧНЫЕ МАТЕРИАЛЫ</div>
                                        <p class="sr-grill-row-description-info">Адаптированы под российский климат</p>
                                    </div>
                                </div>

                                <div class="sr-grill-row clearfix">
                                    <div class="sr-grill-row-img-block">
                                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/si12.png">
                                    </div>

                                    <div class="sr-grill-row-description">
                                        <div class="sr-grill-row-description-header">АНТИКОРРОЗИЙНАЯ КРАСКА</div>
                                        <p class="sr-grill-row-description-info">На поверхностях всех материалов каждого типа грилей</p>
                                    </div>
                                </div>

                                <div class="sr-grill-row clearfix">
                                    <div class="sr-grill-row-img-block">
                                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/si13.png">
                                    </div>

                                    <div class="sr-grill-row-description">
                                        <div class="sr-grill-row-description-header">НЕРЖАВЕЮЩАЯ СТАЛЬ</div>
                                        <p class="sr-grill-row-description-info">Применяется в эксклюзивной линейке Inox</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--slider end-->
            </div>

        </div>
    </div>
</section>
<!--slider end-->




<!--grills types start-->
<section id="grill-types">
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <a href="#" class="section-header">Добро пожаловать в мир грилей-барбекю</a>
                <p class="section-info">
                    Относится к числу островных. традиционных моделей финских гирлей-барбекю. Она оптимально сочетает в себе стоимость с хорошим функциональными характеристиками. Она оптимально сочетает в себе стоимость с хорошим функциональными характеристиками.
                    Мы производим отечественные угольные и дровяные грили по собственным технологиям, адаптированным под российский рынок на оптимально сочетает в себе стоимость с хорошим функциональными характеристиками. Мы производим отечественные угольные и дровяные грили по собственным технологиям
                </p>

                <div class="grills-list container clearfix">
                    <div class="col-md-3 grill-block-container">
                        <a href="#" class=" text-center grill-block">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/img/gt1.png" alt="" class="grill-img">

                            <p class="grill-name">
                                каминные  грили  
                            </p>
                        </a>
                    </div>

                    <div class="col-md-3 grill-block-container">
                        <a href="#" class=" text-center grill-block">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/img/gt2.png" alt="" class="grill-img">

                            <p class="grill-name">
                                ОСТРОВНЫЕ  грили  
                            </p>
                        </a>
                    </div>

                    <div class="col-md-3 grill-block-container">
                        <a href="#" class=" text-center grill-block">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/img/gt3.png" alt="" class="grill-img">

                            <p class="grill-name">
                                СЕРИЯ ГРИЛЕЙ  INOX  
                            </p>
                        </a>
                    </div>

                    <div class="col-md-3 grill-block-container">
                        <a href="#" class=" text-center grill-block">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/img/gt4.png" alt="" class="grill-img">

                            <p class="grill-name">
                                ДОПОЛНИТЕЛЬНОЕ  ОСНАЩЕНИЕ
                            </p>
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!--grills types end-->



<!--video section start-->
<section id="main-grills-video">
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <h2  class="section-header">наши грили в действии</h2>
                <p class="section-info">
                    Относится к числу островных. традиционных моделей финских гирлей-барбекю. Она оптимально сочетает в себе стоимость с хорошим функциональными характеристиками. Она оптимально сочетает в себе стоимость с хорошим функциональными характеристиками.
                    Мы производим отечественные угольные и дровяные грили по собственным технологиям, адаптированным под российский рынок на оптимально сочетает в себе стоимость с хорошим функциональными характеристиками. Мы производим отечественные угольные и дровяные грили по собственным технологиям
                </p>

                <div class="video-main clearfix">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/img/grill-blade.jpg" alt="" class="grill-blade">

                    <div class="col-md-10 col-md-offset-1 video-block">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1 video-block-container" id="video-main">
                                <iframe src="https://www.youtube.com/embed/pa68WP4m3lc" frameborder="0"></iframe>
                                <iframe src="https://www.youtube.com/embed/ir5U88d_3iI" frameborder="0"></iframe>
                                <iframe src="https://www.youtube.com/embed/LsAzNuwPFnw" frameborder="0"></iframe>
                            </div>
                        </div>
                    </div>

                    <img src="<?php echo get_stylesheet_directory_uri() ?>/img/grill-fork.jpg" alt="" class="grill-fork">
                </div>

                <a href="#" class="video-all">смотреть все</a>
            </div>

        </div>
    </div>
</section>
<!--video section end-->




<!--fireplace grills start-->
<section id="fp-grills">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2  class="section-header">каминные грили LAPPIGRILL</h2>
                <p class="section-info">
                    Объединяют в себе функционал финских гpилей - баpбекю и эстетику камина. Данные модели предназначены для эксплуатации на открытом воздухе, с возможностью максимально безопасного использования в беседках или на террасах. Регулировка высоты поддона с дровами/ углем производится съемной поворотной ручкой, расположенной над топкой.
                    Уникальный дизайн, мобильность (модель ST), возможность разработки топки по индивидуальному проекту (модель BOX) - откройте безграничные возможности LAPPIGRILL.
                </p>

                <div class="grills-product">
                    <div class="grills-product-block">
                        <div class="grills-product-container row">            
                            <div class="grills-info-blocks">
                                <div class="grills-product-nav">
                                    <a href="#">Посмотреть все модели</a>
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="grills-product-nav-row active clearfix">
                                            <a data-target="#lvs" aria-controls="lvs" role="tab" data-toggle="tab" class="grills-product-nav-btn ">lappigrill-vs</a>
                                        </li>

                                        <li class="grills-product-nav-row half-row  clearfix">
                                            <a  aria-controls="lst" data-target="#lst"  role="tab" data-toggle="tab" class="grills-product-nav-btn">lappigrill-st</a>
                                        </li>

                                        <li class="grills-product-nav-row half-row half-row last-elem clearfix">
                                            <a  aria-controls="lbox" data-target="#lbox" role="tab" data-toggle="tab" class="grills-product-nav-btn">lappigrill-box</a>
                                        </li>

                                    </ul>
                                </div>
                                <!--lappigrill-vs start-->
                                <div role="tabpanel" id="lvs" class="grills-product-info tab-pane clearfix active">
                                    <div class="grills-product-info-text">
                                        <h4 class="grills-product-info-text-header">lappigrill-vs</h4>
                                        <div class="grills-product-info-text-materials">Уголь + Дрова</div>
                                        <div class="grills-product-info-text-text">Модель VS объединяет в себе все возможности финского гpиля - баpбекю и эстетику камина. Двойные стенки купола с вентиляционным зазором и термоизолированный корпус обеспечивают максимальную безопасность для «пристенного» размещения и использова- ния в беседках и на террасах.</div>
                                        <div class="grills-product-info-text-text">Регулировка высоты поддона с дровами/углем производится поворотной ручкой, расположенной над топкой</div>
                                        <ul class="grill-product-params">
                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Размер очага:
                                                </div>
                                                <div class="grill-product-params-value">60 x 46 см</div>
                                            </li>

                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Толщина/материал очага:
                                                </div>
                                                <div class="grill-product-params-value">2 мм</div>
                                            </li>

                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Регулировка по высоте:
                                                </div>
                                                <div class="grill-product-params-value">Есть</div>
                                            </li>

                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Вид топлива:
                                                </div>
                                                <div class="grill-product-params-value">Уголь/Дрова</div>
                                            </li>

                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Гарантийный срок:
                                                </div>
                                                <div class="grill-product-params-value">2 года</div>
                                            </li>
                                        </ul>

                                        <a href="#" class="grills-product-info-text-more">Узнать подробности о модели</a>
                                    </div>


                                    <div class="grills-product-info-img">
                                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/firegrill-1.png" alt="">
                                    </div>

                                    <div class="grills-product-info-slider">
                                        <div class="slider-container" id="fp-vs-slider">
                                            <div class="slide">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/vs3.png" alt="">
                                            </div>

                                            <div class="slide">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/vs4.png" alt="">
                                            </div>

                                            <div class="slide">
                                                <div class="video-btn"></div>
                                                <iframe src="https://www.youtube.com/embed/pa68WP4m3lc" frameborder="0"></iframe>
                                            </div>

                                            <div class="slide">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/vs2.png"  alt="">
                                            </div>

                                            <div class="slide">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/firegrill-1.png" alt="">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!--lappigrill-vs end-->

                                <!--lappigrill-st start-->
                                <div role="tabpanel" id="lst" class="grills-product-info tab-pane clearfix">
                                    <div class="grills-product-info-text">
                                        <h4 class="grills-product-info-text-header">lappigrill-st</h4>
                                        <div class="grills-product-info-text-materials">Уголь</div>
                                        <div class="grills-product-info-text-text">Модель VS объединяет в себе все возможности финского гpиля - баpбекю и эстетику камина. Двойные стенки купола с вентиляционным зазором и термоизолированный корпус обеспечивают максимальную безопасность для «пристенного» размещения и использова- ния в беседках и на террасах.</div>
                                        <div class="grills-product-info-text-text">Регулировка высоты поддона с дровами/углем производится поворотной ручкой, расположенной над топкой</div>
                                        <ul class="grill-product-params">
                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Размер очага:
                                                </div>
                                                <div class="grill-product-params-value">60см</div>
                                            </li>

                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Толщина/материал очага:
                                                </div>
                                                <div class="grill-product-params-value">3 мм</div>
                                            </li>

                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Регулировка по высоте:
                                                </div>
                                                <div class="grill-product-params-value">Есть</div>
                                            </li>

                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Вид топлива:
                                                </div>
                                                <div class="grill-product-params-value">Уголь</div>
                                            </li>

                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Гарантийный срок:
                                                </div>
                                                <div class="grill-product-params-value">3 года</div>
                                            </li>
                                        </ul>

                                        <a href="#" class="grills-product-info-text-more">Узнать подробности о модели</a>
                                    </div>


                                    <div class="grills-product-info-img">
                                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/firegrill-1.png" alt="">
                                    </div>

                                    <div class="grills-product-info-slider">
                                        <div class="slider-container" id="fp-st-slider">
                                            <div class="slide">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/vs2.png" alt="">
                                            </div>

                                            <div class="slide">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/vs4.png" alt="">
                                            </div>

                                            <div class="slide">
                                                <div class="video-btn"></div>
                                                <iframe src="https://www.youtube.com/embed/pa68WP4m3lc" frameborder="0"></iframe>
                                            </div>

                                            <div class="slide">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/vs2.png"  alt="">
                                            </div>

                                            <div class="slide">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/firegrill-1.png" alt="">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!--lappigrill-st end-->

                                <!--lappigrill-box start-->
                                <div role="tabpanel" id="lbox" class="grills-product-info tab-pane clearfix">
                                    <div class="grills-product-info-text">
                                        <h4 class="grills-product-info-text-header">lappigrill-box</h4>
                                        <div class="grills-product-info-text-materials">Дрова</div>
                                        <div class="grills-product-info-text-text">Модель VS объединяет в себе все возможности финского гpиля - баpбекю и эстетику камина. Двойные стенки купола с вентиляционным зазором и термоизолированный корпус обеспечивают максимальную безопасность для «пристенного» размещения и использова- ния в беседках и на террасах.</div>
                                        <div class="grills-product-info-text-text">Регулировка высоты поддона с дровами/углем производится поворотной ручкой, расположенной над топкой</div>
                                        <ul class="grill-product-params">
                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Размер очага:
                                                </div>
                                                <div class="grill-product-params-value">46 см</div>
                                            </li>

                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Толщина/материал очага:
                                                </div>
                                                <div class="grill-product-params-value">2 мм</div>
                                            </li>

                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Регулировка по высоте:
                                                </div>
                                                <div class="grill-product-params-value">Есть</div>
                                            </li>

                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Вид топлива:
                                                </div>
                                                <div class="grill-product-params-value">Дрова</div>
                                            </li>

                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Гарантийный срок:
                                                </div>
                                                <div class="grill-product-params-value">2 года</div>
                                            </li>
                                        </ul>

                                        <a href="#" class="grills-product-info-text-more">Узнать подробности о модели</a>
                                    </div>


                                    <div class="grills-product-info-img">
                                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/firegrill-1.png" alt="">
                                    </div>

                                    <div class="grills-product-info-slider">
                                        <div class="slider-container" id="fp-box-slider">
                                            <div class="slide">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/vs2.png" alt="">
                                            </div>

                                            <div class="slide">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/vs1.png" alt="">
                                            </div>

                                            <div class="slide">
                                                <div class="video-btn"></div>
                                                <iframe src="https://www.youtube.com/embed/pa68WP4m3lc" frameborder="0"></iframe>
                                            </div>

                                            <div class="slide">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/vs3.png"  alt="">
                                            </div>

                                            <div class="slide">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/firegrill-1.png" alt="">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!--lappigrill-box end-->

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!--fireplace grills start-->





<!--bbq grills start-->
<section id="bbq-grills">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2  class="section-header">островные грили LAPPIGRILL</h2>
                <p class="section-info">
                    Традиционные модели финских стационарных грилей - барбекю. Используются для приготовления блюд на живом огне в беседках, на террасах и на открытом воздухе, применяя в качестве топлива дрова или угли. «Островные» модели сочетают в себе функциональность, современный дизайн и новейшие технологии производства. 
                </p>

                <div class="grills-product">
                    <div class="grills-product-block">
                        <div class="grills-product-container row">            
                            <div class="grills-info-blocks">
                                <div class="grills-product-nav">
                                    <a href="#">Посмотреть все модели</a>
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="grills-product-nav-row half-row active clearfix">
                                            <a  aria-controls="lbbq" data-target="#lbbq"  role="tab" data-toggle="tab" class="grills-product-nav-btn">lappigrill-bbq</a>
                                        </li>

                                        <li class="grills-product-nav-row half-row half-row last-elem clearfix">
                                            <a  aria-controls="lbbq90" data-target="#lbbq90" role="tab" data-toggle="tab" class="grills-product-nav-btn">lappigrill-90</a>
                                        </li>

                                    </ul>
                                </div>

                                <!--lappigrill-bbq start-->
                                <div role="tabpanel" id="lbbq" class="grills-product-info tab-pane active clearfix">
                                    <div class="grills-product-info-text">
                                        <h4 class="grills-product-info-text-header">lappigrill-bbq</h4>
                                        <div class="grills-product-info-text-materials">Дрова</div>
                                        <div class="grills-product-info-text-text">Усовершенствованная «островная» модель стационарного гриля-барбекю, имеющая - максимальную толщину стали очага. </div>
                                        <div class="grills-product-info-text-text">Сочетает в себе современный дизайн, функциональность и передовые технологии. <br>
                                            Бесступенчатая «лифтовая» система регулировки высоты поворотных подставок позволяет готовить блюда с дополнительным комфортом на дровах или углях.</div>
                                        <ul class="grill-product-params">
                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Размер очага:
                                                </div>
                                                <div class="grill-product-params-value"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/icon-rad.png" class="icon-rad" alt="">80 см</div>
                                            </li>

                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Толщина/материал очага:
                                                </div>
                                                <div class="grill-product-params-value">3 мм</div>
                                            </li>

                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Регулировка по высоте:
                                                </div>
                                                <div class="grill-product-params-value">Есть</div>
                                            </li>

                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Вид топлива:
                                                </div>
                                                <div class="grill-product-params-value">Дрова</div>
                                            </li>

                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Гарантийный срок:
                                                </div>
                                                <div class="grill-product-params-value">2 года</div>
                                            </li>
                                        </ul>

                                        <a href="#" class="grills-product-info-text-more">Узнать подробности о модели</a>
                                    </div>


                                    <div class="grills-product-info-img">
                                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/bbq-img.png" alt="">
                                    </div>

                                    <div class="grills-product-info-slider">
                                        <div class="slider-container" id="bbq-slider">
                                            <div class="slide">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/bbq1.png" alt="">
                                            </div>

                                            <div class="slide">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/bbq4.png" alt="">
                                            </div>

                                            <div class="slide">
                                                <div class="video-btn"></div>
                                                <iframe src="https://www.youtube.com/embed/pa68WP4m3lc" frameborder="0"></iframe>
                                            </div>

                                            <div class="slide">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/bbq2.png"  alt="">
                                            </div>

                                            <div class="slide">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/firegrill-1.png" alt="">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!--lappigrill-bbq end-->

                                <!--lappigrill-90 start-->
                                <div role="tabpanel" id="lbbq90" class="grills-product-info tab-pane  clearfix">
                                    <div class="grills-product-info-text">
                                        <h4 class="grills-product-info-text-header">lappigrill-90</h4>
                                        <div class="grills-product-info-text-materials">Дрова</div>
                                        <div class="grills-product-info-text-text">Усовершенствованная «островная» модель стационарного гриля-барбекю, имеющая - максимальную толщину стали очага. </div>
                                        <div class="grills-product-info-text-text">Сочетает в себе современный дизайн, функциональность и передовые технологии. <br>
                                            Бесступенчатая «лифтовая» система регулировки высоты поворотных подставок позволяет готовить блюда с дополнительным комфортом на дровах или углях.</div>
                                        <ul class="grill-product-params">
                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Размер очага:
                                                </div>
                                                <div class="grill-product-params-value">80 см</div>
                                            </li>

                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Толщина/материал очага:
                                                </div>
                                                <div class="grill-product-params-value">3 мм</div>
                                            </li>

                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Регулировка по высоте:
                                                </div>
                                                <div class="grill-product-params-value">Есть</div>
                                            </li>

                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Вид топлива:
                                                </div>
                                                <div class="grill-product-params-value">Дрова</div>
                                            </li>

                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Гарантийный срок:
                                                </div>
                                                <div class="grill-product-params-value">2 года</div>
                                            </li>
                                        </ul>

                                        <a href="#" class="grills-product-info-text-more">Узнать подробности о модели</a>
                                    </div>


                                    <div class="grills-product-info-img">
                                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/bbq-img.png" alt="">
                                    </div>

                                    <div class="grills-product-info-slider">
                                        <div class="slider-container" id="bbq90-slider">
                                            <div class="slide">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/bbq1.png" alt="">
                                            </div>

                                            <div class="slide">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/bbq2.png" alt="">
                                            </div>

                                            <div class="slide">
                                                <div class="video-btn"></div>
                                                <iframe src="https://www.youtube.com/embed/pa68WP4m3lc" frameborder="0"></iframe>

                                            </div>

                                            <div class="slide">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/bbq3.png"  alt="">
                                            </div>

                                            <div class="slide">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/firegrill-1.png" alt="">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!--lappigrill-90 end-->

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!--bbq grills end-->


<!--inox grills start-->
<section id="inox-grills">
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <h2  class="section-header">грили LAPPIGRILL СЕРИи INOX</h2>
                <p class="section-info">
                    Серия стационарных грилей для традиционного приготовления блюд на дровах или углях. Прочная конструкция гриля полностью выполнена из нержавеющей стали и идеально подходит для  использования на открытом воздухе, так и внутри помещений. Опциональные трубы позволяют использовать гриль внутри беседок и террас. 
                </p>


                <div class="grills-product">
                    <div class="grills-product-block">
                        <div class="grills-product-container row">            
                            <div class="grills-info-blocks">
                                <div class="grills-product-nav">
                                    <a href="#">Посмотреть все модели</a>
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="grills-product-nav-row half-row active clearfix">
                                            <a data-target="#linoxqg" aria-controls="linoxqg" role="tab" data-toggle="tab" class="grills-product-nav-btn ">inoxQ-g</a>
                                        </li>

                                        <li class="grills-product-nav-row half-row last-elem  clearfix">
                                            <a  aria-controls="linoxqs" data-target="#linoxqs"  role="tab" data-toggle="tab" class="grills-product-nav-btn">inoxQ-s</a>
                                        </li>

                                        <li class="grills-product-nav-row half-row  clearfix">
                                            <a  aria-controls="linoxvg" data-target="#linoxvg"  role="tab" data-toggle="tab" class="grills-product-nav-btn">inoxV-g</a>
                                        </li>

                                        <li class="grills-product-nav-row half-row last-elem  clearfix">
                                            <a  aria-controls="linoxvs" data-target="#linoxvs"  role="tab" data-toggle="tab" class="grills-product-nav-btn">inoxV-s</a>
                                        </li>

                                    </ul>
                                </div>

                                <!--lappigrill-inoxq-s start-->
                                <div role="tabpanel" id="linoxqg" class="grills-product-info tab-pane active clearfix">
                                    <div class="grills-product-info-text">
                                        <h4 class="grills-product-info-text-header">lappigrill-inoxQ-g</h4>
                                        <div class="grills-product-info-text-materials">Уголь/Дрова</div>
                                        <div class="grills-product-info-text-text">Стационарная модель гриля для открытых площадок, позволяет готовить блюда на дровах или углях.</div>

                                        <div class="grills-product-info-text-text">Внешнее покрытие TIN (титановое напыление) - имеет привлекательный внешний вид и легко чиститься. Верхняя часть конструкции гриля может быть установлена на несгораемую поверхность без использования тумбы.</div>

                                        <div class="grills-product-info-text-text">В комплекте регулируемый по высоте модуль для шашлыка.</div>
                                        <ul class="grill-product-params">
                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Размер очага:
                                                </div>
                                                <div class="grill-product-params-value">80 см</div>
                                            </li>

                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Толщина/материал очага:
                                                </div>
                                                <div class="grill-product-params-value">3 мм</div>
                                            </li>

                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Регулировка по высоте:
                                                </div>
                                                <div class="grill-product-params-value">Есть</div>
                                            </li>

                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Вид топлива:
                                                </div>
                                                <div class="grill-product-params-value">Уголь/Дрова</div>
                                            </li>

                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Гарантийный срок:
                                                </div>
                                                <div class="grill-product-params-value">3 года</div>
                                            </li>
                                        </ul>

                                        <a href="#" class="grills-product-info-text-more">Узнать подробности о модели</a>
                                    </div>


                                    <div class="grills-product-info-img">
                                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/inox-img.png" alt="">
                                    </div>

                                    <div class="grills-product-info-slider">
                                        <div class="slider-container" id="fp-box-slider">
                                            <div class="slide">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/in2.png" alt="">
                                            </div>

                                            <div class="slide">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/in1.png" alt="">
                                            </div>

                                            <div class="slide">
                                                <div class="video-btn"></div>
                                                <iframe src="https://www.youtube.com/embed/pa68WP4m3lc" frameborder="0"></iframe>

                                            </div>

                                            <div class="slide">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/in3.png"  alt="">
                                            </div>

                                            <div class="slide">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/in4.png" alt="">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!--lappigrill-inoxQ-g start-->


                                <!--lappigrill-inoxq-s start-->
                                <div role="tabpanel" id="linoxqs" class="grills-product-info tab-pane clearfix">
                                    <div class="grills-product-info-text">
                                        <h4 class="grills-product-info-text-header">lappigrill-inoxQ-s</h4>
                                        <div class="grills-product-info-text-materials">Дрова</div>
                                        <div class="grills-product-info-text-text">Стационарная модель гриля для открытых площадок, позволяет готовить блюда на дровах или углях.</div>

                                        <div class="grills-product-info-text-text">Внешнее покрытие TIN (титановое напыление) - имеет привлекательный внешний вид и легко чиститься. Верхняя часть конструкции гриля может быть установлена на несгораемую поверхность без использования тумбы.</div>

                                        <div class="grills-product-info-text-text">В комплекте регулируемый по высоте модуль для шашлыка.</div>
                                        <ul class="grill-product-params">
                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Размер очага:
                                                </div>
                                                <div class="grill-product-params-value">70 см</div>
                                            </li>

                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Толщина/материал очага:
                                                </div>
                                                <div class="grill-product-params-value">3 мм</div>
                                            </li>

                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Регулировка по высоте:
                                                </div>
                                                <div class="grill-product-params-value">Есть</div>
                                            </li>

                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Вид топлива:
                                                </div>
                                                <div class="grill-product-params-value">Уголь</div>
                                            </li>

                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Гарантийный срок:
                                                </div>
                                                <div class="grill-product-params-value">3 года</div>
                                            </li>
                                        </ul>

                                        <a href="#" class="grills-product-info-text-more">Узнать подробности о модели</a>
                                    </div>


                                    <div class="grills-product-info-img">
                                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/inox-img.png" alt="">
                                    </div>

                                    <div class="grills-product-info-slider">
                                        <div class="slider-container" id="fp-box-slider">
                                            <div class="slide">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/in2.png" alt="">
                                            </div>

                                            <div class="slide">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/in1.png" alt="">
                                            </div>

                                            <div class="slide">
                                                <div class="video-btn"></div>
                                                <iframe src="https://www.youtube.com/embed/pa68WP4m3lc" frameborder="0"></iframe>
                                            </div>

                                            <div class="slide">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/in3.png"  alt="">
                                            </div>

                                            <div class="slide">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/in4.png" alt="">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!--lappigrill-inoxQ-s start-->

                                <!--lappigrill-inoxv-g start-->
                                <div role="tabpanel" id="linoxvg" class="grills-product-info tab-pane clearfix">
                                    <div class="grills-product-info-text">
                                        <h4 class="grills-product-info-text-header">lappigrill-inoxV-g</h4>
                                        <div class="grills-product-info-text-materials">Дрова</div>
                                        <div class="grills-product-info-text-text">Стационарная модель гриля для открытых площадок, позволяет готовить блюда на дровах или углях.</div>

                                        <div class="grills-product-info-text-text">Внешнее покрытие TIN (титановое напыление) - имеет привлекательный внешний вид и легко чиститься. Верхняя часть конструкции гриля может быть установлена на несгораемую поверхность без использования тумбы.</div>

                                        <div class="grills-product-info-text-text">В комплекте регулируемый по высоте модуль для шашлыка.</div>
                                        <ul class="grill-product-params">
                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Размер очага:
                                                </div>
                                                <div class="grill-product-params-value">70 см</div>
                                            </li>

                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Толщина/материал очага:
                                                </div>
                                                <div class="grill-product-params-value">3 мм</div>
                                            </li>

                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Регулировка по высоте:
                                                </div>
                                                <div class="grill-product-params-value">Есть</div>
                                            </li>

                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Вид топлива:
                                                </div>
                                                <div class="grill-product-params-value">Уголь</div>
                                            </li>

                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Гарантийный срок:
                                                </div>
                                                <div class="grill-product-params-value">3 года</div>
                                            </li>
                                        </ul>

                                        <a href="#" class="grills-product-info-text-more">Узнать подробности о модели</a>
                                    </div>


                                    <div class="grills-product-info-img">
                                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/inox-img.png" alt="">
                                    </div>

                                    <div class="grills-product-info-slider">
                                        <div class="slider-container" id="fp-box-slider">
                                            <div class="slide">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/in3.png" alt="">
                                            </div>

                                            <div class="slide">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/in1.png" alt="">
                                            </div>

                                            <div class="slide">
                                                <div class="video-btn"></div>
                                                <iframe src="https://www.youtube.com/embed/pa68WP4m3lc" frameborder="0"></iframe>
                                            </div>

                                            <div class="slide">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/in4.png"  alt="">
                                            </div>

                                            <div class="slide">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/in3.png" alt="">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!--lappigrill-inoxv-g end-->

                                <!--lappigrill-inoxv-s start-->
                                <div role="tabpanel" id="linoxvs" class="grills-product-info tab-pane clearfix">
                                    <div class="grills-product-info-text">
                                        <h4 class="grills-product-info-text-header">lappigrill-inoxV-s</h4>
                                        <div class="grills-product-info-text-materials">Дрова</div>
                                        <div class="grills-product-info-text-text">Стационарная модель гриля для открытых площадок, позволяет готовить блюда на дровах или углях.</div>

                                        <div class="grills-product-info-text-text">Внешнее покрытие TIN (титановое напыление) - имеет привлекательный внешний вид и легко чиститься. Верхняя часть конструкции гриля может быть установлена на несгораемую поверхность без использования тумбы.</div>

                                        <div class="grills-product-info-text-text">В комплекте регулируемый по высоте модуль для шашлыка.</div>
                                        <ul class="grill-product-params">
                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Размер очага:
                                                </div>
                                                <div class="grill-product-params-value">80 см</div>
                                            </li>

                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Толщина/материал очага:
                                                </div>
                                                <div class="grill-product-params-value">3 мм</div>
                                            </li>

                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Регулировка по высоте:
                                                </div>
                                                <div class="grill-product-params-value">Есть</div>
                                            </li>

                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Вид топлива:
                                                </div>
                                                <div class="grill-product-params-value">Уголь/Дрова</div>
                                            </li>

                                            <li class="clearfix">
                                                <div class="grill-product-params-param">
                                                    Гарантийный срок:
                                                </div>
                                                <div class="grill-product-params-value">2 года</div>
                                            </li>
                                        </ul>

                                        <a href="#" class="grills-product-info-text-more">Узнать подробности о модели</a>
                                    </div>


                                    <div class="grills-product-info-img">
                                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/inox-img.png" alt="">
                                    </div>

                                    <div class="grills-product-info-slider">
                                        <div class="slider-container" id="fp-box-slider">
                                            <div class="slide">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/in1.png" alt="">
                                            </div>

                                            <div class="slide">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/in2.png" alt="">
                                            </div>

                                            <div class="slide">
                                                <div class="video-btn"></div>
                                                <iframe src="https://www.youtube.com/embed/pa68WP4m3lc" frameborder="0"></iframe>
                                            </div>

                                            <div class="slide">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/in4.png"  alt="">
                                            </div>

                                            <div class="slide">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/in1.png" alt="">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!--lappigrill-inoxv-s end-->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--inox grills end-->




<!--additional equipment start-->
<section id="add-equipment">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2  class="section-header">дополнительное оснащение</h2>
                <p class="section-info">
                    Относится к числу островных. традиционных моделей финских гирлей-барбекю. <br>
                    Она оптимально сочетает в себе стоимость с хорошим функциональными характеристиками.
                </p>

                <div class="add-equipment">
                    <!--add-eq slider start-->
                    <div class="add-equipment-container" id="addeq">
                        <div class="add-equipment-slide">
                            <div class="add-equipment-inner">
                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/addeqmain.png" alt="" class="add-equipment-img">
                                <div class="add-equipment-header">Название оснащения</div>
                                <div class="add-equipment-info">
                                    <div class="add-equipment-info-text">
                                        Относится к числу островных. традиционных моделей финских гирлей-барбекю.
                                        Она оптимально сочетает в себе стоимость с хорошим функциональными характеристиками.
                                    </div>

                                    <a href="#" class="add-equipment-info-more">
                                        подробнее
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="add-equipment-slide">
                            <div class="add-equipment-inner">
                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/addeq1.png" alt="" class="add-equipment-img">
                                <div class="add-equipment-header">Название оснащения 2</div>
                                <div class="add-equipment-info">
                                    <div class="add-equipment-info-text">
                                        Относится к числу  традиционных моделей финских гирлей-барбекю.
                                    </div>

                                    <a href="#" class="add-equipment-info-more">
                                        подробнее
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="add-equipment-slide">
                            <div class="add-equipment-inner">
                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/addeq2.png" alt="" class="add-equipment-img">
                                <div class="add-equipment-header">Название оснащения 2</div>
                                <div class="add-equipment-info">
                                    <div class="add-equipment-info-text">
                                        Относится к числу  традиционных моделей финских гирлей-барбекю.
                                    </div>

                                    <a href="#" class="add-equipment-info-more">
                                        подробнее
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="add-equipment-slide">
                            <div class="add-equipment-inner">
                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/addeq3.png" alt="" class="add-equipment-img">
                                <div class="add-equipment-header">Название оснащения 2</div>
                                <div class="add-equipment-info">
                                    <div class="add-equipment-info-text">
                                        Относится к числу  традиционных моделей финских гирлей-барбекю.
                                    </div>

                                    <a href="#" class="add-equipment-info-more">
                                        подробнее
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="add-equipment-slide">
                            <div class="add-equipment-inner">
                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/addeq4.png" alt="" class="add-equipment-img">
                                <div class="add-equipment-header">Название оснащения 2</div>
                                <div class="add-equipment-info">
                                    <div class="add-equipment-info-text">
                                        Относится к числу  традиционных моделей финских гирлей-барбекю.
                                    </div>

                                    <a href="#" class="add-equipment-info-more">
                                        подробнее
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="add-equipment-slide">
                            <div class="add-equipment-inner">
                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/addeq2.png" alt="" class="add-equipment-img">
                                <div class="add-equipment-header">Название оснащения 2</div>
                                <div class="add-equipment-info">
                                    <div class="add-equipment-info-text">
                                        Относится к числу  традиционных моделей финских гирлей-барбекю.
                                    </div>

                                    <a href="#" class="add-equipment-info-more">
                                        подробнее
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="add-equipment-slide">
                            <div class="add-equipment-inner">
                                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/addeq1.png" alt="" class="add-equipment-img">
                                <div class="add-equipment-header">Название оснащения 2</div>
                                <div class="add-equipment-info">
                                    <div class="add-equipment-info-text">
                                        Относится к числу  традиционных моделей финских гирлей-барбекю.
                                    </div>

                                    <a href="#" class="add-equipment-info-more">
                                        подробнее
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--add-eq slider end-->
                    <a href="#" class="addeq-all">Посмотреть все</a>

                </div>
            </div>
        </div>
    </div>
</section>
<!--additional equipment end-->



<!--dilers start-->
<section id="dilers">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2  class="section-header">наши дилеры в 30 регионах</h2>
                <div class="dilers-block clearfix">
                    <div class="col-md-6">
                        <p class="section-info">
                            LAPPIGRILL всегда стремится быть ближе к клиентам и дилерам, поэтому мы <br> предлагаем наилучшие условия сотрудничества.
                        </p>

                        <p class="section-info">
                            Приобрести нашу продукцию вы можете  у <a href="#" class="colored">официальных дилеров</a> <br>
                            на территории РФ и СНГ.
                        </p>
                    </div>

                    <div class="col-md-6 map">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/map.png" alt="">
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!--dilers end-->



<?php get_footer(); ?>
