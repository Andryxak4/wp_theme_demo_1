<?php
/*
 * Template name: Блог
 */
get_header();
$fields = get_fields();

function pagination($total_pages = '') {
    global $paged;
    if(empty($paged)) $paged = 1;

    if($total_pages == '') {
        global $wp_query;
        $total_pages = $wp_query->max_num_pages;
        if(!$total_pages) {
            $total_pages = 1;
        }
    }
?>
<nav class="page-nav">
    <ul class="pagination clearfix">
<?php
    if ($total_pages != 1) {
       $str = get_pagenum_link($paged - 1);

        if ($paged > 1) {
            echo "<li class='page-item'><a class='page-link prev' href='".$str."'></a></li>";
        }

        for ($i=1; $i <= $total_pages; $i++) {
            if ($total_pages != 1 && ($i < $paged) && $paged - $i == 2) {
                echo "<li class='page-item pag_small'><a class='page-link  ' href=".get_pagenum_link($i)."> {$i} </a></li>";
            }
            //main part starts
            if ( $total_pages != 1 && ($i > $paged && $i - $paged == 1 ) || ($paged == $i) || ($i < $paged && $paged - $i == 1)) {
                echo ($paged == $i)
                    ? " <li class='page-item'><a class='page-link active ' href=".get_pagenum_link($i)."> {$i} </a></li>"
                    : "<li class='page-item'><a class='page-link' href=".get_pagenum_link($i).">{$i}</a></li>";
            }
            //main part end
            if ($total_pages != 1 && ($i > $paged) &&  $i - $paged == 2) {
                echo "<li class='page-item pag_small_bottom'><a class='page-link  ' href=".get_pagenum_link($i)."> {$i} </a></li>";
            }
        }
        if ($paged < $total_pages ) {
            echo "<li class='page-item'><a class='page-link next' href=".get_pagenum_link($paged+1)."></a></li>";
        }
    }
    ?>
    </ul>
</nav>
<?php
}
?>
<?php $parent = get_post($post->post_parent); ?>
<!--product section start-->
<section id="years">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                    <li><a href="<?= get_permalink($parent->ID) ?>"><?= get_the_title($parent->ID); ?></a></li>
                    <li class="active"><?php the_title() ?></li>
                </ol>

                <h2  class="section-header">
                    <?php the_title() ?>
                </h2>

                <?php
                if (!empty($parent)):
                    $args = array(
                        'post_parent' => $parent->ID,
                        'post_type' => 'page',
                        'order' => 'ASC',
                        'numberposts' => -1,
                        'post_status' => 'publish'
                    );
                    $children = get_children($args);
                endif;
                ?>

                <div class="nav-panel clearfix">
                    <?php if ($parent && !empty($parent)): ?>
                        <a class="nav-panel-block" href="<?= get_permalink($parent->ID) ?>"><?= $parent->post_title ?></a>
                    <?php endif; ?>
                    <?php
                    if ($parent && $children):
                        foreach ($children as $child):// var_dump($child->ID);
                            echo '<a class="nav-panel-block'.($child->ID == $post->ID ? " active" : "").'"'
                                .' href="' . get_permalink($child->ID) . '">' . $child->post_title . '</a>';
                            ?>

                        <?php endforeach; ?>
                    <?php endif; ?>

                </div>
            </div>

            <?php

            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

            $args = array(
                'post_type'         => 'post',
                'post_status'       => 'publish',
                'posts_per_page'    => 3,
                'paged'             =>  $paged,
            );

            $wp_query = new WP_Query($args);

            ?>
            <?php if ($wp_query->have_posts()): ?>
                <div class="years-blog col-md-12">
                    <div class="years-blog-container">

                        <?php while ($wp_query->have_posts()): $wp_query->the_post(); ?>
                            <div class="years-blog-post row">
                                <div class="years-blog-post-img col-md-6">
                                    <?php
                                    $post_thumbnail = get_the_post_thumbnail_url($wp_query->post->ID);

                                    if (!empty($post_thumbnail)):
                                        ?>
                                        <img src="<?= $post_thumbnail ?>" alt="<?php the_title() ?>">
                                    <?php endif; ?>
                                </div>

                                <div class="years-blog-post-text  col-md-6">
                                    <div class="years-blog-post-text-header">
                                        <?php the_title(); ?>
                                    </div>
                                    <div class="years-blog-post-text-date">
                                        <?php the_date('d/m/Y') ?>
                                    </div>
                                    <div class="years-blog-post-text-text">
                                        <?php the_excerpt(); ?>
                                    </div>
                                    <a href="<?php the_permalink() ?>" class="years-blog-post-text-more">Читать новость полностью</a>
                                </div>
                            </div>

                        <?php endwhile; ?>
                        <?php pagination($wp_query->max_num_pages); ?>

                    </div>
                </div>
                <?php
            endif;
            wp_reset_query();
            wp_reset_postdata();
            ?>

        </div>
    </div>
</section>
<!--product section end-->

<?php get_footer() ?>
