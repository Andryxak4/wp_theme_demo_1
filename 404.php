<?php
get_header();
?>

<section id="page404">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                </ol>
            </div>

            <div class="error-page error-page col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0">
                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/404.png" class="error-page-img" alt="">
                <div class="error-page-text">
                    извините, но Произошла ошибка, такой страницы не существует
                </div>
                <a href="/" class="error-page-back">
                    на главную
                </a>
            </div>
        </div>
    </div>
</section>

<?php get_footer() ?>




