<?php
/*
 * Template name: Гарантия и Сервис
 */
get_header();
$fields = get_fields();
$parent = get_post($post->post_parent);
?>


<!--product section start-->
<section id="years">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                    <li><a href="<?= get_permalink($parent->ID) ?>">7 лет</a></li>
                    <li class="active">Гарантия и сервис</li>
                </ol>

                <h2  class="section-header">
                    <?php the_title() ?>
                </h2>


                <?php
                if (!empty($parent)):
                    $args = array(
                        'post_parent' => $parent->ID,
                        'post_type' => 'page',
                        'order' => 'ASC',
                        'numberposts' => -1,
                        'post_status' => 'publish'
                    );
                    $children = get_children($args);
                endif;
                ?>

                <div class="nav-panel clearfix">
                    <?php if ($parent && !empty($parent)): ?>
                        <a class="nav-panel-block" href="<?= get_permalink($parent->ID) ?>"><?= $parent->post_title ?></a>
                    <?php endif; ?>
                    <?php
                    if ($parent && $children):
                        foreach ($children as $child):
                            echo '<a class="nav-panel-block'.($child->ID == $post->ID ? " active" : "").'"'
                                .' href="' . get_permalink($child->ID) . '">' . $child->post_title . '</a>';
                            ?>

                        <?php endforeach; ?>
                    <?php endif; ?>

                </div>
            </div>

            <?php include 'templates/warranty.php'; ?>

        </div>
    </div>
</section>
<!--product section end-->


<?php get_footer() ?>