<?php
/*
 * Template name: Контакты
 */

get_header();
?>
<?php
$fields = get_fields();
//var_dump($fields);
?>
<!--contacts section start-->
<section id="contacts">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                    <li class="active">Контакты</li>
                </ol>

                <h2  class="section-header">
                    <?= $fields['block_title'] ?>
                </h2>
            </div>

            <div class="contact col-md-12">
                <div class="contact-block">
                    <h4 class="contact-header">
                        <?= $fields['block_subtitle'] ?>
                    </h4>
                    <p class="contact-text">  
                        <?= $fields['block_description'] ?>
                    </p>
                </div>

                <div class="form-container row">

                    <?php if ($fields['company_name']): ?>
                        <div class="col-lg-4 col-lg-offset-0 col-md-4 col-md-offset-0 col-sm-6 col-sm-offset-3 col-xs-12 col-xs-offset-0">
                            <div class="address">
                                <div class="address-header">
                                    <?= $fields['company_name'] ?>
                                </div>
                                <?php if ($fields['contacts']): ?>
                                    <?php foreach ($fields['contacts'] as $contact): ?>
                                        <div class="address-subheader">
                                            <?= $contact['department'] ?>
                                        </div>

                                        <ul class="address-list">
                                            <?php if ($contact['phone']): ?>
                                                <li class="clearfix">
                                                    <div class="address-list-name">
                                                        Телефон:
                                                    </div>

                                                    <div class="address-list-value">
                                                        <?= $contact['phone'] ?>
                                                    </div>
                                                </li>
                                            <?php endif; ?>
                                            <?php if ($contact['email']): ?>
                                                <li class="clearfix">
                                                    <div class="address-list-name">
                                                        E-mail:
                                                    </div>
                                                    <div class="address-list-value">
                                                        <?= $contact['email'] ?>
                                                    </div>
                                                </li>
                                            <?php endif; ?>
                                        </ul>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                            </div>
                        </div>
                    <?php endif; ?>



                    <?php
                    if ($fields['form']):
                        $form = $fields['form'];
                        ?>
                        <div class="col-lg-8 col-lg-offset-0 col-md-8 col-md-offset-0 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0">
                            <?php echo do_shortcode('[contact-form-7 id="' . $form->ID . '" title="' . $form->post_title . '" html_class="contact-form"]') ?>
                            <button id="show_modal_thanks" data-toggle="modal" data-target="#acc-form" style="display: none;">отправить</button>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!--contacts section end-->

<!--modal start-->
<div class="modal fade" id="acc-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-main">
        <div class="modal-main-header">
          Спасибо за обращение в LappiGrill
        </div>
        <div class="modal-main-text">
          Мы свяжемся с Вами в ближайшее время
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="modal-close" data-dismiss="modal">Закрыть</button>
      </div>
    </div>
  </div>
</div>
<script>
    var wpcf7Elm = document.querySelector( '.wpcf7' );
    wpcf7Elm.addEventListener( 'wpcf7mailsent', function( event ) {
        $('#show_modal_thanks').click();
    }, false );
</script>
<!--modal end-->
<?php get_footer(); ?>



