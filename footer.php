<!--footer start-->
<?php
$fields = get_fields('options');
$compare_page_url = arrayItem($fields, 'comparision_page_url', '#');
?>
<footer id="footer">
    <div class="footer-main">
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-md-offset-0 col-sm-4 col-sm-offset-1 col-xs-6 col-xs-offset-0 footer-nav">
                    <div class="footer-nav-header">
                        <a href="<?= $fields['block_1_title_url'] ?>"><?= $fields['Block_1_title'] ?></a>
                    </div>
                    <?php if ($fields['block_1_items']): ?>
                        <ul class="footer-nav-list">
                            <?php foreach ($fields['block_1_items'] as $item): ?>
                                <li><a href="<?= $item['url'] ?>"><?= $item['label'] ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif ?>
                </div>

                <div class="col-md-2 col-md-offset-1 col-sm-4 col-sm-offset-2 col-xs-6 col-xs-offset-0 footer-nav">
                    <div class="footer-nav-header">
                        <a href="<?= $fields['block_2_title_url'] ?>"><?= $fields['Block_2_title'] ?></a>
                    </div>
                    <?php if ($fields['block_2_items']): ?>
                        <ul class="footer-nav-list">
                            <?php foreach ($fields['block_2_items'] as $item): ?>
                                <li><a href="<?= $item['url'] ?>"><?= $item['label'] ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif ?>
                </div>

                <div class="col-md-2 col-md-offset-1 col-sm-4 col-sm-offset-1 col-xs-6 col-xs-offset-0 footer-nav">
                    <div class="footer-nav-header">
                        <a href="<?= $fields['block_3_title_url'] ?>"><?= $fields['Block_3_title'] ?></a>
                    </div>
                    <?php if ($fields['block_3_items']): ?>
                        <ul class="footer-nav-list">
                            <?php foreach ($fields['block_3_items'] as $item): ?>
                                <li><a href="<?= $item['url'] ?>"><?= $item['label'] ?></a></li>
                            <?php endforeach; ?>

                        </ul>
                    <?php endif ?>
                </div>

                <div class="col-md-3 col-md-offset-1 col-sm-4 col-sm-offset-2 col-xs-6 col-xs-offset-0 footer-nav">
                    <div class="footer-nav-header colored">
                        <a href="<?= $fields['block_4_title_url'] ?>"><?= $fields['Block_4_title'] ?></a>
                    </div>

                    <?php
                    $phone1 = arrayItem($fields, 'footer_phone_retail');
                    $phone1_comment = arrayItem($fields, 'footer_phone_retail_comment');
                    $email1 = arrayItem($fields, 'footer_email_retail');
                    $email1_comment = arrayItem($fields, 'footer_email_retail_comment');
                    $phone2 = arrayItem($fields, 'footer_phone_sale');
                    $phone2_comment = arrayItem($fields, 'footer_phone_sale_comment');
                    $email2 = arrayItem($fields, 'footer_email_sale');
                    $email2_comment = arrayItem($fields, 'footer_email_sale_comment');
                    ?>
                    <ul class="footer-nav-list contacts">
                      <li>
                        <?php if ($phone1 || $phone1_comment): ?>
                          <div>
                          <?php if ($phone1): ?>
                            <?php if ($phone1_digits=preg_replace("/[^0-9]/", "", $phone1)): ?>
                              <img src="<?php echo get_stylesheet_directory_uri() ?>/img/footer-icon-call.png" alt="" class="icon">
                              <a href="tel:+<?= $phone1_digits ?>"><?= $phone1 ?></a>
                            <?php else: ?>
                              <span class="footer-nav-list-contact-comment"><?= $phone1 ?></span>
                            <?php endif; ?>
                          <?php endif; ?>
                          <?php if ($phone1_comment): ?>
                              <span class="footer-nav-list-contact-comment"><?= $phone1_comment ?></span>
                          <?php endif; ?>
                          </div>
                        <?php endif; ?>

                        <?php if ($email1 || $email1_comment): ?>
                          <div>
                            <?php if ($email1): ?>
                              <img src="<?php echo get_stylesheet_directory_uri() ?>/img/footer-icon-mail.png" alt="" class="icon icon-mail">
                              <a href="mailto:<?= $email1 ?>"><?= $email1 ?></a>
                            <?php endif; ?>
                            <?php if ($email1_comment): ?>
                              <span class="footer-nav-list-contact-comment"><?= $email1_comment ?></span>
                            <?php endif; ?>
                          </div>
                        <?php endif; ?>
                      </li>

                      <li>
                        <?php if ($phone2 || $phone2_comment): ?>
                          <div>
                          <?php if ($phone2): ?>
                            <?php if ($phone2_digits=preg_replace("/[^0-9]/", "", $phone2)): ?>
                              <img src="<?php echo get_stylesheet_directory_uri() ?>/img/footer-icon-call.png" alt="" class="icon">
                              <a href="tel:+<?= $phone2_digits ?>"><?= $phone2 ?></a>
                            <?php else: ?>
                              <span class="footer-nav-list-contact-comment"><?= $phone2 ?></span>
                            <?php endif; ?>
                          <?php endif; ?>
                          <?php if ($phone2_comment): ?>
                              <span class="footer-nav-list-contact-comment"><?= $phone2_comment ?></span>
                          <?php endif; ?>
                          </div>
                        <?php endif; ?>

                        <?php if ($email2 || $email2_comment): ?>
                          <div>
                            <?php if ($email2): ?>
                              <img src="<?php echo get_stylesheet_directory_uri() ?>/img/footer-icon-mail.png" alt="" class="icon icon-mail">
                              <a href="mailto:<?= $email2 ?>"><?= $email2 ?></a>
                            <?php endif; ?>
                            <?php if ($email2_comment): ?>
                              <span class="footer-nav-list-contact-comment"><?= $email2_comment ?></span>
                            <?php endif; ?>
                          </div>
                        <?php endif; ?>
                      </li>

                        <li>
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/img/footer-icon-youtubel.png" alt="" class="icon">
                            Мы на 
                            <a href="<?= $fields['youtube'] ?>" target="_blank" class="colored">
                                You Tube
                            </a>
                        </li>
                    </ul>

                    <a href="<?= $fields['contact_us_page_url'] ?>" class="contact-us">Свяжитесь с нами</a>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <a href="<?= $fields['contact_us_page_url'] ?>" class="contact-us mobile">Свяжитесь с нами</a>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <?= $fields['copyrighted'] ?>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-layer"></div>
</footer>
<!--footer end-->

<!--fade comparision block start-->
<div class="product-fade added">
  <div class="container">
    <div class="row">
      <div class="product-container clearfix">
        <div class="product-fade-img">
          <img src="" alt="">
        </div>  

        <div class="product-fade-text">
          <div class="product-fade-text-header">
            <span class="product-name"></span> добавлен к сравнению  
          </div>

          <div class="product-fade-text-total">
              Всего в списке сравнения <span class="comparision-count"></span> товара(ов)
          </div>
        </div>
        
        <div class="product-fade-right">
          <a href="<?= $compare_page_url; ?>" class="product-fade-comparision clearfix">
            <span class="product-fade-comparision-amount"></span>
            <span class="product-fade-comparision-icon "></span>
            <span class="product-fade-comparision-text">Сравнение</span>
          </a>
          <div class="comparision-close">
            <div class="comparision-close-icon">
              <div class="line"></div>
              <div class="line line45"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="product-fade removed">
  <div class="container">
    <div class="row">
      <div class="product-container clearfix">
        <div class="product-fade-img">
          <img src="" alt="">
        </div>  

        <div class="product-fade-text">
          <div class="product-fade-text-header">
            <span class="product-name"></span> удален из списка сравнения  
          </div>

          <div class="product-fade-text-total">
            Всего в списке сравнения <span class="comparision-count"></span> товара(ов)
          </div>
        </div>
        
        <div class="product-fade-right">
          <a href="<?= $compare_page_url; ?>" class="product-fade-comparision clearfix">
            <span class="product-fade-comparision-amount"></span>
            <span class="product-fade-comparision-icon "></span>
            <span class="product-fade-comparision-text">Сравнение</span>
          </a>
          <div class="comparision-close">
            <div class="comparision-close-icon">
              <div class="line"></div>
              <div class="line line45"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--fade comparision block end-->

<!-- modal layout start -->
<div class="popup-layout"></div>
<!-- modal layout end -->

<!-- popup for one click start-->
<div class="popup one-click-popup">
  <div class="popup-wrapper">
    <div class="popup-close">
      <div class="popup-close-icon">
        <div class="line hor"></div>
        <div class="line vert"></div>
      </div>
    </div>
    <div class="popup-header"></div>
    <div class="popup-body">
      <form action="#" class="one-click-order-form">
        <input type="hidden" name="product_id">
        <div class="inputs-group clearfix">
          <input type="text" name="name" placeholder="Ваше имя" required>
          <input type="text" name="phone" placeholder="Ваш телефон" required>
          <textarea id="" name="comment" placeholder="Сообщение"></textarea>
        </div>

        <div class="agreement">
          <div class="popup-checkbox clearfix">
            <input type="checkbox" id="agreement-accept" name="accept_terms" value="1" checked>
            <label for="agreement-accept"></label>
          </div>
          <div class="agreement-text">
            С <a href="<?= $fields['privacy_policy_page']; ?>" class="popup-link">политикой конфиденциальности</a> ознакомлен
            С <a href="<?= $fields['privacy_policy_page']; ?>" class="popup-link" target="_blank">политикой конфиденциальности</a> ознакомлен;
          </div>
        </div>

        <div class="captcha">Капча</div>

        <button type="submit" class="popup-submit one-click-order-form-submit">
          Купить
        </button>
      </form>

      <img src="<?php echo get_stylesheet_directory_uri() ?>/img/preloader.gif" class="popup-preloader" alt="" >
      
      <div class="one-click-order-thankyou">
        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/thanks-img.png" class="one-click-order-thankyou-img" alt="">
        <h2 class="one-click-order-thankyou-header">Поздравляем с покупкой!</h2>
        <div class="one-click-order-thankyou-text">
          <p>
            Ваш заказ <span class="order-number"></span> успешно оформлен.
          </p>
          <p>
            Наши менеджеры свяжутся с Вами!
          </p>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- popup for one click end-->

<!-- popup for cart start-->
<div class="popup cart-popup">
  <div class="popup-wrapper">
    <div class="popup-close">
      <div class="popup-close-icon">
        <div class="line hor"></div>
        <div class="line vert"></div>
      </div>
    </div>
    <div class="popup-header">корзина</div>
    <div class="popup-body">
      <form action="#">
        <!-- product header start-->
        <div class="cart-row row-header clearfix">
          <div class="cell cell-name">Наименование товара</div>
          <div class="cell cell-artic">Артикул</div>
          <div class="cell cell-quantity">Кол</div>
          <div class="cell cell-price">Цена</div>
          <div class="cell cell-remove">убрать</div>
        </div>
        <!-- product header end-->

        <?php foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item): ?>
        <?php $wc_product = $cart_item['data']; ?>
        <!-- product row start-->
        <div class="cart-row row-product clearfix" data-product-id="<?= $wc_product->id; ?>">
          <div class="cell cell-name">
            <div class="product-header-mobile">
              Наименование товара
            </div>
            <div class="product-img">
             <img src="<?php echo get_stylesheet_directory_uri() ?>/img/cart-popup-img.png" alt="">

             <div class="product-img-hover">
               <img src="<?= get_the_post_thumbnail_url($wc_product->id); ?>" alt="">
             </div>
            </div>
            <?= $wc_product->name; ?>
          </div>
          <div class="cell cell-artic">
            <?= $wc_product->get_sku() ?: "&nbsp;"; ?>
          </div>
          <div class="cell cell-quantity">
            <div class="product-header-mobile">
              Количество
            </div>
            <div class="cell-quantity-wrapper">
              <span class="quantity-decr"></span>
              <input type="text" value="<?= $cart_item['quantity']; ?>">
              <span class="quantity-inc"></span>
            </div>
          </div>
          <div class="cell cell-artic mobile">
            <div class="product-header-mobile">
              Артикул
            </div>
            <?= $wc_product->get_sku() ?: "&nbsp;"; ?>
          </div>
          <div class="cell cell-price">
            <div class="product-header-mobile">
              Цена
            </div>
            <div class="amount"><?= number_format($wc_product->get_price() ?: 0, 0, '.', ' '); ?></div> руб
          </div>
          <div class="cell cell-remove">
            <div class="product-remove-wrapper">
              <img class="product-remove" src="<?php echo get_stylesheet_directory_uri() ?>/img/cart-popup-remove.png" alt="">
              <div class="product-remove-mobile">
                Убрать
              </div>
            </div>
          </div>
        </div>
        <!-- product row end-->
        <?php endforeach; ?>

        <!-- footer nav start -->
        <div class="cart-popup-nav clearfix">
          <div class="total">
            <span class="total-header">Цена всего:</span> 
            <span class="amount"><?= WC()->cart->total; ?></span> 
            РУБ
          </div>

          <div class="cart-popup-nav-btns clearfix">
            <a href="javascript:void(0);" class="popup-btn continue">Продолжить покупки</a>
            <a href="<?= wc_get_cart_url(); ?>" class="popup-btn order">Оформить заказ</a>
          </div>
        </div>
        <!-- footer nav end -->

      </form>
    </div>
  </div>
</div>

<!-- popup for cart end-->



<?php wp_footer(); ?>

</body>
</html>
