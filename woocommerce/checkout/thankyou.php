<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $wp;
$order_id = arrayItem($wp->query_vars, 'order-received');
?>

<section class="thanks">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2 thanks-wrapper">
        <img src="<?= get_template_directory_uri() ?>/img/thanks-img.png" class="thanks-img" alt="">
        <h2 class="thanks-header">Поздравляем с покупкой!</h2>
        <div class="thanks-text">
          <p>
            Ваш заказ <span class="order-number">#<?= $order_id; ?></span> успешно оформлен.
          </p>
          <p>
            Наши менеджеры свяжутся с Вами!
          </p>
        </div>
        
        <a href="/" class="thanks-btn">На главную</a>
      </div>
    </div>
  </div>
</section>