<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

//wc_print_notices();

do_action( 'woocommerce_before_cart' ); ?>

<div class="container">
  <div class="row">
    <div class="cart-wrapper col-lg-12 col-md-12 col-sm-12 col-xs-12 clearfix">

      <?= dimox_breadcrumbs(); ?>

      <h2 class="section-header">
        <?php the_title() ?>
      </h2>

      <form class="woocommerce-cart-form clearfix" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
        <?php do_action( 'woocommerce_before_cart_table' ); ?>

        <table class="shop_table shop_table_responsive cart woocommerce-cart-form__contents" cellspacing="0">
            <thead>
                <tr>

    <?php /*				<th class="product-thumbnail">&nbsp;</th> */ ?>
                    <th class="product-name">Наименование товара</th>
                    <th class="product-articul">Артикул</th>
                    <th class="product-quantity">Кол.</th>
                    <th class="product-articul mobile">Артикул</th>
                    <th class="product-price">Цена</th>
            <?php /*		<th class="product-subtotal"><?php _e( 'Total', 'woocommerce' ); ?></th> */ ?>
                    <th class="product-remove">Убрать</th>
                </tr>
            </thead>
            <tbody>
                <?php do_action( 'woocommerce_before_cart_contents' ); ?>

                <?php
                foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
                    $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
                    $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

                    if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
                        $product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
                        ?>
                        <tr class="woocommerce-cart-form__cart-item <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>"
                            data-product-id="<?= $product_id; ?>">
    <!--
                            <td class="product-thumbnail">

                            </td>
    -->

                            <td class="product-name">
                                <div class="table-product-mobile">
                                  Наименование товара
                                </div>
                                <div class="product-img">
                                  <img src="<?php echo get_stylesheet_directory_uri() ?>/img/cart-popup-img.png" alt="">
                                  <div class="product-thumbnail-popup">
                                    <?php
                                      $thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
                                      /*
                                      if ( ! $product_permalink ) {
                                          echo $thumbnail;
                                      } else {
                                          printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail );
                                      } */
                                      echo $thumbnail;
                                    ?>  
                                  </div>
                                </div>

                                <?php /*
                                    if ( ! $product_permalink ) {
                                        echo apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;';
                                    } else {
                                        echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $cart_item_key );
                                    }*/
                                    echo apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;';

                                    // Meta data
                                    echo WC()->cart->get_item_data( $cart_item );

                                    // Backorder notification
                                    if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
                                        echo '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>';
                                    }
                                ?>
                            </td>

                            <td class="product-articul">
                              <?= $cart_item['data']->get_sku() ?: "&nbsp;"; ?>
                            </td>	

                            <td class="product-quantity">
                              <div class="table-product-mobile">
                                Количество
                              </div>
                                <div class="product-quantity-wrapper">
                                 <div class="decr"></div>
                                  <?php
                                    if ( $_product->is_sold_individually() ) {
                                        $product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
                                    } else {
                                      $product_quantity = woocommerce_quantity_input( array(
                                          'input_name'  => "cart[{$cart_item_key}][qty]",
                                          'input_value' => $cart_item['quantity'],
                                          'max_value'   => $_product->get_max_purchase_quantity(),
                                          'min_value'   => '0',
                                      ), $_product, false );
                                    }

                                    echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );
                                  ?>
                                  <div class="inc"></div>
                                </div>
                            </td>
                            
                            <td class="product-articul mobile">
                              <div class="table-product-mobile">
                                Артикул
                              </div>
                              <?= $cart_item['data']->get_sku() ?: "&nbsp;"; ?>
                            </td>

                            <td class="product-price">
                                <div class="table-product-mobile">
                                  Цена
                                </div>
                                <div class="product-price-amount">
                                  <?= number_format($cart_item['data']->get_price() ?: 0, 0, '.', ' '); ?> 
                                  <span class="currency">руб</span>
                                </div>
                            </td>

    <!--
                            <td class="product-subtotal" data-title="<?php esc_attr_e( 'Total', 'woocommerce' ); ?>">
                                <?php /*
                                    echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); */
                                ?>
                            </td>
    -->

                            <td class="product-remove">
                                <?php
                                // hidden button for native WooCommerce functionality - removing item from cart via AJAX
                                //  it must be element matching selector '.woocommerce-cart-form .product-remove > a'
                                    echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
                                        '<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s"></a>',
                                        esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
                                        __( 'Remove this item', 'woocommerce' ),
                                        esc_attr( $product_id ),
                                        esc_attr( $_product->get_sku() )
                                    ), $cart_item_key );
                                ?>
                                <div class="product-remove-wrapper">
                                  <?php
                                    // this button is for JS clicking on button above
                                    echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
                                        '<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s"><span class="product-remove-mobile">Убрать</span></a>',
                                        esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
                                        __( 'Remove this item', 'woocommerce' ),
                                        esc_attr( $product_id ),
                                        esc_attr( $_product->get_sku() )
                                    ), $cart_item_key );
                                  ?>
                                  
                                </div>
                                
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>

                <?php do_action( 'woocommerce_cart_contents' ); ?>

    <!--
                <tr>
                    <td colspan="6" class="actions">

                        <?php /* if ( wc_coupons_enabled() ) { ?>
                            <div class="coupon">
                                <label for="coupon_code"><?php _e( 'Coupon:', 'woocommerce' ); ?></label> <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" /> <input type="submit" class="button" name="apply_coupon" value="<?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?>" />
                                <?php do_action( 'woocommerce_cart_coupon' ); ?>
                            </div>
                        <?php } ?>

                        <input type="submit" class="button" name="update_cart" value="<?php esc_attr_e( 'Update cart', 'woocommerce' ); ?>" />

                        <?php do_action( 'woocommerce_cart_actions' ); ?>

                        <?php wp_nonce_field( 'woocommerce-cart' ); */ ?>
                    </td>
                </tr>
    -->

                <?php do_action( 'woocommerce_after_cart_contents' ); ?>
            </tbody>
        </table>
        <?php do_action( 'woocommerce_after_cart_table' ); ?>
        <div class="cart-additional">
          <div class="total-cart-sum">
            <span class="total-cart-sum-text">Цена всего:</span>
            <span class="total-cart-sum-amount"><?= number_format(WC()->cart->total ?: 0, 0, '.', ' '); ?> </span>
            <span class="total-cart-sum-currency">руб</span>
          </div>

          <a href="<?= get_post_type_archive_link('product'); ?>" class="cart-continue">Продолжить покупки</a>
        </div>
      </form>
    </div>
  </div>
  
  <?php get_template_part('templates/checkout-form') ?>
</div>

<div class="cart-collaterals">
	<?php
		/**
		 * woocommerce_cart_collaterals hook.
		 *
		 * @hooked woocommerce_cross_sell_display
		 * @hooked woocommerce_cart_totals - 10
		 
	 	do_action( 'woocommerce_cart_collaterals' );*/
	?>
</div>

<?php do_action( 'woocommerce_after_cart' ); ?>

