<?php
get_header();
$parent = get_post('20');
?>
<section id="years">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                    <li><a href="<?= get_permalink($parent->ID) ?>">7 лет</a></li>
                    <li><a href="<?= get_permalink(29) ?>">Блог</a></li>
                    <li class="active">Название новости</li>
                </ol>

                <?php
                if (!empty($parent)):
                $args = array(
                'post_parent' => $parent->ID,
                'post_type' => 'page',
                'order' => 'ASC',
                'numberposts' => -1,
                'post_status' => 'publish'
                );
                $children = get_children($args);
                endif;
                ?>

                <div class="nav-panel clearfix">
                    <?php if ($parent && !empty($parent)): ?>
                    <a class="nav-panel-block" href="<?= get_permalink($parent->ID) ?>"><?= $parent->post_title ?></a>
                    <?php endif; ?>
                    <?php
                    if ($parent && $children):
                    foreach ($children as $child):// var_dump($child->ID);
                    echo '<a class="nav-panel-block'.($child->ID == $post->ID ? " active" : "").'"'
                    .' href="' . get_permalink($child->ID) . '">' . $child->post_title . '</a>';
                    ?>
                    <?php endforeach; ?>
                    <?php endif; ?>
                </div>

                <?php  while ( have_posts() ): the_post();    ?>
                    <div class="single-post col-md-10 col-md-offset-1">
                        <h2 class="single-post-header">
                            <?php echo the_title(); ?>
                        </h2>

                        <div class="single-post-date">
                            <?php the_date('d/m/Y') ?>
                        </div>

                        <div class="single-post-row">
                            <?php echo the_content(); ?>
                        </div>

                        <div class="single-post-img">
                            <?php the_post_thumbnail(); ?>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer() ?>
