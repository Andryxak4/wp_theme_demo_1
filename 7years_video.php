<?php
/*
 * Template name: 7 Лет Видео
 */
get_header();
$fields = get_fields();
$parent = get_post($post->post_parent);
?>

<!--product section start-->
<section id="years">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                    <li><a href="<?= get_permalink($parent->ID) ?>">7 лет</a></li>
                    <li class="active">Видео</li>
                </ol>
                <h2  class="section-header">
                    <?= $fields['page_subtile'] ?>
                </h2>
                <?php
                if (!empty($parent)):
                    $args = array(
                        'post_parent' => $parent->ID,
                        'post_type' => 'page',
                        'order' => 'ASC',
                        'numberposts' => -1,
                        'post_status' => 'publish'
                    );
                    $children = get_children($args);
                endif;
                ?>
                <div class="nav-panel clearfix">
                    <?php if ($parent && !empty($parent)): ?>
                        <a class="nav-panel-block" href="<?= get_permalink($parent->ID) ?>"><?= $parent->post_title ?></a>
                    <?php endif; ?>
                    <?php
                    if ($parent && $children):
                        foreach ($children as $child):
                            echo '<a class="nav-panel-block'.($child->ID == $post->ID ? " active" : "").'"'
                                .' href="' . get_permalink($child->ID) . '">' . $child->post_title . '</a>';
                            ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>

            <div class="blog-video col-md-12">
                <div class="video-container">
                    <?php if ($fields['main_video']): ?>
                        <div class="video-main years-main-video">
                            <div class="video-main-inner">
                                <a href="https://www.youtube.com/watch?v=<?= $fields['main_video'] ?>">
                                    <img src="https://img.youtube.com/vi/<?= $fields['main_video'] ?>/hqdefault.jpg" alt="">
                                </a>
                            </div>
                            <?php if ($fields['main_video_title']): ?><p class="video-main-header"><?= $fields['main_video_title'] ?></p><?php endif; ?>
                        </div>
                    <?php endif; ?>
                    <?php if ($fields['list_of_videos']): ?>
                        <!--video blocks start-->
                        <div class="video-blocks clearfix">
                            <?php foreach ($fields['list_of_videos'] as $video): ?>
                                <?php if ($video['video']): ?>
                                    <div class="video-blocks-block">
                                        <a href="https://www.youtube.com/watch?v=<?= $video['video'] ?>">
                                            <img src="https://img.youtube.com/vi/<?= $video['video'] ?>/hqdefault.jpg" alt="">
                                        </a>
                                        <?php if ($video['title']): ?>
                                            <div class="video-header">
                                                <?= $video['title'] ?>
                                            </div>
                                        <?php endif; ?>
                                        <?php if ($video['description']): ?>
                                            <div class="video-info">
                                                <?= $video['description'] ?>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                        <!--video blocks end-->
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!--product section end-->

<?php get_footer() ?>