<div class="product-gallery product-page-content-tab col-md-12 clearfix" id="product-tab-gallery">
    <div class="grill-main-block-inner">
        <div class="grill-main-block-slider clearfix">
            <?php foreach ($photos as $i=>$photo): ?>
                <a href="<?= $photo['image'] ?>" title="<?= (!empty($photo['name'])?$photo['name']:'') ?>" class="slide"><img src="<?= $photo['image'] ?>" alt="<?= $photo['name'] ?>"></a>
            <?php endforeach; ?>
        </div>
    </div>
</div>