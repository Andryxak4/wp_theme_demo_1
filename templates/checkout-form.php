<div class="checkout-wrapper clearfix">
  <form action="#" class="cart-checkout-form">
    <img src="<?= get_template_directory_uri() ?>/img/preloader.gif" class="popup-preloader" alt="">
    <div class="form-header">ОФОРМИТЬ ЗАКАЗ</div>
    <div class="form-wrapper">
      <div class="cart-checkout-form-fields">
        <div class="field-row">
          <input type="text" name="name" placeholder="ФИО/Компания" required>
          <input type="text" name="phone" placeholder="Телефон" required>
          <input type="email" name="email" placeholder="E-mail" required>
          <input type="text" name="city" placeholder="Город" required>
        </div>
        
        <div class="field-row">
            <?php
            $payment_methods = wc()->payment_gateways->get_available_payment_gateways();
            $delivery_zones = WC_Shipping_Zones::get_zones();
            $active_zone = $delivery_zones ? array_values($delivery_zones)[0] : null;
            ?>
          <input type="hidden" name="payment_method" value="" />
          <div class="checkout-dropdown dropdown-payment">
            <div class="checkout-dropdown-header active">Выберите способ оплаты</div>
            <div class="checkout-dropdown-wrapper" data-input="payment_method">
              <?php foreach ($payment_methods as $key=>$method): ?>
              <div class="checkout-dropdown-row" data-value="<?= $key ?>"><?= $method->title ?></div>
              <?php endforeach; ?>
            </div>
          </div>
          
          <input type="hidden" name="shipping_method" value="" />
          <?php if ($active_zone && !empty($active_zone['shipping_methods'])): ?>
          <div class="checkout-dropdown dropdown-delivery">
            <div class="checkout-dropdown-header active">Выберите способ доставки</div>
            <div class="checkout-dropdown-wrapper" data-input="shipping_method">
              <?php foreach ($active_zone['shipping_methods'] as $method): ?>
                <?php if ($method->enabled == 'yes'): ?>
              <div class="checkout-dropdown-row" data-code="<?= $method->id ?>" data-value="<?= $method->title ?>"><?= $method->title ?></div>
                <?php endif; ?>
              <?php endforeach; ?>
            </div>
          </div>
          <?php endif; ?>
        </div>
        
        <div class="field-row">
          <textarea name="comment" placeholder="Комментарий"></textarea>
        </div>
      </div>
      
      <div class="cart-agreement">
        <div class="cart-agreement-checkbox clearfix">
          <input type="checkbox" id="agreement-cart-accept" name="accept_terms" value="1" checked>
          <label for="agreement-cart-accept"></label>
        </div>
        <div class="cart-agreement-text">
          С <a href="<?= get_field('privacy_policy_page', 'options'); ?>" class="link" target="_blank">политикой конфиденциальности</a> ознакомлен
        </div>
      </div>
      <button class="checkout-submit" type="submit">Отправить заказ</button>
    </div>
  </form>
</div>  