<?php
$all_categories = get_terms( array(
    'taxonomy' => 'product_categories',
    'hide_empty' => false,
    'orderby' => 'id',
) );
?>

<div class="nav-panel clearfix">
  <div class="nav-panel-mobile-header" id="nav-mobile-header">каталог</div>
  <div class="nav-panel-toggle clearfix">
    <a href="/product/" class="nav-panel-block <?= empty($current_category_id) ? "active" : "" ?>">Все</a>
    <?php foreach ($all_categories as $category): ?>
    <a href="/product-categories/<?= $category->slug ?>" class="nav-panel-block <?= $category->term_id==$current_category_id ? "active" : "" ?>">
        <?= $category->name ?> <span class="cat-amount"><?= $category->count ? "({$category->count})" : "" ?></span>
    </a>
    <?php endforeach; ?>
  </div>
</div>
