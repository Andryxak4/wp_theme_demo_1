<!--product video start-->
<div class="video col-md-12 product-page-content-tab clearfix">
    <?php
        $new_videos = array();
        $i = 0;
        foreach($videos as $video => $arr_video) {
            if(!empty($arr_video) && is_array($arr_video)) {
                $new_videos[$i]['name'] = $arr_video['name'];
                $new_videos[$i]['description'] = $arr_video['description'];
                $new_videos[$i]['url'] = $arr_video['url'];
                $i++;
            }
        }
    ?>
    <div class="video-container">
        <?php // DISABLED ?>
        <?php if( 0 && (count($new_videos) == 1)): ?>
          <div class="video-main years-main-video">
            <div class="video-main-inner">
                <a href="http://www.youtube.com/watch?v=<?= $new_videos[0]['url'] ?>" class="slide popup-youtube">
                    <img src="http://img.youtube.com/vi/<?= $new_videos[0]['url'] ?>/hqdefault.jpg" alt="">
                </a>
            </div>
            <p class="video-main-header"><?= $new_videos[0]['name']; ?></p>
          </div>
        <?php else: ?>
          <!--video blocks start-->
          <div class="video-blocks clearfix">
                <?php $block_class = empty($is_mobile_tab) ? "video-blocks-block" : "video-blocks-mobile"; ?>
                <?php foreach ($new_videos as $i=>$video): ?>
                    <div class="<?= $block_class; ?>">
                        <a href="http://www.youtube.com/watch?v=<?= $video['url'] ?>" class="slide popup-youtube">
                            <img src="http://img.youtube.com/vi/<?= $video['url'] ?>/hqdefault.jpg" alt="">
                        </a>
                        <div class="video-header"><?= $video['name']; ?></div>
                        <div class="video-info"><?= $video['description']; ?></div>
                    </div>
                <?php endforeach; ?>
          </div>
        <!--video blocks end-->
        <?php endif; ?>
    </div>

</div>
<!--product video end-->