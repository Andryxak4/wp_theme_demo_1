<?php
$warranty_page_id = get_field('warranty_page', 'options');
$fields = get_fields($warranty_page_id);
?>
<!--product guarantee start-->
<div class="guarantee <?= empty($is_mobile_tab) ? "product-page-content-tab" : "" ?> col-md-12">
    <?php foreach (arrayItem($fields, 'top_lists', array()) as $list): ?>
        <?php if (!empty($list['title'])): ?>
            <div class="guarantee-header">
                <?= $list['title'] ?>
            </div>
        <?php endif; ?>

        <?php foreach (arrayItem($list, 'text_lists', array()) as $text): ?>
            <?php if (!empty($text['text'])): ?> 
                <div class="guarantee-row">
                    <?= $text['text'] ?>  
                </div>
            <?php endif; ?>
        <?php endforeach; ?>

    <?php endforeach; ?>

    <?php if ($list_of_rules = arrayItem($fields, 'list_of_rules')): ?>
        <!--guarantee list start-->
        <div class="guarantee-list">
            <?php foreach ($list_of_rules as $rule): ?>
                <div class="guarantee-list-row clearfix">
                  <?php if (!empty($rule['number'])): ?>
                    <div class="guarantee-list-row-number"><?= $rule['number'] ?>.</div>
                  <?php endif; ?>
                  <?php if (!empty($rule['text'])): ?>
                    <div class="guarantee-list-row-text"><?= $rule['text'] ?></div>
                  <?php endif; ?>
                </div>
            <?php endforeach; ?>
        </div>
        <!--guarantee list end-->

    <?php endif; ?>
    <?php if (!empty($fields['text_on_bottom'])): ?>
        <div class="guarantee-row <?= empty($is_mobile_tab) ? "" : "bold"; ?>">
            <?= $fields['text_on_bottom'] ?>
        </div>
    <?php endif; ?>

</div>
<!--product guarantee end-->