<!--product scheme start-->
<div class="product-scheme product-page-content-tab">
  <?php foreach ($scheme as $item): ?>
  <div class="product-scheme-block row">
    <div class="product-scheme-img col-lg-5 col-md-5 col-sm-6">
      <img src="<?= arrayItem($item, 'image'); ?>" alt="">
    </div>

    <div class="product-scheme-info col-lg-6 col-lg-offset-1 col-md-6 col-md-offset-1 col-sm-6 col-sm-offset-0">
      <div class="product-scheme-info-text">
        <?= arrayItem($item, 'description'); ?>
      </div>
        
      <div class="product-scheme-info-text">
        <?= arrayItem($item, 'description_2'); ?>
      </div>

      <div class="product-scheme-info-btns clearfix">
        <?php if ($pdf_link=arrayItem($item, 'download_link_pdf')): ?>
        <a href="<?= $pdf_link; ?>" class="btn down-pdf" download>
          <span class="icon-download"></span>
          скачать схему в pdf
        </a>
        <?php endif; ?>

        <?php if ($instruction_link=arrayItem($item, 'download_link_instruction')): ?>
        <a href="<?= $instruction_link; ?>" class="btn" download>
          скачать инструкцию
        </a>
        <?php endif; ?>
      </div>
    </div>
  </div>
  <?php endforeach; ?>
</div>
<!--product scheme end-->