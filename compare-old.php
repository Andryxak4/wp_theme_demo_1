<?php
/*
 * Template name: Страница сравнения
 */

get_header();

$lappigrill_compare_ids = arrayItem($_COOKIE, 'lappigrill_compare_ids', '');
$compare_ids = $lappigrill_compare_ids ? explode(',', $lappigrill_compare_ids) : array();

$compared_products_args = array(
    'post_type' => 'product',
    'post__in' => $compare_ids,
);
$compared_products = get_posts($compared_products_args);

?>

<!--dilers section start-->
<section id="compare" class="compare">
  <div class="container">
    <div class="row">
      <div class="col-md-11">
        <ol class="breadcrumb">
          <li><a href="/">Главная</a></li>
          <li class="active">Сравнение товаров</li>
        </ol>
        
        <h2  class="section-header">
          сравнение товаров
        </h2>
          
        <?php if ($compared_products): ?>
        
        <div class="compare-block cd-products-comparison-table">
          
          <div class="cd-products-table">
            <div class="features">
                <ul class="cd-features-headers">
                    <li class="ch-header" data-header="h1">Заголовок характеристик</li>
                    <li class="ch-header" data-header="h2">Заголовок характеристик</li>
                </ul>
                


                <ul class="cd-features-list-subheaders">
                    <li class="ch-subheader" data-subheader="h1">
                      <div class="ch-subheader-content">
                        <img src="<?= get_template_directory_uri(); ?>/img/grill-sickness.png" alt="">
                        Размер очага
                      </div>
                    </li>
                    
                    <li class="ch-subheader" data-subheader="h2">
                      <div class="ch-subheader-content">
                        <img src="<?= get_template_directory_uri(); ?>/img/grill-size.png" alt="">
                        Толщина/материал очага
                      </div>
                    </li>
                    
                    <li class="ch-subheader" data-subheader="h3">
                      <div class="ch-subheader-content">
                        <img src="<?= get_template_directory_uri(); ?>/img/grill-adjustment.png" alt="">
                        Регулировка по высоте:
                      </div>
                    </li>
                    
                    <li class="ch-subheader" data-subheader="h4">
                      <img src="<?= get_template_directory_uri(); ?>/img/grill-sickness.png" alt="">
                      Вид топлива:
                    </li>
                    
                    <li class="ch-subheader" data-subheader="h5">
                      <img src="<?= get_template_directory_uri(); ?>/img/grill-size.png" alt="">
                      Гарантийный срок:
                    </li>
                </ul>
            </div> <!-- .features -->
            
            <div class="cd-products-wrapper">
                <ul class="cd-products-columns">
                    <?php foreach ($compared_products as $product): ?>
                    <?php $fields = get_fields($product->ID, false); ?>
                    <li class="product">
                        <div class="top-info">
                            <img src="<?= get_the_post_thumbnail_url($product->ID) ?>" alt="product image">
                            <a href="<?= get_permalink($product->ID) ?>"><?= $product->post_title; ?></a>
                        </div> <!-- .top-info -->

                        <ul class="cd-features-list" data-header="h1">
                            <li  data-subheader="h1"><?= lappi_hearth_size(arrayItem($fields, 'hearth_size')) ?: '&nbsp;'; ?></li>
                            <li  data-subheader="h2"><?= arrayItem($fields, 'hearth_thickness') ?: '&nbsp;'; ?></li>
                            <li  data-subheader="h3"><?= arrayItem($fields, 'hearth_zone_control', 'Нет'); ?></li>

                        </ul>
                        
                        <ul class="cd-features-list" data-header="h2">
                            <li  data-subheader="h4"><?= arrayItem($fields, 'fuel_type') ?: '&nbsp;'; ?></li>
                            <li  data-subheader="h5"><?= arrayItem($fields, 'warranty_period') ?: '&nbsp;'; ?></li>
                        </ul>            
                    </li> <!-- .product -->
                    <?php endforeach; ?>
                </ul> <!-- .cd-products-columns -->
            </div> <!-- .cd-products-wrapper -->
            
            <ul class="cd-table-navigation">
                <li><a href="#0" class="prev inactive">Prev</a></li>
                <li><a href="#0" class="next">Next</a></li>
            </ul>
        </div> <!-- .cd-products-table -->  
          
          
          
          
        </div>
          
        <?php else: ?>
          <div class="compare-no-products">
              Не выбраны продукты для сравнения!
          </div>
        <?php endif; ?>
      </div>

      
    </div>
  </div>
</section>
<!--dilers section end-->
<?php
get_footer();
?>