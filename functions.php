<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

include_once 'includes/functions.php';
include_once 'includes/menu_walker.php';
require_once 'includes/post_types_and_taxonomies.php';
require_once 'includes/scripts.php';
require_once 'includes/ajax.php';
require_once 'includes/init.php';
require_once 'includes/filters.php';

add_theme_support('post-thumbnails');

register_nav_menus(array(
    'primary' => __('Primary menu', 'lappigrill'),
));

if (!empty($_GET['sql_remove_prices'])):
    global $wpdb;

    $sql1 = "UPDATE {$wpdb->prefix}postmeta SET meta_key='_lappi_price' WHERE meta_key='_price'";
    $sql2 = "UPDATE {$wpdb->prefix}postmeta SET meta_key='lappi_price' WHERE meta_key='price'";
    $res1 = $wpdb->query($sql1);
    $res2 = $wpdb->query($sql2);
    
//    $sql3 = "UPDATE {$wpdb->prefix}postmeta SET meta_key='_lappi_dop_price' WHERE meta_key='_dop_price'";
//    $sql4 = "UPDATE {$wpdb->prefix}postmeta SET meta_key='lappi_dop_price' WHERE meta_key='dop_price'";
//    $res3 = $wpdb->query($sql3);
//    $res4 = $wpdb->query($sql4);
    
    $sql5 = "SELECT pm.post_id, pm.meta_value FROM {$wpdb->prefix}postmeta pm "
        . "INNER JOIN {$wpdb->prefix}posts p ON p.ID=pm.post_id "
        . "WHERE p.post_type='product' "
        . "AND pm.meta_key='lappi_price'";
    $res5 = $wpdb->get_results($sql5, ARRAY_A);
    foreach ($res5 as $row):
        $val = preg_replace('/[^0-9]/', '', $row['meta_value']);
        update_post_meta($row['post_id'], '_price', $val);
        update_post_meta($row['post_id'], '_regular_price', $val);
    endforeach;
    
    die('done');
    
endif;

add_action('template_redirect', function() {
    if (isset($_GET['lappi_cart_ajax_add_product'])):
        header("HTTP/1.1 200 OK");
        die('1');
    endif;
});
