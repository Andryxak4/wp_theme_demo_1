<?php
/*
 * Template name: Дилеры
 */
get_header();
?>


  <!--dilers section start-->
  <section id="dilersp">
    <div class="container">
      <div class="row">
        <div class="dilersp col-md-11">
          <ol class="breadcrumb">
            <li><a href="/">Главная</a></li>
            <li class="active">Дилеры</li>
          </ol>

          <h2 class="section-header">
                    <?php the_title() ?>
                </h2>
          <div class="dilersp-text">
            <?php echo the_content() ?>
          </div>
        </div>

        <div class="col-md-12">
          <div class="dilersp-panel clearfix">
            <div class="dilersp-panel-search">
              <input class="input-search" name="searchInput" type="text" placeholder="Введите название города или региона">
            </div>
            
            <div class="mobile-filter">
              <ul class="mobile-filter-menu">
                <div class="mobile-filter-menu-wrapper">
                  <li class="mobile-item id-default current" data-cs="">Все регионы</li>
                  <?php
                        $args = array( 'taxonomy' => 'dealers_region');
                        $terms = get_terms($args);
                        for ($i=0; $i < count($terms); $i++) {
                        ?>
                            <li class="mobile-item <?= "id-" . $terms[$i]->slug ?>" data-cs="<?= $terms[$i]->slug ?>">
                              <?= $terms[$i]->name ?>
                            </li>
                        <?php
                        }
                    ?>
                  </div>
                </ul>
            </div>

            <div class="nav-regions clearfix">
              <div class="nav-regions-block clearfix">
                <div class="nav-regions-block-model id-default" data-cs="" name="">Все регионы</div>
                <?php
                    $args = array('taxonomy' => 'dealers_region');
                    $terms = get_terms($args);
                    for ($i=0; $i < count($terms); $i++) {
                    ?>
                        <div class="nav-regions-block-model  <?= "id-" . $terms[$i]->slug ?>" data-cs="<?= $terms[$i]->slug ?>">
                        <?= $terms[$i]->name ?>
                        </div>
                    <?php
                    }
                ?>
              </div>
            </div>
          </div>
          <!-- Mobile nav -->
          
        <?php
        $args = array(
            'taxonomy' => 'dealers_region'
        );
        $categories = get_categories($args);
        foreach($categories as $category):
        ?>
          <div class="dilersp-region <?= $category->slug ?>">
            <textarea class="hide-regions-taxonomy" style="display: none;">
                <?php
                    $taxonomies = get_fields('dealers_region_'.$category->term_id);
                    echo $taxonomies['regions_taxonomy'];
                ?>
            </textarea>
            <div class="dilersp-region-header" data-name="<?= $category->name ?>"><?= $category->name ?></div>
                <?php
                $posts = get_posts(
                    array(
                        'numberposts' => -1,
                        'post_type' => 'dealers',
                        'order' => 'ASC',
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'dealers_region',
                                'field' => 'slug',
                                'terms' => array($category->slug),
                            ),
                        ),
                    )
                );
                if (isset($posts) && !empty($posts)) {
                    foreach($posts as $post):
                        $postData = get_fields();
                        $postTerms = wp_get_post_terms(get_the_ID(), "dealers_region");
                ?>
                        <!-- ellements of search -->
                        <div class="dilersp-region-row <?= $postTerms[0]->slug ?> clearfix">
                            <div class="dilersp-region-row-header">
                                <?php echo the_title(); ?>
                            </div>
                            <div class="dilersp-region-row-info clearfix">
                                <?php foreach($postData['contacts'] as $contact): ?>
                                <div class="dilersp-region-row-info-block">
                                    <div class="dilersp-region-row-info-block-address">
                                        <?= arrayItem($contact, 'address'); ?>
                                    </div>
                                    <?php if(isset($contact['phone_1']) && !empty($contact['phone_1'])) { ?>
                                        <div class="dilersp-region-row-info-block-phone">
                                            <?php echo $contact['phone_1']; ?>
                                        </div>
                                    <?php } ?>
                                    <?php if(isset($contact['phone_2']) && !empty($contact['phone_2'])) { ?>
                                        <div class="dilersp-region-row-info-block-phone">
                                        <?php echo $contact['phone_2']; ?>
                                        </div>
                                    <?php } ?>
                                    <?php if(isset($contact['phone_3']) && !empty($contact['phone_3'])) { ?>
                                        <div class="dilersp-region-row-info-block-phone">
                                            <?php echo $contact['phone_3']; ?>
                                        </div>
                                    <?php } ?>
                                </div>
                                <?php endforeach; ?>
                            </div>
                            <?php if(isset($postData['site_url']) && !empty($postData['site_url'])): ?>
                                <div class="dilersp-region-row-site">
                                    <a href="http://<?= $postData['site_url'] ?>" class="dilersp-region-row-site-link" target="_blank">
                                        <?php echo $postData['site_url']; ?>
                                    </a>
                                    <?php if(isset($postData['site_url_2']) && !empty($postData['site_url_2'])): ?>
                                        <a href="http://<?= $postData['site_url_2'] ?>" class="dilersp-region-row-site-link" target="_blank">
                                            <?php echo $postData['site_url_2']; ?>
                                        </a>
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                <?php
                    endforeach;
                }
                ?>
            </div>
        <?php endforeach; ?>
        </div>
      </div>
    </div>
  </section>
  <!--dilers section end-->

  <?php get_footer(); ?>
