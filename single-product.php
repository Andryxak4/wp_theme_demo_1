<?php
get_header();
$fields = get_fields();
$photos = arrayItem($fields, 'photo', array());
$videos = arrayItem($fields, 'video', array());
$scheme = arrayItem($fields, 'scheme', array());
$basic_set = arrayItem($fields, 'basic_set', array());
$additional_items = arrayItem($fields, 'additional_items');
$images = arrayItem($fields, 'images');
$is_additional_equipment = !empty($fields['choose_product']);
$js_vars = array('mobile' => array());

$product_categories = wp_get_object_terms(get_the_ID(), 'product_categories');
$product_category = arrayItem($product_categories, 0);
$current_category_id = arrayItem($product_category, 'term_id');
if (!$is_additional_equipment):
    $same_category_posts_args = array(
        'post_type' => 'product',
//        'post__not_in' => array(get_the_ID()),
        'tax_query' => array(
            array(
                'taxonomy' => 'product_categories',
                'terms' => $current_category_id,
            ),
        ),
    );
    $same_category_posts = get_posts($same_category_posts_args);
endif;
?>
    <!--product section start-->
    <section id="product">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <?= dimox_breadcrumbs(); ?>

                    <?php include __DIR__ . DIRECTORY_SEPARATOR . 'templates/product-categories-menu.php' ?>
                </div>

                <div class="product-nav col-lg-12 col-md-12 col-sm-12 col-xs-12 clearfix">
                    <div class="row">
                        <?php if (!$is_additional_equipment): ?>
                        <div class="nav-models mobile clearfix">
                            <div class="nav-models-block clearfix">
<?php /*                                <div class="nav-models-block-model active"><?php the_title(); ?></div> */ ?>

                                <?php foreach ($same_category_posts as $p): ?>
                                  <?php if ($p->ID==get_the_ID()): ?>
                                    <div class="nav-models-block-model active">
                                        <?= $p->post_title ?>
                                    </div>
                                  <?php else: ?>
                                    <a class="nav-models-block-model" href="<?= get_permalink($p); ?>">
                                        <?= $p->post_title ?>
                                    </a>
                                  <?php endif; ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <?php endif; ?>

                        <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 product-nav-img">
                            <h2 class="section-header">
                                <span class="section-header-product-name"><?php the_title(); ?></span>
                                <div class="section-header-descr"><?= $product_categories[0]->name ?></div>
                            </h2>
                        </div>
                        <?php if (!$is_additional_equipment): ?>
                            <div class="col-lg-6 col-lg-offset-1 col-md-6 col-md-offset-1 col-sm-6 col-sm-offset-0 col-xs-12 col-xs-offset-0  product-nav-about">
                                <div class="panels-filter">
                                    <div class="nav-models clearfix">
                                        <div class="nav-models-header clearfix">Модель:</div>
                                        <div class="nav-models-block clearfix">
<?php /*                                            <div class="nav-models-block-model active"><?php the_title(); ?></div> */ ?>
                                            <?php foreach ($same_category_posts as $p): ?>
                                              <?php if ($p->ID==get_the_ID()): ?>
                                                <div class="nav-models-block-model active">
                                                    <?= $p->post_title ?>
                                                </div>
                                              <?php else: ?>
                                                <a class="nav-models-block-model" href="<?= get_permalink($p); ?>">
                                                    <?= $p->post_title ?>
                                                </a>
                                              <?php endif; ?>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>

                                    <div class="sub-panel clearfix">
                                        <div class="sub-panel-block active" data-tab="grill-main-block" data-hash="complect">гриль</div>
                                        <div class="sub-panel-block" data-tab="product-gallery" data-hash="photos">галерея</div>
                                        <div class="sub-panel-block" data-tab="video" data-hash="video">видео</div>
                                        <div class="sub-panel-block" data-tab="product-scheme" data-hash="plans">схема</div>
                                        <div class="sub-panel-block" data-tab="guarantee" data-hash="instruction">гарантия</div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <?php if (!$is_additional_equipment): ?>
                    <?php while (have_posts()): the_post(); ?>
                        <?php $wc_product = new WC_Product(get_the_ID()); ?>
                        <div class="grill-main-block product-page-content-tab col-lg-12 col-md-12 col-sm-12 col-xs-12 clearfix">
                            <div class="grill-main-block-img col-lg-5 col-md-5 col-sm-6">
                                <div class="grill-main-block-img-block">
                                    <div class="grill-main-block-img-block-inner">
                                        <a href="<?= get_the_post_thumbnail_url() ?>" title="<?= the_title() ?>">
                                            <img src="<?= get_the_post_thumbnail_url() ?>" alt="<?= the_title() ?>">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="grill-main-block-description col-lg-6 col-lg-offset-1 col-md-6 col-md-offset-1 col-sm-6 col-sm-offset-0">
                                <h3 class="recommend-price">
                                    Рекомендованная розничная цена:
                                    <div class="hint">
                                        <div class="hint-icon">?</div>
                                        <div class="hint-text">
                                            <?= arrayItem($fields, 'price_hint'); ?>
                                            <div class="hint-close"></div>
                                        </div>
                                    </div>
                                </h3>

                                <div class="grill-main-block-description-price">
                                    <?= number_format($wc_product->get_price() ?: 0, 0, '.', ' '); ?> руб
                                </div>

                                <ul class="grill-main-block-description-list">
                                    <li class="clearfix">
                                        <div class="grill-main-block-description-list-param">
                                            Размер очага:
                                        </div>

                                        <div class="grill-main-block-description-list-right clearfix">
                                            <div class="grill-main-block-description-list-value">
                                                <!--<img src="<?= get_stylesheet_directory_uri() ?>/img/icon-rad.png" alt="">-->
                                                <?= lappi_hearth_size(arrayItem($fields, 'hearth_size')); ?>
                                            </div>
                                            <div class="hint">
                                                <div class="hint-icon">?</div>
                                                <div class="hint-text">
                                                    <?= arrayItem($fields, 'hearth_size_hint'); ?>
                                                    <div class="hint-close"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                    <li class="clearfix">
                                        <div class="grill-main-block-description-list-param">
                                            Толщина/материал очага:
                                        </div>

                                        <div class="grill-main-block-description-list-right clearfix">
                                            <div class="grill-main-block-description-list-value">
                                                <?= arrayItem($fields, 'hearth_thickness'); ?>
                                            </div>

                                            <div class="hint">
                                                <div class="hint-icon">?</div>
                                                <div class="hint-text">
                                                    <?= arrayItem($fields, 'hearth_thickness_hint'); ?>
                                                    <div class="hint-close"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                    <li class="clearfix">
                                        <div class="grill-main-block-description-list-param">
                                            Регулировка по высоте:
                                        </div>

                                        <div class="grill-main-block-description-list-right clearfix">
                                            <div class="grill-main-block-description-list-value">
                                                <?= ( ! empty( $fields['hearth_zone_control'] ) ? $fields['hearth_zone_control'] : 'Нет' )  ?>
                                            </div>

                                            <div class="hint">
                                                <div class="hint-icon">?</div>
                                                <div class="hint-text">
                                                    <?= arrayItem($fields, 'hearth_zone_control_hint'); ?>
                                                    <div class="hint-close"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                    <li class="clearfix">
                                        <div class="grill-main-block-description-list-param">
                                            Гарантийный срок:
                                        </div>

                                        <div class="grill-main-block-description-list-right clearfix">
                                            <div class="grill-main-block-description-list-value">
                                                <?= arrayItem($fields, 'warranty_period'); ?>
                                            </div>

                                            <div class="hint">
                                                <div class="hint-icon">?</div>
                                                <div class="hint-text">
                                                    <?= arrayItem($fields, 'warranty_period_hint'); ?>
                                                    <div class="hint-close"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                    <li class="clearfix">
                                        <div class="grill-main-block-description-list-param">
                                            Вид топлива:
                                        </div>

                                        <div class="grill-main-block-description-list-right clearfix">
                                            <div class="grill-main-block-description-list-value">
                                                <?= arrayItem($fields, 'fuel_type'); ?>
                                            </div>

                                            <div class="hint">
                                                <div class="hint-icon">?</div>
                                                <div class="hint-text">
                                                    <?= arrayItem($fields, 'fuel_type_hint'); ?>
                                                    <div class="hint-close"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <?php if( ! empty( $fields['thermo_case'] ) ): ?>
                                        <li class="clearfix">
                                            <div class="grill-main-block-description-list-param">
                                                Термоизолированный внешний корпус:
                                            </div>

                                            <div class="grill-main-block-description-list-right clearfix">
                                                <div class="grill-main-block-description-list-value">
                                                    <?= arrayItem($fields, 'thermo_case'); ?>
                                                </div>

                                                <div class="hint">
                                                    <div class="hint-icon">?</div>
                                                    <div class="hint-text">
                                                        <?= arrayItem($fields, 'thermo_case_hint'); ?>
                                                        <div class="hint-close"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    <?php endif; ?>
                                    <?php if( ! empty( $fields['case_material'] ) ): ?>
                                        <li class="clearfix">
                                            <div class="grill-main-block-description-list-param">
                                                Материал внешнего корпуса:
                                            </div>

                                            <div class="grill-main-block-description-list-right clearfix">
                                                <div class="grill-main-block-description-list-value">
                                                    <?= arrayItem($fields, 'case_material'); ?>
                                                </div>
                                                <div class="hint">
                                                    <div class="hint-icon">?</div>
                                                    <div class="hint-text">
                                                        <?= arrayItem($fields, 'case_material_hint'); ?>
                                                        <div class="hint-close"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    <?php endif; ?>
                                </ul>

                                <div class="grill-main-block-description-text">
                                    <?php the_content(); ?>
                                </div>

                                <div class="grill-main-block-description-btns">
                                    <?php //DISABLED ?>
                                    <?php if (0): ?>
                                    <a href="<?= get_permalink(45) ?>" class="wtb-btn btn">
                                        <img src="<?= get_stylesheet_directory_uri() ?>/img/where-to-buy.png" alt="" class="wtb-btn-icon"> 
                                        <span class="wtb-btn-text">
                                          Где купить?
                                        </span>
                                    </a>
                                    <?php endif; ?>

                                    <?php
                                    $lappigrill_compare_ids = arrayItem($_COOKIE, 'lappigrill_compare_ids', '');
                                    $compare_ids = $lappigrill_compare_ids ? explode(',', $lappigrill_compare_ids) : array();
                                    $in_compare_array = in_array(get_the_ID(), $compare_ids);
                                    ?>
                                    <a href="javascript:void(0);"
                                       class="compare-btn btn <?= $in_compare_array ? "in-compare" : ""; ?>">
                                        <span class="compare-btn-amount"><?= count($compare_ids); ?></span>
                                        <span class="compare-btn-icon"></span>
                                        <span class="compare-btn-text"><?= $in_compare_array ? "В сравнении" : "Сравнение"; ?></span>
                                    </a>
                                </div>
                                
                                <div class="btns-buy">
                                  <a href="" class="buy-btn oneclick"
                                        data-product-id="<?php the_ID(); ?>"
                                        data-product-name="<?= esc_html(get_the_title()); ?>">
                                    <span class="buy-btn-icon oneclick">
                                      <img src="<?= get_stylesheet_directory_uri() ?>/img/buy-oneclick.png" alt="">
                                      <img src="<?= get_stylesheet_directory_uri() ?>/img/buy-oneclick-hover.png" class="hover" alt="">
                                    </span>
                                    Купить в 1 клик
                                  </a>
                                  <a href="" class="buy-btn btn-to-cart"
                                        data-product-id="<?php the_ID(); ?>"
                                        data-product-name="<?= esc_html(get_the_title()); ?>"
                                        data-product-sku="<?= $wc_product->get_sku(); ?>"
                                        data-product-price="<?= number_format($wc_product->get_price() ?: 0, 0, '.', ' '); ?>"
                                        data-product-image="<?= get_the_post_thumbnail_url(get_the_ID()); ?>">
                                    <span class="buy-btn-icon oneclick">
                                      <img src="<?= get_stylesheet_directory_uri() ?>/img/to-cart.png" alt="">
                                      <img src="<?= get_stylesheet_directory_uri() ?>/img/to-cart-hover.png" class="hover" alt="">
                                    </span>                                 
                                    В корзину
                                  </a>
                                </div>
                            </div>

                            <?php $is_mobile_tab = true; ?>
                            <div class="panel-group grill-accordeon" id="grill-accordion">
                                <!--panel photo start-->
                                <div class="panel panel-default">
                                    <div class="grill-accordeon-header panel-heading accordion-toggle collapsed"
                                         data-toggle="collapse" data-parent="#grill-accordion"
                                         data-target="#product-photo" id="product-photo-panel">
                                        Фото
                                        <div class="panel-status">
                                            <div class="line horizontal"></div>
                                            <div class="line vertical"></div>
                                        </div>
                                    </div>
                                    <div id="product-photo" class="panel-collapse collapse">
                                        <?php ob_start(); ?>
                                        <?php include 'templates/product-photo.php' ?>
                                        <?php $js_vars['mobile']['product-photo'] = ob_get_clean(); ?>
                                    </div>
                                </div>
                                <!--panel photo end-->

                                <!--panel video start-->
                                <div class="panel panel-default">
                                    <div class="grill-accordeon-header panel-heading accordion-toggle collapsed"
                                         data-toggle="collapse" data-parent="#grill-accordion"
                                         data-target="#product-video" id="product-video-panel">
                                        Видео
                                        <div class="panel-status">
                                            <div class="line horizontal"></div>
                                            <div class="line vertical"></div>
                                        </div>
                                    </div>
                                    <div id="product-video" class="panel-collapse collapse">
                                        <?php ob_start(); ?>
                                        <?php include 'templates/product-video.php' ?>
                                        <?php $js_vars['mobile']['product-video'] = ob_get_clean(); ?>
                                    </div>
                                </div>
                                <!--panel video end-->

                                <!--panel scheme start-->
                                <div class="panel panel-default">
                                    <div class="grill-accordeon-header panel-heading accordion-toggle collapsed"
                                         data-toggle="collapse" data-parent="#grill-accordion"
                                         data-target="#product-scheme" id="product-scheme-panel">
                                        Схема
                                        <div class="panel-status">
                                            <div class="line horizontal"></div>
                                            <div class="line vertical"></div>
                                        </div>
                                    </div>

                                    <div id="product-scheme" class="panel-collapse collapse">
                                        <?php ob_start(); ?>
                                        <?php include 'templates/product-scheme.php' ?>
                                        <?php $js_vars['mobile']['product-scheme'] = ob_get_clean(); ?>
                                    </div>
                                </div>
                                <!--panel scheme end-->

                                <!--panel warranty start-->
                                <div class="panel panel-default">
                                    <div class="grill-accordeon-header panel-heading accordion-toggle collapsed"
                                         data-toggle="collapse" data-parent="#grill-accordion"
                                         data-target="#product-warranty" id="product-warranty-panel">
                                        гарантия
                                        <div class="panel-status">
                                            <div class="line horizontal"></div>
                                            <div class="line vertical"></div>
                                        </div>
                                    </div>

                                    <div id="product-warranty" class="panel-collapse collapse">
                                        <?php include 'templates/warranty.php'; ?>
                                    </div>
                                </div>
                                <!--panel warranty end-->

                                <!--panel equipment start-->
                                <div class="panel panel-default">
                                    <div class="grill-accordeon-header panel-heading accordion-toggle collapsed"
                                         data-toggle="collapse" data-parent="#grill-accordion"
                                         data-target="#product-equipment" id="product-equipment-panel">
                                        комплектация
                                        <div class="panel-status">
                                            <div class="line horizontal"></div>
                                            <div class="line vertical"></div>
                                        </div>
                                    </div>
                                    <div id="product-equipment" class="panel-collapse collapse">
                                        <!--grill equipment start-->
                                        <div class="grill-main-block-equipment col-xs-12 clearfix">
                                            <a class="equipment-btn" role="button" data-toggle="collapse"
                                               href="#mobile-eq-main" aria-expanded="false"
                                               aria-controls="mobile-eq-main">
                                                Входит в базовый комплект
                                            </a>
                                            <!--base equipment start-->
                                            <div class="tab-pane collapse clearfix" id="mobile-eq-main">
                                                <ul class="equipment-tabs-list">
                                                    <?php foreach ($basic_set as $item): ?>
                                                        <?php if( ! empty( $item['name'] ) ): ?>
                                                            <li>
                                                                <span class="equipment-tabs-list-header">
                                                                    <?= $item['name'] ?>
                                                                </span>
                                                                <?php if (!empty($item['description'])): ?>
                                                                    (<?= $item['description'] ?>)
                                                                <?php endif; ?>
                                                            </li>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                </ul>
                                            </div>
                                            <!--base equipment end-->
                                            <?php if ($additional_items): ?>
                                                <a class="equipment-btn" role="button" data-toggle="collapse"
                                                   href="#mobile-eq-add" aria-expanded="false"
                                                   aria-controls="mobile-eq-add">
                                                    Дополнительное оснащение (приобретается отдельно)
                                                </a>
                                                <!--additional equipment start-->
                                                <div class="tab-pane collapse clearfix" id="mobile-eq-add">
                                                    <!--second ul start-->
                                                    <ul class="equipment-tabs-list">
                                                        <?php foreach ($additional_items as $item): ?>
                                                            <?php if( ! empty( $item['name'] ) ): ?>
                                                                <li>
                                                                    <span class="equipment-tabs-list-header">
                                                                        <?= $item['name'] ?>
                                                                    </span>
                                                                    <?php if (!empty($item['description'])): ?>
                                                                        (<?= $item['description'] ?>)
                                                                    <?php endif; ?>
                                                                </li>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    </ul>
                                                </div>
                                                <!--additional equipment end-->
                                            <?php endif; ?>
                                        </div>
                                        <!--grill equipment end-->
                                    </div>
                                </div>
                                <!--panel equipment end-->
                            </div>
                            <?php unset($is_mobile_tab); ?>
                            <?php if ($images): ?>
                                <div class="grill-main-block-inner">
                                    <div class="grill-main-block-slider clearfix eq-base">
                                        <?php foreach ($images as $image): ?>
                                            <?php if (!empty($image['image'])): ?>
                                                <a href="<?= $image['image'] ?>" class="slide"
                                                   title="<?= (!empty($image['description']) ? $image['description'] : '') ?>">
                                                    <img src="<?= $image['image'] ?>"
                                                         alt="<?= (!empty($image['description']) ? $image['description'] : '') ?>">
                                                </a>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <!--grill equipment start-->
                            <div class="grill-main-block-equipment col-lg-12 col-md-12 col-sm-12 col-xs-12 clearfix">
                                <h2 class="section-header">
                                    комплектация
                                </h2>
                                <div class="equipment-tabs">
                                    <ul class="nav nav-tabs " role="tablist">
                                        <li role="equipm-tabs" class="active">
                                            <a href="#eq-base" aria-controls="eq-base" role="tab" data-toggle="tab">
                                                Входит в базовый комплект
                                            </a>
                                        </li>
                                        <?php if ($additional_items): ?>
                                            <li role="equipm-tabs">
                                                <a href="#eq-add" aria-controls="eq-add" role="tab" data-toggle="tab">
                                                    Дополнительное оснащение (приобретается отдельно)
                                                </a>
                                            </li>
                                        <?php endif; ?>
                                    </ul>
                                    <div class="tab-content">
                                        <!--base equipment start-->
                                        <div role="tabpanel" class="tab-pane clearfix active" id="eq-base">
                                            <?php
                                            if ($basic_set):
                                                $items_per_row = count($basic_set) > 18 ? ceil(count($basic_set) / 3) : 6;
                                                $current_row_items_count = 0;
                                                ?>
                                                <ul class="equipment-tabs-list">
                                                    <?php foreach ($basic_set as $item): ?>
                                                        <?php if (!empty($item['name'])): ?>
                                                            <li>
                                                                <?php if (!empty($item['equipment'])):
                                                                  echo '<a class="equipment-tabs-list-link" href="' . get_permalink($item['equipment']) . '">';
                                                                endif; ?>
                                                                <span class="equipment-tabs-list-header">
                                                                     <?= $item['name'] ?>
                                                                </span>
                                                                <?php if (!empty($item['equipment'])):
                                                                    echo '</a>';
                                                                endif; ?>
                                                                <?php if ($item['description']): ?>
                                                                    (<?= $item['description'] ?>)
                                                                <?php endif; ?>
                                                            </li>
                                                        <?php endif;
                                                        $current_row_items_count++;
                                                        if ($current_row_items_count >= $items_per_row):
                                                            echo "</ul>\n<ul class=\"equipment-tabs-list\">\n";
                                                            $current_row_items_count = 0;
                                                        endif;
                                                    endforeach; ?>
                                                </ul>
                                            <?php endif; ?>
                                            <?php if ($basic_set): ?>
                                                <div class="grill-main-block-inner">
                                                    <div class="grill-main-block-slider grill-basic-block-slider clearfix eq-base">
                                                        <?php foreach ($basic_set as $image): ?>
                                                            <?php if (!empty($image['image'])): ?>
                                                                <a href="<?= $image['image'] ?>" class="basic-img slide"
                                                                   title="<?= (!empty($image['img_description']) ? $image['img_description'] : '') ?>">
                                                                    <img src="<?= $image['image'] ?>"
                                                                         alt="<?= (!empty($image['img_description']) ? $image['img_description'] : '') ?>">
                                                                </a>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                        <!--base equipment end-->
                                        <!--additional equipment start-->
                                        <div role="tabpanel" class="tab-pane clearfix" id="eq-add">
                                            <?php
                                            if ($additional_items):
                                                $items_per_row = count($additional_items) > 18 ? ceil(count($additional_items) / 3) : 6;
                                                $current_row_items_count = 0;
                                                ?>
                                                <ul class="equipment-tabs-list">
                                                    <?php foreach ($additional_items as $item): ?>
                                                        <?php if (!empty($item['name'])): ?>
                                                            <li>
                                                                <?php if (!empty($item['equipment'])):
                                                                  echo '<a class="equipment-tabs-list-link" href="' . get_permalink($item['equipment']) . '">';
                                                                endif; ?>
                                                                <span class="equipment-tabs-list-header">
                                                                    <?= $item['name'] ?>
                                                                </span>
                                                                <?php if (!empty($item['equipment'])):
                                                                    echo '</a>';
                                                                endif; ?>
                                                                <?php if ($item['description']): ?>
                                                                    (<?= $item['description'] ?>)
                                                                <?php endif; ?>
                                                            </li>
                                                        <?php   $current_row_items_count++;
                                                                if ($current_row_items_count >= $items_per_row):
                                                                    echo "</ul>\n<ul class=\"equipment-tabs-list\">\n";
                                                                    $current_row_items_count = 0;
                                                                endif;
                                                            endif;
                                                    endforeach; ?>
                                                </ul>
                                            <?php endif; ?>
                                            <?php if ($additional_items): ?>
                                                <div class="grill-main-block-inner">
                                                    <div class="grill-main-block-slider grill-additional-block-slider clearfix eq-add">
                                                        <?php foreach ($additional_items as $image): ?>
                                                            <?php if (!empty($image['image'])): ?>
                                                                <a href="<?= $image['image'] ?>" class="slide"
                                                                   title="<?= (!empty($image['img_description']) ? $image["img_description"] : '') ?>">
                                                                    <img src="<?= $image['image'] ?>"
                                                                         alt="<?= (!empty($image['img_description']) ? $image["img_description"] : '') ?>">
                                                                </a>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                        <!--additional equipment end-->
                                    </div>
                                </div>
                            </div>
                            <!--grill equipment end-->
                        </div>
                        <?php if ($photos): ?>
                            <?php ob_start(); ?>
                            <?php include 'templates/product-photo.php' ?>
                            <?php $js_vars['product-gallery'] = ob_get_clean(); ?>
                        <?php endif; ?>

                        <?php if ($videos): ?>
                            <?php ob_start(); ?>
                            <?php include 'templates/product-video.php' ?>
                            <?php $js_vars['video'] = ob_get_clean(); ?>
                        <?php endif; ?>

                        <?php if ($scheme): ?>
                            <?php ob_start(); ?>
                            <?php include 'templates/product-scheme.php' ?>
                            <?php $js_vars['product-scheme'] = ob_get_clean(); ?>
                        <?php endif; ?>

                        <?php ob_start(); ?>
                        <?php include 'templates/warranty.php'; ?>
                        <?php $js_vars['guarantee'] = ob_get_clean(); ?>

                    <?php endwhile; ?>
                    <?php wp_reset_query(); ?>
                    <?php wp_reset_postdata(); ?>
                <?php else: ?>
                    <?php while (have_posts()): the_post(); ?>
                        <?php $wc_product = new WC_Product(get_the_ID()); ?>
                        <div class="grill-main-block added-prod col-lg-12 col-md-12 col-sm-12 col-xs-12 clearfix">
                            <div class="grill-main-block-img col-lg-5 col-md-5 col-sm-6">
                                <div class="grill-main-block-img-block">
                                    <div class="grill-main-block-img-block-inner" title="<?= the_title() ?>">
                                        <a href="<?= get_the_post_thumbnail_url() ?>" title="<?= the_title() ?>">
                                            <img src="<?= get_the_post_thumbnail_url() ?>" alt="<?= the_title() ?>">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="grill-main-block-description  col-lg-6 col-lg-offset-1 col-md-6 col-md-offset-1 col-sm-6 col-sm-offset-0">
                                <h3 class="recommend-price">
                                    Рекомендованная розничная цена:
                                    <div class="hint">
                                        <div class="hint-icon">?</div>
                                        <div class="hint-text">
                                            <?= arrayItem($fields, 'dop_price_alt') ?>
                                            <div class="hint-close"></div>
                                        </div>
                                    </div>
                                </h3>
                                <div class="grill-main-block-description-price">
                                    <?= number_format($wc_product->get_price() ?: 0, 0, '.', ' '); ?> руб
                                </div>
                                <?php if (isset($fields['dop_confiigs']) && is_array($fields['dop_confiigs'])): ?>
                                    <ul class="grill-main-block-description-list">
                                        <?php foreach ($fields['dop_confiigs'] as $config): ?>
                                            <li class="clearfix">
                                                <div class="grill-main-block-description-list-param">
                                                    <?= arrayItem($config, 'config_name') ?>:
                                                </div>
                                                <div class="grill-main-block-description-list-right clearfix">
                                                    <div class="grill-main-block-description-list-value">
                                                        <?= lappi_hearth_size(arrayItem($config, 'config_param')) ?>
                                                    </div>
                                                    <div class="hint">
                                                        <div class="hint-icon">?</div>
                                                        <div class="hint-text">
                                                            <?= arrayItem($config, 'config_descr') ?>
                                                            <div class="hint-close"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>
                                <?php if (isset($fields['dop_products_for']) && is_array($fields['dop_products_for'])): ?>
                                    <div class="coming-to clearfix">
                                        <div class="coming-to-header">
                                            Подходит к грилям:
                                        </div>
                                        <div class="coming-to-models">
                                            <div class="coming-to-models-row clearfix">
                                                <?php foreach ($fields['dop_products_for'] as $product): ?>
                                                    <?php $products_for = get_post($product); ?>
                                                    <a href="<?= get_permalink($product) ?>"
                                                       class="coming-to-model"><?= $products_for->post_title ?></a>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <div class="grill-main-block-description-text">
                                    <?= get_the_content() ?>
                                </div>

                                <div class="grill-main-block-description-btns">
                                    <?php //DISABLED ?>
                                    <?php if (0): ?>
                                    <a href="<?= get_permalink(45) ?>" class="wtb-btn btn">
                                        Где купить?
                                    </a>
                                    <?php endif; ?>

                                    <?php
                                    $lappigrill_compare_ids = arrayItem($_COOKIE, 'lappigrill_compare_ids', '');
                                    $compare_ids = $lappigrill_compare_ids ? explode(',', $lappigrill_compare_ids) : array();
                                    $in_compare_array = in_array(get_the_ID(), $compare_ids);
                                    ?>
                                    <a href="javascript:void(0);"
                                       class="compare-btn btn <?= $in_compare_array ? "in-compare" : ""; ?>">
                                        <span class="compare-btn-amount"><?= count($compare_ids); ?></span>
                                        <span class="compare-btn-icon"></span>
                                        <span class="compare-btn-text"><?= $in_compare_array ? "В сравнении" : "Сравнение"; ?></span>
                                    </a>
                                </div>
                                
                                <div class="btns-buy">
                                  <a href="" class="buy-btn oneclick"
                                        data-product-id="<?php the_ID(); ?>"
                                        data-product-name="<?= esc_html(get_the_title()); ?>">
                                    <span class="buy-btn-icon oneclick">
                                      <img src="<?= get_stylesheet_directory_uri() ?>/img/buy-oneclick.png" alt="">
                                      <img src="<?= get_stylesheet_directory_uri() ?>/img/buy-oneclick-hover.png" class="hover" alt="">
                                    </span>
                                    Купить в 1 клик
                                  </a>

                                  <a href="" class="buy-btn btn-to-cart"
                                        data-product-id="<?php the_ID(); ?>"
                                        data-product-name="<?= esc_html(get_the_title()); ?>"
                                        data-product-sku="<?= $wc_product->get_sku(); ?>"
                                        data-product-price="<?= number_format($wc_product->get_price() ?: 0, 0, '.', ' '); ?>"
                                        data-product-image="<?= get_the_post_thumbnail_url(get_the_ID()); ?>">

                                    <span class="buy-btn-icon oneclick">
                                      <img src="<?= get_stylesheet_directory_uri() ?>/img/to-cart.png" alt="">
                                      <img src="<?= get_stylesheet_directory_uri() ?>/img/to-cart-hover.png" class="hover" alt="">
                                    </span>                                 
                                    В корзину
                                  </a>
                                </div>
                            </div>
                            <?php if (isset($fields['dop_pics'])): ?>
                                <div class="grill-main-block-inner">
                                    <div class="grill-main-block-slider clearfix">
                                        <?php foreach ($fields['dop_pics'] as $item): ?>
                                            <a href="<?= $item['image']['url'] ?>" class="slide">
                                                <img src="<?= $item['image']['url'] ?>" alt="">
                                            </a>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </section>
    <!--product section end-->

    <script>
        var product_id = <?= get_the_ID(); ?>;
        var product_template_parts_html = <?= json_encode($js_vars); ?>;
    </script>

<?php
get_footer();
?>