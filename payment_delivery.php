<?php
/*
 * Template name: Доставка и оплата
 */
get_header();

$fields = get_fields();
$delivery_items = arrayItem($fields, 'delivery_info', array());
$payment_items = arrayItem($fields, 'payment_info', array());

?>

<?php while (have_posts()): the_post(); ?>

<section id="payment_delivery_page">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ol class="breadcrumb">
            <li><a href="/">Главная</a></li>
            <li class="active"><?php the_title() ?></li>
        </ol>

        <h2 class="section-header">
            <?php the_title() ?>
        </h2>

        <div class="page-content">
            <div class="pd-section section-payment">
                <div class="pd-section-title"><?= arrayItem($fields, 'payment_title'); ?></div>
                <ul class="pd-section-list">
                    <?php foreach ($payment_items as $item): ?>
                    <li class="pd-section-list-item">
                        <div class="pd-section-list-item-title"><?= arrayItem($item, 'title'); ?></div>
                        <div class="pd-section-list-item-content"><?= arrayItem($item, 'text'); ?></div>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="pd-section section-delivery">
                <div class="pd-section-title"><?= arrayItem($fields, 'delivery_title'); ?></div>
                <ul class="pd-section-list">
                    <?php foreach ($delivery_items as $item): ?>
                    <li class="pd-section-list-item">
                        <div class="pd-section-list-item-title"><?= arrayItem($item, 'title'); ?></div>
                        <div class="pd-section-list-item-content"><?= arrayItem($item, 'text'); ?></div>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="after-delivery-section"><?= arrayItem($fields, 'after_delivery_text'); ?></div>
            <div class="assembly-section">
                <div class="assembly-section-title"><?= arrayItem($fields, 'assembly_title'); ?></div>
                <div class="assembly-section-content"><?= arrayItem($fields, 'assembly_info'); ?></div>
            </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php endwhile; ?>

<?php get_footer(); ?>